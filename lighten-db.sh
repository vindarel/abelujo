#!/usr/bin/env bash

# Delete all records in the Barcode64 table (they are taking the most space),
# and shrink the DB disk space.
#
# For development, when a DB is around 500 MB and we want it to be lighter (100 MB).

help() {
    echo "Deleting barcodes and shrinking the DB space."
    echo
    echo "Usage: chmod +x lighten-db.sh then"
    echo "./lighten-db.sh"
    exit 0
}

if [ "-h" = $1 ]
then
    help
fi

ls -lh db.db

echo "deleting barcodes and shrinking the DB space."

sqlite3 db.db 'DELETE FROM search_barcode64 ; VACUUM ;'

ls -lh db.db

echo "all done."
