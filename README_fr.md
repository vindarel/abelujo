Abelujo - logiciel libre de gestion de librairie
================================================

Projet actuellement utilisé au quotidien par des
particuliers, des assocations, des salons de thé, restaurants,
théâtres, centres d'arts et des libraires professionnels.

Abelujo permet de:

-   rechercher des **livres** par mots-clefs ou par bip de l'ISBN. La recherche
    peut se faire via **Dilicom** ou **Electre** si vous avez un compte ou utiliser les résultats qu'on trouve sur le web (moins d'informations bibliographiques, usage non commercial),
    - imports multiples (Dilicom)
    - fonctionne pour la **Suisse** et la **Belgique** via des données web
    - Belgique: il faut pousser la Banque du Livre à mettre en place un FEl à la demande *par web service*.
-   utiliser un **lecteur de codes barres** à travers toute l'application,
-   rechercher des **CDs** (via [discogs.com](http://www.discogs.com)),
-   gérer son stock et voir des statistiques,
-   effectuer des ventes, consulter l'historique des ventes et d'autres
    mouvements de stock, créer des factures,
-   **réserver** des livres et prendre des **commandes** pour des **clients**,
-   faire des **inventaires** en quelques coups de douchette,
-   manipuler des **listes** indépendamment du stock,
-   préparer, recevoir et ventiler des **colis**,
-   exporter des listes en **csv** ou en **pdf** (avec **code-barres** intégrés),
-  importer des données depuis un fichier CSV,
-   créer des dépôts, faire des états de dépôt (attention: à étudier de près avant usage),
-  …


IL est traduit en français (et le support est en français également).

**Abelujo** signifie Ruche en Espéranto.


Installation et développement
=============================

Nous vous laissons le soin de [lire les instructions en anglais](README.md "").

Ci-dessous une capture d'écran de la recherche dans la base de donnée


![chercher une notice](doc/abelujo-collection.png)
