Javascript use in Abelujo
======================

Many pages are built using the AngularJS library.

New pages use custom Javascript code and HTMX.


What happens when we load a page ?
----------------------------------

When we access our root html page:

* Django reads ``base.html`` and loads the "normal" html. If our
  template wants to display an Angular variable enclosed in double
  brackets, we will see them, since Angular didn't interpret them yet
  (unless we use the appropriate directive).
* the scripts called in the ``head`` are loaded: the css and the
  javascript libraries. Angular loads up. Our custom js loads too.
* Angular parses the DOM (Document Object Model, i.e. the html tree)
  and sees a call to ``ng-app="abelujo"``. Angular starts and looks
  for a module called "abelujo". Fortunately, our js code provides
  one, so it is called.
* Angular evaluates the other directives included in the DOM and
  executes the logic of the controllers, like our "IndexController".


How to add a javascript package to the project
----------------------------------------------

To install a package check those steps:

* either import them with a CDN in the base.html template

either

* add the package dependency in package.json for npm,
* add the needed js file(s) into our ``gulpfile.js`` (see vendor
  sources),
* add the (optional) needed css file(s) in the header of our
  ``base.html``,
* declare the module as a dependence of our angularjs app, in
  ``app.js`` (if needed),
* declare the module as a dependency of the controller,

and recompile (`gulp`).

How to add Javascript code
--------------------------

See the file ``abelujo-js.js`` or create another one.


Build the javascript sources
----------------------------

``Gulp`` is responsible for
concatenating every js and css files into a single one, ``abelujo.js``
(among other actions).

When you modify the js, re-build with gulp::

    gulp

see the gulpfile for the other actions.
