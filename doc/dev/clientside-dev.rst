Client-side development
=======================

Write custom CSS
----------------

To customize the look of Abelujo, you can set your CSS rules in each
app ``<name of the app>/static/<name of the app>/style.css`` file,
i.e. in ``search/static/search/style.css``. This file will be loaded
automatically by Django, and collected when we run `collectstatic`
command.


Write JavaScript in Livescript
------------------------------

.. note::

   We used LiveScript but we're now switching to plain Javascript.

Our current Angularjs controllers are written in Livescript::

    * http://livescript.net

The LiveScript executable and repl is `lsc`.

Now to compile LiveScript files run `make gulp`.


You can recompile everything on every change with::

    gulp watch

.. note::

   If you have the same file with both a js and a ls extension, the
   javascript will take precedence (it is appended first in
   ``abelujo.js`` so it is read first by angularjs).


Bootstrap and custom CSS
~~~~~~~~~~~~~~~~~~~~~~~~

The css file for the app is at `search/static/search/style.css`.

Our CSS layout and widgets come from Bootstrap3 and Angular-UI:

- http://getbootstrap.com/components/
- https://angular-ui.github.io/bootstrap/versioned-docs/0.14.3/ .


How to develop
~~~~~~~~~~~~~~

Install the npm dependencies::

  make npm

and at each change, build them::

  make gulp
