#!/usr/bin/env bash

# Goal: make a light copy of the DB.

# Steps:
#
# Make a copy of db.db,
# delete all records in the Barcode64 table of this backup (they are taking the most space),
# shrink the DB disk space (vacuum),
# zip it.
#
# Results:
# from a 21 MB DB to 1.9 MB to 300K zipped.
# or
# from a 600 MB to 100 MB to 28 MB zipped.
# (otherwise 45 MB zipped)

help() {
    echo "Usage: ./backup-db.sh. This will delete an existing backup, delete all Barcode64 records in the DB (saving a lot of space), and create a zip backup with a timestamp."
    echo "for sqlite3 >= 3.15"
    exit 0
}

if [ "-h" = $1 ]
then
    help
fi

echo "deleting barcodes, deleting existing db-backup.db, shrinking DB space and making backup into db-backup.db..."

delete_backup_file() {
    if [ -f db-backup.db ]
    then
        rm db-backup.db
    fi
}

delete_zip() {
    if [ -f db-backup.zip ]
    then
        rm db-backup.zip
    fi
}

delete_backup_file

# First we backup our DB with all data,
# we'll clean it after.
# This is safer than copying the db file.

PROD_NAME=db.db
BACKUP_NAME=db-backup.db

# VACUUM INTO isn't for old sqlite3 (< 2019, on very old servers)
sqlite3 $PROD_NAME 'VACUUM INTO "db-backup.db"'

# Now we delete useless records and shrink space agian.
sqlite3 $BACKUP_NAME 'DELETE FROM search_barcode64 ; VACUUM ;'

echo "now ziping the backup into db-backup.zip with timestamp."
if [ -f db-backup.db ]
then
    zip db-backup.zip db-backup.db && bash -c "cp db-backup.zip{,.`date +%Y%m%d-%H%M%S`}"
else
    echo "no db-backup.db file to zip. There was an error."
    exit 1
fi

delete_backup_file
delete_zip

echo "all done."
