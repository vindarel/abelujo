
function sellUndo(id) {
    // Undo the sell of the given id.
    // (history page)
    if (confirm("Voulez-vous annuler toute cette vente ?")){
        console.log("--- sellUndo id", id);
        fetch(`/api/sell/${id}/undo`, {
            method: 'POST',
        })
            .then((response) => {
                console.log(response);
                // console.log(response.text());
                return response.json();
            })
            .then((myJson) => {
                console.log("json: ", myJson);
                if (myJson.status == true) {
                    console.log('removing line.');
                    var lines = document.querySelectorAll(`#sell${id}`);
                    for (var i = 0; i < lines.length; i++) {
                        lines[i].remove();
                    }
                }
            })
            .catch((error) => {
                console.error('There has been a problem with your fetch operation:', error);
            });
    }
}

function sellBill(sell_id) {
    "Download a PDF bill for that given sell. History page."
    // Defined in abelujo-utils.js:
    let language = url_language(window.location.pathname);

    fetch("/api/bill", {
        method: 'POST',
        body: '{"sell_id": ' + sell_id + ',' + ' "language": ' +
            '"' + language + '"' + '}',
    })
        .then((response) => {
            return response.json();
        })
        .then(function(data){
            if (data.status == 200) {
                let element = document.createElement('a');
                element.setAttribute('href', data.fileurl);
                element.setAttribute('download', data.filename);
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            }});
}

function card_add_one_to_default_place(card_id) {
    // Called from: card_show.html.
    console.log("-- hello add 1 ", card_id);
    let places_ids_qties = "";

    let url = "/api/card/" + card_id + "/add";  // interpolation clunky…
    console.log("-- POSTing to ", url);
    fetch(url, {
        method: 'POST',
        // we don't use Places but this can stay an indicator that
        // we are adding the card to our stock, not to baskets.
        // See card_add in api.py
        body: "default_place: 1"  // JSON.stringify a bit picky…
    })
        .then((response) => {
            console.log("response is ", response);
            return response.json();
        })
        .then((myJson) => {
            if (myJson.status == 200) {
                console.log("-- success. New quantity: ", myJson.quantity);;
                // beware when default_place changes of name :S
                let elt = document.getElementById('default_place_quantity');
                let in_stock = document.getElementById('in_stock');
                let quantity_in_stock = in_stock.innerText;
                console.log("quantity_in_stock: ", quantity_in_stock);
                quantity_in_stock = parseInt(quantity_in_stock);
                in_stock.innerText = quantity_in_stock + 1;
                if (elt) {
                    elt.innerText = myJson.quantity;
                }
                Notiflix.Notify.Success('+1');
            }
            else {
                console.log("status is not success: ", myJson.status);
            }
        })
        .catch((error) => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}

function card_remove_one_from_default_place(card_id) {
    // copy-paste is aweful O_o
    console.log("-- hello remove 1 ", card_id);
    let places_ids_qties = "";

    let url = "/api/card/" + card_id + "/add";  // interpolation clunky…
    console.log("-- POSTing to ", url);
    fetch(url, {
        method: 'POST',
        body: "default_place: -1"  // JSON.stringify a bit picky…
    })
        .then((response) => {
            console.log("response is ", response);
            return response.json();
        })
        .then((myJson) => {
            if (myJson.status == 200) {
                console.log("-- success. New quantity: ", myJson.quantity);;
                // beware when default_place changes of name :S
                let elt = document.getElementById('default_place_quantity');
                let in_stock = document.getElementById('in_stock');
                let quantity_in_stock = in_stock.innerText;
                quantity_in_stock = parseInt(quantity_in_stock);
                in_stock.innerText = quantity_in_stock - 1;
                if (elt) {
                    elt.innerText = myJson.quantity;
                }
                Notiflix.Notify.Success('-1');
            }
            else {
                console.log("status is not success: ", myJson.status);
            }
        })
        .catch((error) => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}

function card_command(card_id) {
    // Called from: card_show.html.
    console.log("-- command 1 ", card_id);
    let places_ids_qties = "";

    let url = "/api/card/" + card_id + "/command";
    console.log("-- POSTing to ", url);
    fetch(url, {
        method: 'POST'
    })
        .then((response) => {
            console.log("response is ", response);
            return response.json();
        })
        .then((myJson) => {
            if (myJson.status == 200 || myJson.status == "success") {
                console.log("-- success.");;
                Notiflix.Notify.Success('OK');

                // Update quantity.
                let elt = document.getElementById('nb_to_command_' + card_id);
                elt.innerText = myJson.data.nb;
            }
            else {
                console.log("status is not success: ", myJson.status);
                Notiflix.Notify.Warning("OK ou pas ?");
            }
        })
        .catch((error) => {
            console.error('There has been a problem with your fetch operation:', error);
            Notiflix.Notify.Warning("An error occured, sorry. We have been notified.");
        });

}

function card_catalogue_select(card_id) {
    // Called from: card_show.html.
    console.log("-- select for catalogue ", card_id);
    let places_ids_qties = "";

    let url = "/api/card/" + card_id + "/select_catalogue";
    console.log("-- POSTing to ", url);
    fetch(url, {
        method: 'POST'
    })
        .then((response) => {
            console.log("response is ", response);
            return response.json();
        })
        .then((myJson) => {
            if (myJson.status == 200 || myJson.status == "success") {
                // Update and toggle the heart colour.
                var heart_selector = "heart-" + card_id;
                let elt = document.getElementById(heart_selector);
                if (elt != null && elt != undefined) {
                    if (myJson.data.is_catalogue_selection) {
                        elt.style = "background-color: pink";
                    } else {
                        elt.style = "";
                    }
                    Notiflix.Notify.Success('OK');
                }
                else {
                    console.warn("warn: we did not find ", heart_selector);
                }
            }
            else {
                console.log("status is not success: ", myJson.status);
                Notiflix.Notify.Warning("OK ou pas ?");
            }
        })
        .catch((error) => {
            console.error('There has been a problem with your fetch operation:', error);
            Notiflix.Notify.Warning("An error occured, sorry. We have been notified.");
        });

}

function card_catalogue_exclude(card_id) {
    // Called from: card_show.html.
    console.log("-- exclude from catalogue ", card_id);
    let places_ids_qties = "";

    let url = "/api/card/" + card_id + "/exclude_catalogue";
    console.log("-- POSTing to ", url);
    fetch(url, {
        method: 'POST'
    })
        .then((response) => {
            console.log("response is ", response);
            return response.json();
        })
        .then((myJson) => {
            if (myJson.status == 200 || myJson.status == "success") {
                // Update and toggle the exclude colour.
                var heart_selector = "exclude-" + card_id;
                let elt = document.getElementById(heart_selector);
                console.log("--- got elt: ", elt);
                if (elt != null && elt != undefined) {
                    if (myJson.data.is_excluded_for_website) {
                        elt.style = "background-color: pink";
                    } else {
                        elt.style = "";
                    }
                    Notiflix.Notify.Success('OK');
                }
                else {
                    console.warn("warn: we did not find ", heart_selector);
                }
            }
            else {
                console.log("status is not success: ", myJson.status);
                Notiflix.Notify.Warning("OK ou pas ?");
            }
        })
        .catch((error) => {
            console.error('There has been a problem with your fetch operation:', error);
            Notiflix.Notify.Warning("An error occured, sorry. We have been notified.");
        });

}

function getCookie(name) {
    // Used to get Django's CSRF token.
    // source: Django documentation.
    // Usage: getCookie('csrftoken') => getCSRFToken()
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

function getCSRFToken(){
    return getCookie('csrftoken');
};

/* Hide and show the menu. */
// Doesn't work on Chrome 87 ? O_o Functions not loaded (on time).
function collapseMenu() {
    var menu  = document.getElementById('sidebar-wrapper');
    var wrapper = document.getElementById('wrapper');
    menu.style.width = 0;
    wrapper.style.paddingLeft = 0;
};

function showMenu() {
    var menu  = document.getElementById('sidebar-wrapper');
    var wrapper = document.getElementById('wrapper');
    menu.style.width = "250px";
    wrapper.style.paddingLeft = "250px";
};

function do_focus() {
    // the ID doesn't contain a # ;)
    document.getElementById('default-input').focus();
}

function toggleMenu() {
    // Toggle the left menu.
    // Focus the mouse cursor back to the default input field.
    var menu  = document.getElementById('sidebar-wrapper');
    if (menu.getAttribute('data-menu-collapsed') == "true") {
        showMenu();
        menu.setAttribute('data-menu-collapsed', false);
    } else {
        collapseMenu();
        menu.setAttribute('data-menu-collapsed', true);
    }

    do_focus();
};

function sendSMS(client_pk) {
    // Called from: reservations.html.
    console.log("-- send SMS to client ", client_pk);

    let url = "/api/clients/" + client_pk + "/notify/sms/";
    let textarea = document.getElementById("modal-body-" + client_pk);
    let body = textarea.value;  // textContent doesn't grab the updated text.
    console.log("body: ", body);
    console.log("-- POSTing to ", url);
    fetch(url, {
        method: 'POST',
        body: body,
    })
        .then((response) => {
            console.log("response is ", response);
            return response.json();
        })
        .then((myJson) => {
            if (myJson.status == 200 || myJson.status == "success") {
                console.log("-- success.");;
                Notiflix.Notify.Success('OK');

                // Update quantity.
                // let elt = document.getElementById('nb_to_command_' + card_id);
                // elt.innerText = myJson.data.nb;
            }
            else {
                // console.log("status is not success: ", myJson.status);
                for (var i = 0; i < myJson.alerts.length; i++) {
                    Notiflix.Notify.Warning(myJson.alerts[i]);
                }
            }
        })
        .catch((error) => {
            console.error('There has been a problem with your fetch operation:', error);
            Notiflix.Notify.Warning("An error occured. Are you connected to the internet?");
        });

}

//////////////////////////
//                      //
// Client Reservations. //
//                      //
//////////////////////////
function filterClients() {
    // Filter clients by the search input.
    let input = document.getElementById('default-input');
    let search_input = input.value.toLowerCase();
    let rows = document.getElementsByClassName('client-row');
    for (var i = 0; i < rows.length; i++) {
        let row = rows[i];
        let author_span = row.getElementsByClassName('client-name')[0];
        let name = author_span.textContent.trim().toLowerCase();
        if (!search_input || !search_input || search_input.trim() == "") {
            // If no input, show all.
            // console.log("show all");
            row.style.display = "";
        } else {
            if (name.search(search_input) < 0) {
                // Hide.
                row.style.display = "none";
            } else {
                // Show.
                // console.log("showing ", name);
                row.style.display = "";
            }
        }
    }
}

/////////////////////////////////////////
//                                     //
// Institution baskets: filter titles. //
// Also for Reservations and Suppliers page. //
//                                     //
/////////////////////////////////////////

function filterTitles( input_id = 'default-input' ) {
    // Adapted from filterClients.
    // Filter titles by the search input.
    let input = document.getElementById(input_id);
    let search_input = input.value.toLowerCase();
    let rows = document.getElementsByClassName('result-row');
    for (var i = 0; i < rows.length; i++) {
        let row = rows[i];
        let author_span = row.getElementsByClassName('result-value')[0];
        let name;
        if (author_span !== null) {
            name = author_span.textContent.trim().toLowerCase();
        }
        if (!search_input || !search_input || search_input.trim() == "") {
            // If no input, show all.
            // console.log("show all");
            row.style.display = "";
        } else {
            if (name.search(search_input) < 0) {
                // Hide.
                row.style.display = "none";
            } else {
                // Show.
                // console.log("showing ", name);
                row.style.display = "";
            }
        }
    }
}


function reload_with_url_param(param, param_re, val) {
    // "param_re: such that it matches place_id=99 or another URL parameter."
    // let re = /place_id=\d+/gi ;
    let re = param_re;
    let has_place_param = re.exec(document.location.href);
    // let pk = document.getElementById('places').value;

    console.log(has_place_param, val);

    if ( has_place_param !== null ) {
        // assign = reload with back history button.
        document.location.assign(
            document.location.href.replace(
                param_re,
                param + '=' + val
            )
        );
    }
    else {
        // fails with other query parameters.
        document.location.assign(
            document.location.href + '?' + param + '=' + val
        );
    }
}

function sortTable(n, is_numeric) {
  // The table is named with ID table-data.
  //
  // All data must be in a data-value attribute on the row.
  // If is_numeric == 1, parse the value as float before comparison.
  // https://www.w3schools.com/howto/howto_js_sort_table.asp + added numeric sort and data-value.
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;

  table = document.getElementById("table-data");
  switching = true;

  // Set the sorting direction to ascending:
  dir = "asc";

  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;

    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];

      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      let xval = x.getAttribute('data-value').toLowerCase();
      let yval = y.getAttribute('data-value').toLowerCase();
      if (is_numeric == 1) {
          xval = parseFloat(x.getAttribute('data-value'));
          yval = parseFloat(y.getAttribute('data-value'));
      }

      if (dir == "asc") {
          if (xval > yval) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
          if (xval < yval) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

function sortTableString(n) {
    sortTable(n, 0);
}

function sortTableNumeric(n) {
    // We should read the row's value as a numerical value.
    sortTable(n, 1);
}

////////////////////////////////
// Global keyboard shortcuts.
////////////////////////////////

hotkeys('ctrl+left', function(event, handler) {
    event.preventDefault();  // not necessary here.
    toggleMenu();
});

hotkeys('ctrl+right', function(event, handler) {
    event.preventDefault();
    toggleMenu();
});

hotkeys('s', function(event, handler) {
    event.preventDefault();
    do_focus();
});
