
function url_language(url) {
    // Extract the language from an url like /fr/foo/bar
    //
    // We can also use the request.META.get('HTTP_REFERER') on the server,
    // only it will change by Django versions.
    let re = /\/([a-z][a-z])\//;
    let res = url.match(re);
    if (res) {
        return res[1];
    } else {
        return "en";
    }
};

// console.log("--- language: ", url_language(window.location.pathname));
