#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Update all the cards' information after a Dilicom lookup, including:

- the distributor
- the publisher
- thickness, weight etc
- etc

This can take several minutes with thousands of cards.

Options:

- verbose: -V t

"""
from __future__ import unicode_literals

import time

from django.core.management.base import BaseCommand

from search.datasources.bookshops.frFR.dilicom import dilicomScraper
from search.models import Card
from search.models import SoldCards

# py2/3
try:
    input = raw_input
except NameError:
    pass


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '-V',
            dest='verbose',
            help='debug output.',
        )
        parser.add_argument(
            '--sell-year',
            dest='sell_year',
            help='Sell year.',
        )
        parser.add_argument(
            '--sell-month',
            dest='sell_month',
            help='Sell month.',
        )
        parser.add_argument(
            '--sell-day',
            dest='sell_day',
            help='Sell day.',
        )

    def update_all(self, options={}):
        cards = []
        soldcards = []

        sell_year = options.get('sell_year')
        sell_month = options.get('sell_month')
        sell_day = options.get('sell_day')
        if sell_month and not sell_year:
            self.stdout.write("If you give a sell month, please give us a year.")

        if sell_year:
            self.stdout.write("Getting cards from sell of year {}".format(sell_year))
            soldcards = SoldCards.objects.filter(created__year=sell_year)
            # Utiliser Card.objects.filter(soldcards__created__year=…) etc
            # ne compose pas bien: le résultat augmente au lieu d'être filtré. Merci Python.
        if sell_month:
            self.stdout.write("Getting cards from sell of month {}".format(sell_month))
            soldcards = soldcards.filter(created__month=sell_month)
        if sell_day:
            self.stdout.write("Getting cards from sell of day {}".format(sell_day))
            soldcards = soldcards.filter(created__day=sell_day)

        import ipdb; ipdb.set_trace()

        if soldcards:
            ids = soldcards.values_list('pk', flat=True)
            # cards = [it.card for it in soldcards]
            # On veut garder une queryset pour le code ci-dessous,
            # et ne pas obtenir une bete liste.
            cards = Card.objects.filter(soldcards__pk__in=ids)

        # All cards with an ISBN.
        if not cards:
            cards = Card.objects.filter(isbn__isnull=False, quantity__gt=0)

        self.stdout.write("Updating {} cards.".format(cards.count()))

        confirmation = True
        # confirmation = raw_input("Continue ? [Y/n]")
        if confirmation == "n":
            exit(0)

        isbns = cards.values_list("isbn", flat=True)  # [it.isbn for it in cards]

        # isbns = isbns[:100]  # DEBUG:
        # isbns = isbns[50:500]  # TODO:
        # isbns = isbns[430:500]  # TODO:
        dilicom_query = dilicomScraper.Scraper(*isbns)
        self.stdout.write("Searching all on Dilicom...")
        bklist, errors = dilicom_query.search()

        if len(bklist) != len(isbns):
            self.stdout.write("--- beware: the search results have not the same length that our query: {} vs {}".format(len(bklist), len(isbns)))
            # exit(1)

        cards_updated = []
        cards_not_updated = []
        count_ok = 0
        for i, card in enumerate(cards):
            bk = list(filter(lambda it: it['isbn'] == card.isbn, bklist))
            if bk:
                bk = bk[0]

            if not bk:
                # self.stdout.write("No matching result for card {}. Pass.".format(card.isbn))
                cards_not_updated.append(card)
                continue

            # self.stdout.write("{}: => {}".format(card.title, bk))

            # THE update.
            # same publisher?
            # print("--publishers: {} VS {}".format(bk.get('publishers'), " + ".join([it.name for it in card.publishers.all()])))

            pub_name = ""
            if bk.get('publishers'):
                pub_name = bk.get('publishers')[0]
            if not pub_name:
                print("Could not find the name of distributor for {}. Exiting.".format(bk.get('publishers')[0]))
                exit(1)

            print("* {} - updating {}".format(i, card.isbn))

            try:
                gln = bk.get('distributor_gln')
                if not gln:
                    print("Could not get distributor GLN from {}. Exiting.".format(bk))
                    exit(1)
                Card.update_from_dict(card, card_dict=bk,
                                      distributor_gln=gln,
                                      publisher_name=pub_name)

                cards_updated.append(card)
                count_ok += 1
            except Exception as e:
                self.stdout.write(" ! failed for {}: {}".format(bk.get('isbn'), e))

            # Updating 4.000 cards is resource heavy.
            if i % 100 == 0:
                time.sleep(0.2)

        self.stdout.write("-------------------")
        self.stdout.write("Cards OK: {}.".format(count_ok))
        self.stdout.write("Cards updated:{}".format(len(cards_updated)))
        self.stdout.write("Cards not updated:{}".format(len(cards_not_updated)))
        # self.stdout.write("\n".join([it.isbn for it in cards_not_updated]))
        self.stdout.write("Done.")

    def handle(self, *args, **options):
        self.stdout.write("Go...")
        try:
            self.update_all(options=options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
