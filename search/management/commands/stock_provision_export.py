#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Export the cards that are considered to compute the stock provision, in CSV.

If we export the list some time after the deadline, we need to set the
reference date. Use --date.

./manage.py stock_provision_export [--date "yyyy/mm/dd"] [--format csv]

"""


import os

import pendulum
from django.core.management.base import BaseCommand

from abelujo import settings
from search.models import Card, Stats
from search.models import utils
from search.views_utils import cards2csv

# py2/3
try:
    input = raw_input
except NameError:
    pass


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--format',
                            dest="format",
                            action="store",
                            help="format (csv, pdf, txt). Defaults to csv.")

        parser.add_argument('--date',
                            dest="date",
                            action="store",
                            help="Deadline date, day included, from which to compute and export the stock provision.")

    def handle(self, *args, **options):
        formatt = options.get('format', 'csv')
        if not formatt:
            formatt = 'csv'

        date = options.get('date')
        now = pendulum.date.strftime(pendulum.now(), '%Y-%m-%d %H:%M')

        if date:
            try:
                date = pendulum.parse(date)
                print("--- deadline date for stock provision: ", date)
            except pendulum.parsing.exceptions.ParserError:
                self.stderr.write("Error: invalid date string. You can supply an english-looking date like this: 30th, march 2022. Aborting.")
                exit(1)

        else:
            content = "The export format {} isn't supported.".format(formatt)


        #
        # Select all cards for stock provision
        #
        self.stdout.write("--- Getting cards for stock provision...")
        cards = Stats.stock_provision_cards(deadline=date)

        provision, msgs = Stats.stock_provision(deadline=date)
        self.stdout.write("-- provision: {}".format(provision))

        cards_dicts = [it.to_dict() for it in cards]  # slow…
        # cards_dicts = [it.to_dict() for it in cards[0:10]]  # DEBUG
        content = cards2csv(cards_dicts)


        #
        # Save the file.
        #
        filedate = date or now
        filename = "abelujo-stock-provision-{}.{}".format(filedate, formatt)
        filepath = os.path.join(settings.EXPORTS_ROOT, filename)
        if formatt == 'csv':
            with open(filename, 'w') as f:
                f.write(content)
        else:
            with open(filepath, 'w', encoding='utf8') as f:
                f.write(content)

        # Build the response.
        print("Done. Saved file {}".format(filename))
