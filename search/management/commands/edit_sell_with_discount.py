#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import unicodecsv as csv
from django.core.management.base import BaseCommand

from search.models import utils
from search.models import Sell

# py2/3
try:
    input = raw_input
except NameError:
    pass

"""
Edit a sell to apply a discount.

Usage:

./manage.py edit_sell_with_discount --discount 9 --sells 4006,4033,4063

We need an edit button, but we will still need this for users who can
not edit sells past one month.

- take one or many sells by their id
- take a discount
- apply the discount to the soldcard objects
- apply it to the total_payment_1 of the sell object.
"""

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--sell',
                            dest="sell",
                            action="store",
                            help="Sell ID.")
        parser.add_argument('--sells',
                            dest="sells",
                            action="store",
                            help="Many sells ID, comma-separated.")
        parser.add_argument('--discount',
                            dest="discount",
                            action="store",
                            help="Discount to apply to the sold card objects and the total payment.")


    def edit_sell(self, sell, discount=9):

        print("Vente n°{}".format(sell.pk))

        for sc in sell.soldcards_set.all():
            origprice = sc.price_sold
            newprice = utils.roundfloat(
                utils.price_minus_percent(sc.price_sold, discount)
            )
            sc.price_sold = newprice
            print("- nouveau prix pour {} '{}': {} -> {}".format(
                sc.card.pk, sc.card.title, origprice, newprice,
            ))
            # sc.save()

        origtotal = sell.total_payment_1
        total = utils.roundfloat(
            utils.price_minus_percent(sell.total_payment_1, 9)
        )
        sell.total_payment_1 = total
        # sell.save()
        print("- nouveau total pour vente {}: {} -> {}".format(
            sell.pk, origtotal, total,
        ))

        return total


    def run(self, *args, **options):
        """
        """
        sell_id = options.get('sell')
        sells_ids = options.get('sells')
        if sells_ids:
            sells_ids = sells_ids.split(',')

        discount = options.get('discount')
        discount = int(discount)

        sells = Sell.objects.filter(id__in=[sell_id] if sell_id else sells_ids)

        for sell in sells:
            self.edit_sell(sell, discount=discount)

        self.stdout.write("All done.")


    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
