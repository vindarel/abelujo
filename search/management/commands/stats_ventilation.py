#!/bin/env python
# -*- coding: utf-8 -*-

"""

"""
from __future__ import print_function
from __future__ import unicode_literals

from tqdm import tqdm
from pprint import pprint

from django.core.management.base import BaseCommand

from search.models import stats
from search.models.utils import to_ascii

import logging
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s:%(lineno)s]:%(message)s', level=logging.WARNING)


class Command(BaseCommand):

    help = ""

    def add_arguments(self, parser):
        parser.add_argument(
            '-l',
            dest='lang',
            help='Set the language (unused here).',
        )
        parser.add_argument(
            '-s',
            dest='stock',
            action='store_true',
            help='Old and new stock only, no number on shelves.',
        )

    def run(self, *args, **options):
        """
        """
        if options.get('lang'):
            self.stdout.write("Unimplemented. Would you buy me a beer ?")
            exit(1)

        with_shelves = True
        if options.get('stock'):
            with_shelves = False

        res = stats.categories_ventilation(
            with_shelves=with_shelves,
            verbosity=options.get('verbosity'),
        )

        # self.stdout.write("stats:")

    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
