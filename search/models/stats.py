# -*- coding: utf-8 -*-

"""
Global stats.
"""
from __future__ import unicode_literals

import json
import os

import pendulum
from cachetools import TTLCache
from cachetools import cached
# __unicode__ is now __str__
from django.utils.encoding import python_2_unicode_compatible  # noqa: F401
# from django.utils import six
from toolz.dicttoolz import valmap
from toolz.itertoolz import groupby

from abelujo import settings
# from search.models import EntryCopies
from search.models import Card
from search.models import Preferences
from search.models import Sell
from search.models import Shelf
from search.models import SoldCards
from search.models import StatsCache
from search.models import shelf_age_sort_key
from search.models import utils
from search.models.common import ALERT_ERROR  # noqa: F401
from search.models.common import ALERT_INFO  # noqa: F401
from search.models.common import ALERT_SUCCESS  # noqa: F401
from search.models.common import ALERT_WARNING  # noqa: F401
from search.models.common import CHAR_LENGTH  # noqa: F401
from search.models.common import CURRENCY_CHOICES  # noqa: F401
from search.models.common import DATE_FORMAT  # noqa: F401
from search.models.common import PAYMENT_CHOICES  # noqa: F401
from search.models.common import TEXT_LENGTH  # noqa: F401
from search.models.common import YMD_DATE_FORMAT  # noqa: F401
from search.models.utils import price_fmt

# from django.utils.translation import ugettext as _  # in functions.
# from django.utils.translation import ugettext_lazy as __  # in Meta and model fields.



log = utils.get_logger()

def stock_age(to_dict_light=True):
    """
    Group all the cards in stock by age interval (0-3 months, 3-6, 6-12, 12-18, 18-24, >24).

    Return: dict: int -> list of simple card dicts (only id, title, price, quantity) (necessary for speed).
    """
    # very similar to Stats._shelf_age, but for all cards in stock.
    cards = Card.objects.filter(quantity__gt=0, isbn__isnull=False)
    # XXX: get only books.
    # cards = Card.objects.filter(quantity__gt=0)[:500]   # DEV # TODO:
    stats = groupby(shelf_age_sort_key, cards)

    # Have a dict with all keys (easier for plotting charts)
    for key in range(6):
        if key not in stats:
            stats[key] = []

    # to dict
    if to_dict_light:
        stats = valmap(lambda its: [it.to_dict_light() for it in its], stats)

    return stats

def merge_ages(stats):
    """
    Our categories 3, 4, 5 must be grouped into one (>12 months) for our official stats.
    """
    res = {}
    tmp_cards = []
    for age, cards in stats.items():
        if age in [0, 1, 2]:
            res[age] = cards
        else:
            tmp_cards += cards

    res[3] = tmp_cards
    return res

def stock_age_state(to_dict_light=True):
    """
    Same as above, augmented:
    - nb of references
    - nb of volumes
    - ratio volumes / references
    - stock value
    - % stock value

    And for those official stats, the last age is >12 months, so we need to merge our categories 3, 4 and 5.

    Return: dict int -> dict with keys: data (card objects), and the required numbers.
    """
    stats = stock_age(to_dict_light=to_dict_light)
    stats = merge_ages(stats)
    res = {}
    total_value = 0
    stats_values = {}  # age (int) -> value (float)

    # Get total value.
    for stat in stats.items():
        age = stat[0]
        cards = stat[1]
        value = 0
        for card in cards:
            if card.price and card.quantity:
                value += card.price * card.quantity

        total_value += value
        stats_values[age] = value

    # Get other numbers.
    for stat in stats.items():
        age = stat[0]
        cards = stat[1]
        nb_titles = len(stat[1])
        nb_copies = sum([it.quantity for it in stat[1]])
        if nb_copies and nb_titles:
            ratio_copies_titles = utils.roundfloat(nb_copies / float(nb_titles), rounding="ROUND_HALF_EVEN")
        else:
            ratio_copies_titles = 0

        value = utils.roundfloat(stats_values[stat[0]], rounding="ROUND_HALF_EVEN")
        value_percent = 0
        if value and total_value:
            value_percent = utils.roundfloat(value / total_value * 100, rounding="ROUND_HALF_EVEN")
        res[age] = {
            'nb_titles': nb_titles,
            'nb_copies': nb_copies,
            'ratio_copies_titles': ratio_copies_titles,
            'value': value,
            'value_percent': value_percent,
            # 'data': cards,
        }

        res[99] = {
            'comment': "Products with an ISBN in stock.",
        }

    return res

def _save_ventilation_in_cache(data=None, year=None, month=None, day=None):
    """
    Save this data for this year/month and optionaly day.

    For ventilation data, we only save by month.

    Return: boolean.
    """
    directory = settings.STATS_DIR
    if not os.path.exists(directory):
        # Shouldn't happen but hey...
        log.error("Could not save stats on disk: STATS_DIR doesn't exist.")
        return False
    # TODO:

def categories_ventilation(with_shelves=True, verbosity=0):
    """
    Allocation of books by category: nb of titles, of copies, %, value, % value, rotation, revenue of books.

    Also get the same numbers for: new parutions (<1 month) and old stock (>12 months).

    Params:
    - with_shelves: if not True, don't include data on shelves, include only data on new and old stock.
    - verbosity: if True, print on stdout. Useful when running ./manage.py stats_ventilation -v

    Return: xxx

    The official categories overlap shelves.
    For now, we get numbers on shelves, we don't try to match official categories.
    - Littérature
    - Policier/SF
    - Poésie
    - Théâtre
    - Jeunesse
    - BD
    - …

    """
    # total_value_shelves = 0
    # total_copies_shelves = 0
    # to_ret = {}

    now = pendulum.now()
    min_date = now
    twelve_months_ago = now - now.subtract(months=12)
    one_month_ago = now - now.subtract(months=1)
    if isinstance(min_date, pendulum.Period):
        min_date = min_date.start
    if isinstance(twelve_months_ago, pendulum.Period):
        twelve_months_ago = twelve_months_ago.start
    if isinstance(one_month_ago, pendulum.Period):
        one_month_ago = one_month_ago.start

    # All entries since 1 year.
    # all_entries = EntryCopies.objects.filter(created__lte=min_date, created__gte=twelve_months_ago)

    # All sells since 1 year.
    # all_sells = SoldCards.objects.filter(created__lte=min_date, created__gte=twelve_months_ago)
    all_sells = SoldCards.objects.filter(created__lte=min_date, created__gte=twelve_months_ago)

    vals = all_sells.values_list('price_sold', 'quantity')
    # start = pendulum.now()
    # all_sells_total = sum([it[0] * it[1] for it in vals])
    # end = pendulum.now()
    # log.debug("--- all_sells_total took {}".format(end - start))

    shelves = Shelf.objects.all()

    def _print(s):
        if verbosity:
            print(s)

    def shelf_ventilation(shelf, all_sells=[]):
        to_ret = {
            'shelf': shelf.to_dict(),
            'shelf_id': shelf.pk,
        }
        cards = shelf.cards(in_stock=True)
        _print(shelf)
        # Number of cards.
        cards_qty = shelf.cards_qty
        to_ret['cards_qty'] = cards_qty
        # Number of copies (volumes).
        copies_qty = shelf.copies_qty
        to_ret['copies_qty'] = copies_qty
        # _print("cards: {} copies: {}".format(shelf.cards_qty, shelf.copies_qty))
        _print("références: {} volumes: {}".format(cards_qty, copies_qty))

        # Value.
        # _print("value: {}".format(shelf.value()))
        value = shelf.value()
        to_ret['value'] = value
        # to_ret['percent_value'] = XXX
        _print("valeur stock: {}".format(value))

        # Entries for this shelf.
        # entries = all_entries.filter(card__shelf=shelf)

        sells = all_sells.filter(card__shelf=shelf)
        vals = sells.values_list('price_sold', 'quantity')
        total_sells = sum([it[0] * it[1] for it in vals])
        to_ret['total_sells'] = total_sells
        # XXX: percent

        # Rotation:
        # value of sells / mean value of stock
        # for the mean value, we only take the current value, for now…
        rotation = utils.roundfloat(total_sells / value if value else 0, rounding="ROUND_HALF_EVEN")
        to_ret['rotation'] = rotation
        _print("rotation: {}".format(rotation))

        rotation_days = utils.roundfloat((value * 365) / total_sells if total_sells else 0, rounding="ROUND_HALF_EVEN")
        to_ret['rotation_days'] = rotation_days
        _print("rotation jours: {}".format(rotation_days))

        #
        # Same numbers, for new parutions (<1 month) and old stock (>12 months).
        #
        # new parutions: let's consider the ones published less than
        # one month ago *relatively to their date of entry in stock*.
        new_stock = []
        new_stock_data = {}
        new_stock = cards.filter(date_publication__gte=one_month_ago)
        new_stock_data['cards_qty'] = new_stock.count()
        new_stock_data['copies_qty'] = sum([it.quantity for it in new_stock])
        new_stock_data['value'] = sum([it.quantity * it.price for it in new_stock])

        new_stock_data['percent_copies'] = 0
        if copies_qty:
            percent_copies = utils.roundfloat(
                new_stock_data['copies_qty'] / float(copies_qty) * 100,
                rounding="ROUND_HALF_EVEN"
            )
            new_stock_data['percent_copies'] = percent_copies

        new_stock_data['percent_value'] = 0
        if value:
            percent_value = utils.roundfloat(new_stock_data['value'] / float(value) * 100, rounding="ROUND_HALF_EVEN")
            new_stock_data['percent_value'] = percent_value
        _print("nouveautés: références: {} volumes: {} % volumes: {} valeur: {} % valeur: {}".format(
            new_stock_data['cards_qty'],
            new_stock_data['copies_qty'],
            new_stock_data['percent_copies'],
            new_stock_data['value'],
            new_stock_data['percent_value'],
        ))
        to_ret['new_stock_data'] = new_stock_data

        #
        # Old stock
        #
        old_stock = []
        old_stock_data = {}
        old_stock = cards.filter(date_publication__gte=twelve_months_ago)
        old_stock_data['cards_qty'] = old_stock.count()
        old_stock_data['copies_qty'] = sum([it.quantity for it in old_stock])
        old_stock_data['value'] = sum([it.quantity * it.price for it in old_stock])

        old_stock_data['percent_copies'] = 0
        if copies_qty:
            percent_copies = utils.roundfloat(old_stock_data['copies_qty'] / float(copies_qty) * 100,
                                        rounding="ROUND_HALF_EVEN")
            old_stock_data['percent_copies'] = percent_copies

        old_stock_data['percent_value'] = sum([it.quantity * it.price for it in old_stock])
        if value:
            percent_value = utils.roundfloat(old_stock_data['value'] / float(value) * 100,
                                       rounding="ROUND_HALF_EVEN")
            old_stock_data['percent_value'] = percent_value

        _print("fonds: références: {} volumes: {} % volumes: {} valeur: {} % valeur: {}".format(
            old_stock_data['cards_qty'],
            old_stock_data['copies_qty'],
            old_stock_data['percent_copies'],
            old_stock_data['value'],
            old_stock_data['percent_value'],
        ))
        to_ret['old_stock_data'] = old_stock_data

        _print("")
        return to_ret
        ### end shelf_ventilation ###

    shelves_ventilation = []  # first sorted by …
    if with_shelves:
        for shelf in shelves:
            shelves_ventilation.append(shelf_ventilation(shelf, all_sells=all_sells))

        # Sort by rotation.
        shelves_ventilation = sorted(shelves_ventilation, key=lambda it: it['rotation'], reverse=True)

    #
    # Rotation of new stock
    # for all the stock, globally.
    # XXX: we compute the new stock rotation with this months' sells only,
    # so we compute "this month's stock value" with a mean: the total value, divided by 12.
    this_month_sells = all_sells.filter(created__gte=one_month_ago)
    # Sells of this month for new stock only:
    this_month_new_stock_sells = this_month_sells.filter(
        card__date_publication__gte=one_month_ago
    )
    vals = this_month_new_stock_sells.values_list('price_sold', 'quantity')
    this_month_new_stock_sells_total = utils.roundfloat(
        sum([it[0] * it[1] for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )

    # tmp vars, shorter.
    total_sells = this_month_new_stock_sells_total
    value = 0
    # value of new stock in total.
    new_stock = Card.objects.filter(date_publication__gte=one_month_ago)  # gte one_month_ago
    vals = new_stock.values_list('price', 'quantity')
    new_stock_value = utils.roundfloat(
        sum([it[0] * it[1] for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )
    value = new_stock_value / 12.0  # get a mean.

    new_stock_rotation = utils.roundfloat(
        total_sells / value if value else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("nouveautés rotation: {} (CA ce mois {} / valeur stock (moyenne mois) {})".format(
        new_stock_rotation,
        total_sells,
        value,
    ))

    new_stock_rotation_days = utils.roundfloat(
        (value * 365) / total_sells if total_sells else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("nouveautés rotation jours: {}".format(new_stock_rotation_days))
    # -- end rotation new stock --- #

    # ! copy-pasted from new stock.
    # Rotation of old stock.
    # We compute the old stock rotation with this months' old stock
    # THAT WAS SOLD THIS MONTH
    # only,
    # so we compute a mean of the old stock to get "this month's old stock".

    # this_month_sells = all_sells.filter(created__gte=one_month_ago)  # got it above.
    # Sells of this month for old stock only:
    this_month_old_stock_sells = this_month_sells.filter(
        card__date_publication__lte=twelve_months_ago  # here lte twelve_months_ago
    )
    vals = this_month_old_stock_sells.values_list('price_sold', 'quantity')
    this_month_old_stock_sells_total = utils.roundfloat(
        sum([it[0] * it[1] for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )

    # tmp vars, shorter.
    total_sells = this_month_old_stock_sells_total
    value = 0
    # value of old stock in total.
    old_stock = Card.objects.filter(date_publication__lte=twelve_months_ago)  # lte twelve_months_ago
    vals = old_stock.values_list('price', 'quantity')
    old_stock_value = utils.roundfloat(
        sum([ (it[0] or 0) * max(it[1], 0) for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )
    value = old_stock_value / 12.0  # get a mean.

    old_stock_rotation = utils.roundfloat(
        total_sells / value if value else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("fonds rotation: {} (CA ce mois {} / valeur stock (moyenne mois) {})".format(
        old_stock_rotation,
        total_sells,
        value,
    ))

    old_stock_rotation_days = utils.roundfloat(
        (value * 365) / total_sells if total_sells else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("fonds rotation jours: {}".format(old_stock_rotation_days))
    # -- end rotation old stock --- #

    # Save data for this month on disk.
    # save it on the DB? On files?
    # _save_ventilation_in_cache(now)

    res = {
        'new_stock_rotation': new_stock_rotation,
        'new_stock_rotation_days': new_stock_rotation_days,
        'old_stock_rotation': old_stock_rotation,
        'old_stock_rotation_days': old_stock_rotation_days,
        'shelves_ventilation': shelves_ventilation,
    }
    return res


@cached(cache=TTLCache(maxsize=8024, ttl=72000))  # 20 hours
def history_publishers_with_cache(year=None, month=None):
    """
    Hold data in memory for ± 20 hours.
    We don't enter this function with a cache hit, only when it expired.
    Then in the core function, we query the StatsCache cache.
    """
    log.info("history_publishers_with_cache: querying and populating cache")
    return history_publishers(year=year, month=month)

def maybe_save_to_db_cache(now=None, cache_name=None, year=None, month=None, to_json=True, data=None, debug=False):
    """
    Transform data to json and save to a new StatsCache row.
    """
    obj = None
    if now.year != year or now.month != month:
        log.info("history_publishers: saving cache")
        try:
            cache_date = pendulum.create(year=int(year), month=int(month))
            raw_data = data
            if to_json:
                raw_data = json.dumps(data)
            assert raw_data
            obj = StatsCache.objects.create(
                name=cache_name,
                date=cache_date,
                raw_data=raw_data
            ).save()
            log.info("history_publishers: saved our data to cache for {} on {}-{}".format(
                cache_name, year, month))

        except Exception as e:
            log.error("history_publishers: could not save our data to cache: {}".format(e))
            if debug:
                raise e

    return obj


def maybe_read_db_cache(now=None, cache_name=None, year=None, month=None, from_json=True, data=None, debug=False):
    """
    Query StatsCache, parse JSON if asked.

    Return: a tuple result, found (boolean).
    """
    assert cache_name
    assert year
    assert (month is not None)
    if not now:
        now = pendulum.now()
    if now.year == year and now.month == month:
        log.info("history_publishers: not looking at StatsCache")
    else:
        cache = StatsCache.search(name=cache_name, year=year, month=month)
        if cache:
            # return result, from JSON.
            try:
                res = cache.raw_data
                if from_json:
                    res = json.loads(res)
                log.info("history_publishers: got a cache for {} on {}-{}".format(cache_name, year, month))
                return res, True

            except Exception as e:
                log.error("history_publishers: got a cached result but could not extract JSON: {}".format(e))

    return None, False


def history_publishers(year=None, month=None):
    """
    Before this function is run, we have a memory cache.

    We want to cache this somewhat heavy computation in DB for old months,
    so that all future accesses will be fast, and so than we can compute global stats
    by reading all the cached data quickly.

    If year and month are not the current ones, check for saved data in StatsCache.
    And if not, we save them.

    Return: dict.
    """
    assert year
    assert (month is not None)

    now = pendulum.now()
    # Look for saved history in StatsCache.
    cache_name = "history_publishers"
    res, found = maybe_read_db_cache(now=now, cache_name=cache_name,
                                     year=int(year), month=int(month),
                                     from_json=True)
    if res and found:
        log.info("history_publishers: returning cached data")
        return res

    sells = Sell.sells_of_month(month=month, year=year)

    sells_with_publishers = sells.filter(card__publishers__isnull=False)

    current_publishers = []
    if sells_with_publishers:
        current_publishers = sells_with_publishers.values_list('card__publishers__name', 'card__publishers__id')
        current_publishers = list(set(current_publishers))


    res = _build_history_suppliers(
        current_distributors=current_publishers,
        sells_with_distributor=sells_with_publishers,
        filter_sells_with_target_fn=_filter_sells_with_publishers,
    )

    # Shall we save this data in cache?
    # If year and month are not today.
    # For this case, we have a memory cache of +/- a day.
    debug = False
    # debug = True
    maybe_save_to_db_cache(now=now, cache_name=cache_name, year=year, month=month,
                           to_json=True,
                           debug=debug,
                           data=res)

    return res


@cached(cache=TTLCache(maxsize=8024, ttl=72000))  # 20 hours
def history_suppliers_with_cache(year=None, month=None):
    log.info("history_suppliers_with_cache: querying and populating cache")
    return history_suppliers(year=year, month=month)

def history_suppliers(year=None, month=None):
    """
    Total of sells by distributors for the given month.

    The previous version, unpublished for everybody, also showed
    totals for publishers… for books that were not taken into
    account in the totals for distributors.

    But computing the totals for all publishers is too long. And
    less interesting, so we don't show it.

    Return: a dict with keys
    - distributors_data
    """
    assert year
    assert (month is not None)

    now = pendulum.now()

    # Look for saved history in StatsCache.
    cache_name = "history_suppliers"
    res, found = maybe_read_db_cache(now=now, cache_name=cache_name,
                                     year=int(year), month=int(month),
                                     from_json=True)
    if res and found:
        log.info("history_suppliers: returning cached data")
        return res

    # No cache found, normal execution.
    sells = Sell.sells_of_month(month=month, year=year)

    # Consider sells of distributors.
    sells_with_distributor = sells.filter(card__distributor__isnull=False)

    # And sells for publishers.
    # Well, this dummy algo for all publishers takes too long.
    # The most important is data for distributors, so let's keep this.
    # sells_with_publishers = sells.filter(card__publishers__isnull=False)

    # Without any pub or dist:
    # sells_without_supplier = sells.exclude(card__publishers__isnull=False)\
    #                              .exclude(card__distributor__isnull=False)

    current_distributors = []
    if sells_with_distributor:
        current_distributors = sells_with_distributor.values_list('card__distributor__name', 'card__distributor__id').distinct()  # distinct has no effect?!
        current_distributors = list(set(current_distributors))

    # current_publishers = []
    # if sells_with_publishers:
    #     current_publishers = sells_with_publishers.values_list('card__publishers__name', 'card__publishers__id').distinct()
    #     current_publishers = list(set(current_publishers))

    # Find a faster way and cache data for previous months.
    # (this below is too slow)
    #
    # for name, pk in current_publishers:
    #     data = {}
    #     data['publisher'] = (name, pk)
    #     pub_sells = sells_with_publishers.filter(card__publishers__id=pk)
    #     data['sells'] = pub_sells
    #     cards_sold = pub_sells.values_list('quantity', flat=True)
    #     nb_cards_sold = sum(cards_sold)
    #     data['nb_cards_sold'] = nb_cards_sold
    #     prices_sold = pub_sells.values_list('price_sold', flat=True)
    #     public_prices = pub_sells.values_list('price_init', flat=True)
    #     assert len(cards_sold) == len(prices_sold)
    #     total = sum([cards_sold[i] * prices_sold[i] for i in range(len(prices_sold))])
    #     data['total'] = total
    #     data['total_fmt'] = price_fmt(total, default_currency)
    #     try:
    #         total_public_price = sum([public_prices[i] or 0 * cards_sold[i] for i in range(len(cards_sold))])
    #     except Exception as e:
    #         log.error("Error getting total_public_price: {}".format(e))
    #         total_public_price = -1
    #     data['total_public_price'] = total_public_price
    #     data['total_public_price_fmt'] = price_fmt(total_public_price, default_currency)
    #     publishers_data.append(data)

    # publishers_data = sorted(publishers_data, key=lambda it: it['publisher'][0])  # sort by name

    res = _build_history_suppliers(
        current_distributors=current_distributors,
        sells_with_distributor=sells_with_distributor,
        filter_sells_with_target_fn=_filter_sells_with_distributor,
    )

    # Shall we save this data in cache?
    # If year and month are not today.
    # For this case, we have a memory cache of +/- a day.
    debug = False
    # debug = True
    maybe_save_to_db_cache(now=now, cache_name=cache_name, year=year, month=month,
                           to_json=True,
                           debug=debug,
                           data=res)

    return res

def _filter_sells_with_publishers(sells_with_distributor, pk):
    return sells_with_distributor.filter(card__publishers__id=pk)

def _filter_sells_with_distributor(sells_with_distributor, pk):
    return sells_with_distributor.filter(card__distributor__id=pk)

def _build_history_suppliers(current_distributors=[],
                             sells_with_distributor=None,
                             filter_sells_with_target_fn=None):
    """
    Get: the number of books sold by supplier (distributor/publisher), the total revenue.

    Return: dict.
    """

    default_currency = Preferences.get_default_currency()
    distributors_data = []

    for name, pk in current_distributors:
        data = {}
        data['distributor'] = (name, pk)

        # Filter by either distributor or publishers.
        pub_sells = filter_sells_with_target_fn(sells_with_distributor, pk)

        # no need of this list of sells in the returned results!
        # data['sells'] = pub_sells
        cards_sold = pub_sells.values_list('quantity', flat=True)
        nb_cards_sold = sum(cards_sold)
        data['nb_cards_sold'] = nb_cards_sold
        prices_sold = pub_sells.values_list('price_sold', flat=True)
        public_prices = pub_sells.values_list('price_init', flat=True)
        assert len(cards_sold) == len(prices_sold) == len(public_prices)
        total = sum([cards_sold[i] * prices_sold[i] for i in range(len(prices_sold))])
        data['total'] = total
        data['total_fmt'] = price_fmt(total, default_currency)
        try:
            total_public_price = sum([public_prices[i] or 0 * cards_sold[i] for i in range(len(cards_sold))])
        except Exception as e:
            log.error("Error getting total_public_price of distributors: {}".format(e))
            total_public_price = -1
        data['total_public_price'] = total_public_price
        data['total_public_price_fmt'] = price_fmt(total_public_price, default_currency)
        distributors_data.append(data)

    distributors_data = sorted(distributors_data, key=lambda it: it['distributor'][0])  # sort by name

    # The same data, in a simpler list, for the chart.
    chart_distributors_data = []
    for it in distributors_data:
        chart_distributors_data.append(
            [
                it['distributor'][0],  # name
                it['total_public_price'],
            ]
        )
    if chart_distributors_data:
        chart_distributors_data = json.dumps(chart_distributors_data)

    return {
        'distributors_data': distributors_data,
        'chart_distributors_data': chart_distributors_data,
    }
