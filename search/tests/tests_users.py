#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Test the user models.
"""
from __future__ import unicode_literals

import datetime

from django.test import TestCase
from factory.django import DjangoModelFactory

from search.models import Basket
from search.models import InstitutionBasket
from search.models import Bill
# from search.models import Card
from search.models import CardType
from search.models import Client
from search.models import Preferences
from search.models import Reception
from search.models import Reservation
from search.models import Sell

from search.models import users

from tests_models import CardFactory
from tests_models import PlaceFactory


class PreferencesFactory(DjangoModelFactory):
    # XXX: copied from tests_models (circular imports).
    class Meta:
        model = Preferences
    default_place = PlaceFactory()
    vat_book = "5.5"

class TestBill(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book", vat=5.5)
        self.bill = Bill(ref="987",
                         name="bill test",
                         due_date=datetime.date.today(),
                         total=100,
                    )
        self.bill.save()

    def tearDown(self):
        pass

    def test_create(self):
        bill = Bill(ref="987",
                    name="test bill",
                    due_date=datetime.date.today(),
                    total=100,
                    )
        self.assertTrue(bill)

    def test_add_copy(self):
        created = self.bill.add_copy(CardFactory.create(), nb=2)
        self.assertTrue(created)
        self.assertEqual(self.bill.billcopies_set.first().quantity, 2)

class TestClient(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book", vat=5.5)

        self.client = Client(name="Vincent Ô client test", discount=9)
        self.client.save()
        self.assertTrue(self.client.repr())
        self.assertTrue(self.client.to_dict())
        self.assertTrue(self.client.get_absolute_url())
        # a Card
        self.card = CardFactory.create()
        # the command Basket
        self.to_command = Basket.objects.create(name="auto_command").save()

    def tearDown(self):
        pass

    def test_nominal(self):
        self.assertTrue(Client.search("test"))

    def test_get_reservations(self):
        """
        Test our queryset, with and without payments, is correct.
        """
        # 1 ongoing
        resa_no_payment = Reservation.objects.create(  # noqa
            client=self.client,
            card=self.card,
        )
        # 2 ongoing
        resa_no_payment_ready = Reservation.objects.create(  # noqa
            client=self.client,
            card=self.card,
            is_ready=True,
        )
        # nope
        resa_payment_not_ready = Reservation.objects.create(  # noqa
            client=self.client,
            card=self.card,
            payment_origin="tests",
            is_ready=False,
        )
        # 3 ongoing
        resa_payment_ready = Reservation.objects.create(  # noqa
            client=self.client,
            card=self.card,
            payment_origin="tests",
            is_ready=True,
        )
        # nope
        resa_archived = Reservation.objects.create(  # noqa
            client=self.client,
            card=self.card,
            archived=True,
        )
        self.assertEqual(3, len(Reservation.get_reservations()))

    def test_reserve_in_stock(self):
        # This card is 1 in stock:
        self.card.quantity = 1
        self.card.save()
        self.assertEqual(self.card.quantity, 1)
        # it is not in the command list:
        self.assertEqual(self.card.quantity_to_command(), 0)

        # Reserve: we create a reservation object.
        # We WILL decrement the quantity in stock.
        resa, created = self.client.reserve(self.card)
        self.assertTrue(resa)
        self.assertEqual(self.card.quantity, 0)
        self.assertEqual(self.card.quantity_to_command(), 0)

    def test_reserve_in_stock_2(self):
        """The same, with quantities greater than 1."""
        # This card is 1 in stock:
        self.card.quantity = 10
        self.card.save()

        self.assertEqual(self.card.quantity, 10)
        # it is not in the command list:
        self.assertEqual(self.card.quantity_to_command(), 0)

        # Reserve: we create a reservation object.
        # We WILL decrement the quantity in stock.
        resa, created = self.client.reserve(self.card, nb=2)
        self.card.refresh_from_db()
        self.assertTrue(resa)
        self.assertEqual(self.card.quantity, 8)
        self.assertEqual(self.card.quantity_to_command(), 0)

    def test_reserve_not_in_stock(self):
        """Reserve a card not in stock."""
        # This card is -1 in stock:
        self.card.quantity = -1
        self.card.save()
        self.assertEqual(self.card.quantity, -1)
        # it is not in the command list:
        self.assertEqual(self.card.quantity_to_command(), 0)

        # Reserve: x2
        # We WILL decrement the quantity in stock
        # and we ONLY command 2.
        resa, created = self.client.reserve(self.card, nb=2)
        self.card.refresh_from_db()
        self.assertTrue(resa)
        self.assertEqual(self.card.quantity, -3)
        self.assertEqual(self.card.quantity_to_command(), 2)

    def test_reserve_not_all_in_stock(self):
        "We reserve more than we have in stock."
        # This card is 1 in stock:
        self.card.quantity = 1
        self.card.save()
        self.assertEqual(self.card.quantity, 1)
        # it is not in the command list:
        self.assertEqual(self.card.quantity_to_command(), 0)

        # Reserve: 2 copies.
        # We WILL decrement the quantity in stock,
        # and add 1 in the command.
        resa, created = self.client.reserve(self.card,
                                            nb=2,
                                            # debug=True,
                                            )
        self.assertTrue(resa)
        self.card.refresh_from_db()
        self.assertEqual(self.card.quantity, -1)
        self.assertEqual(self.card.quantity_to_command(), 1)

    def test_reserve_not_all_in_stock_2(self):
        "We reserve more than we have in stock, several times."
        # 0) as previously:
        # This card is 1 in stock:
        self.card.quantity = 1
        self.card.save()
        self.assertEqual(self.card.quantity, 1)
        # it is not in the command list:
        self.assertEqual(self.card.quantity_to_command(), 0)
        # Reserve: 2 copies.
        # We WILL decrement the quantity in stock,
        # and add 1 in the command.
        resa, created = self.client.reserve(self.card, nb=2)
        self.assertTrue(resa)
        self.card.refresh_from_db()

        self.assertEqual(self.card.quantity, -1)
        self.assertEqual(self.card.quantity_to_command(), 1)

        # 1) reserve again, we command more.
        resa, created = self.client.reserve(self.card, nb=2)
        self.assertEqual(self.card.quantity, -1 - 2)
        self.assertEqual(self.card.quantity_to_command(), 1 + 2)


    def test_reserve(self):
        """
        Test the reservations.
        There is a difference between reserving a card that was not in stock and one in stock.
        """
        # 0)
        # This card is not in stock:
        self.assertEqual(self.card.quantity, 0)
        # it is also not in the command list:
        self.assertEqual(self.card.quantity_to_command(), 0)

        # 1)
        # Reserve: we create a reservation object.
        # We will NOT decrement the quantity in stock (rewrite).
        # => NOW WE DO
        resa, created = self.client.reserve(self.card)
        self.assertTrue(resa)
        self.assertEqual(self.card.quantity, -1)
        # We have 1 in the command list (because the quantity was 0 before the reservation).
        self.assertEqual(self.card.quantity_to_command(), 1)

        # let's not do this here because it creates confusion with multiple resa objects
        # for the same client. Though the use case should be fixed.
        # # 1) (bis)
        # # Do it again: reserve, check the quantity in the command list.
        # # We should have 2 in command.
        # # <2024-09-12> we don't try to be clever on the quantity to command,
        # # adding the required number to have 0 in stock again. No, do what the user asks.
        # resa, created = self.client.reserve(self.card)
        # self.assertEqual(self.card.quantity, -2)
        # self.assertEqual(self.card.quantity_to_command(), 2)

        # 2) Find open reservations.
        resas = Reservation.get_card_reservations(self.card)
        self.assertTrue(resas)

        # 3) Cancel
        # put one in stock back only if we had one before so in that case, don't.
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, 0)
        self.assertEqual(self.card.quantity_to_command(), 0)

        # No more open reservations for this card.
        resas = Reservation.get_card_reservations(self.card.id)
        self.assertFalse(resas)

        # Somehow we reserve a card and add it to the command list.
        # Then somehow its quantity becomes > 1 (+1 button).
        # When we cancel the reservation, the card is removed from the command.
        # quantity is 1.
        self.card.quantity = 1
        self.card.save()
        # reserve
        resa, created = self.client.reserve(self.card)
        self.assertEqual(self.card.quantity, 0)
        self.assertEqual(self.card.quantity_to_command(), 0)
        # Add 2 in stock. Quantity: 2.
        self.card.quantity += 2
        self.card.save()
        self.assertEqual(self.card.quantity, 2)
        self.assertEqual(self.card.quantity_to_command(), 0)
        # cancel reservation.
        # It should put back 1 in stock.
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, 3)
        # quantity in command is still at 0.
        self.assertEqual(self.card.quantity_to_command(), 0)

    def test_reserve_multiple(self):
        """
        Reserve many copies of the same book.
        """
        # Preamble:
        # This card is not in stock:
        self.assertEqual(self.card.quantity, 0)
        # it is also not in the command list:
        self.assertEqual(self.card.quantity_to_command(), 0)

        # Reserve:
        # we create a reservation for 2 copies at once.
        resa, created = self.client.reserve(self.card, nb=2)
        self.assertTrue(resa)

        # We decremented the quantity in stock accordingly.
        self.assertEqual(self.card.quantity, -2)
        # We see 2 copies in the list of commands.
        # (this asks the Basket auto command list)
        self.assertEqual(self.card.quantity_to_command(), 2)

        # Client reservations see 2 copies too.
        # There is 1 reservation for the client, of 2 quantities:
        resas = Reservation.get_card_reservations(self.card.id)
        self.assertEqual(self.card.quantity_to_command(), 2)
        self.assertEqual(1, len(resas))
        self.assertEqual(2, resas[0].nb)

    def test_reserve_receive_sell(self):
        """
        - reserve a book
        - receive it
        - put it aside
        - sell it.

        When we reserve a book not in stock, then receive it, then sell it to the client: we should have 0 in stock.
        """
        # This card is not in stock.
        self.assertEqual(self.card.quantity, 0)
        # Reserve: we create a reservation object.
        # We will NOT decrement the quantity in stock (rewrite).
        # => we do now. <2023-05-25 Thu>
        resa, created = self.client.reserve(self.card)
        self.assertTrue(resa)
        self.assertEqual(self.card.quantity, -1)

        # Find open reservations.
        resas = Reservation.get_card_reservations(self.card)
        self.assertTrue(resas)

        # Add one copy with the reception.
        reception = Reception.ongoing()
        status, msgs = reception.add_copy(self.card.pk)
        self.card.refresh_from_db()

        self.assertEqual(self.card.quantity, 0)
        # The quantity in stock is still 0 (and not -1 as we did at the first iteration).
        # We still have open reservations for it.
        # <2023-05-25 Thu> it's BACK from -1 to 0.
        resas = Reservation.get_card_reservations(self.card)
        self.assertTrue(resas)

        # Now we click "OK" to notify the client, so we "put it aside".
        # The quantity is back to 0 (and 1 reservation).
        users.Reservation.putaside(self.card, self.client)
        # <2023-05-25 Thu> No need to click on OK now ?!
        self.assertEqual(self.card.quantity, 0)
        # The reservation is ready and the client is notified.
        resa = resas[0]
        resa.refresh_from_db()  # THE METHOD I VE BEEN LOOKING FOR !^^
        self.assertTrue(resa.is_ready)
        self.assertTrue(resa.notified)

        # Sell it.
        sell, status, msgs = Sell.sell_card(self.card, client=self.client)
        self.assertEqual(self.card.quantity, 0)
        resas = Reservation.get_card_reservations(self.card)
        self.assertFalse(resas)

        #
        # We do the same except that the card is now in stock:
        # - quantity: 1
        # - after reservation, we are at 0 too (and not 1, although that might be simpler…)
        # - because we had it in stock, we are NOT awaiting it from the reception.
        # - after reception and "OK", we have 1 in stock and 1 reserved.
        # - after sell, 1 in stock.
        #
        self.card.quantity = 1
        self.card.save()
        self.assertEqual(self.card.quantity, 1)
        # Reserve: we create a reservation object.
        # We DO decrement the quantity in stock (rewrite).
        # But why? Because we thought it is a good idea,
        # and it's simpler for ABStock and the websites API.
        resa, created = self.client.reserve(self.card)
        self.assertTrue(resa)
        self.assertEqual(self.card.quantity, 0)
        # Find open reservations.
        resas = Reservation.get_card_reservations(self.card)
        self.assertTrue(resas)

        # Add one copy with the reception.
        # The quantity in stock is back to 1: quantity = 1, reserved = 1.
        reception = Reception.ongoing()
        status, msgs = reception.add_copy(self.card.pk)
        self.card.refresh_from_db()

        self.assertEqual(self.card.quantity, 1)
        # We still have open reservations for it.
        resas = Reservation.get_card_reservations(self.card)
        self.assertTrue(resas)
        self.assertEqual(1, len(resas))

        # The client was already notified and the reservation is ready.
        # There is no client to click "OK" for
        # and no need to call users.Reservation.putaside(self.card, self.client)
        resa = resas.first()
        self.assertTrue(resa.is_ready)
        self.assertTrue(resa.notified)

        # Sell it: quantity still 1, reserved 0
        sell, status, msgs = Sell.sell_card(self.card, client=self.client)
        self.assertEqual(self.card.quantity, 1)
        resas = Reservation.get_card_reservations(self.card)
        self.assertFalse(resas)

    def test_cancel_reservation_init(self):
        # quantity starts at 0
        # We reserve 1, quantity -> -1
        resa, created = self.client.reserve(self.card)
        self.assertEqual(self.card.quantity, -1)
        self.assertEqual(self.card.quantity_to_command(), 1)
        # We cancel, everything should be back at 0.
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, 0, "After cancelation, the quantity in stock should be back to 0.")
        self.assertEqual(self.card.quantity_to_command(), 0)

    def test_cancel_reservation_with_stock(self):
        # quantity starts at 1
        self.card.quantity = 1
        self.card.save()
        # We reserve 1, quantity -> 0
        resa, created = self.client.reserve(self.card)
        self.assertEqual(self.card.quantity, 0)
        self.assertEqual(self.card.quantity_to_command(), 0)
        # We cancel, stock should be back at 1.
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, 1)
        self.assertEqual(self.card.quantity_to_command(), 0)

    def test_cancel_reservation_negative_stock(self):
        # quantity starts at -1
        self.card.quantity = -1
        self.card.save()
        # We reserve 1, quantity -> -2
        resa, created = self.client.reserve(self.card)
        self.assertEqual(self.card.quantity, -2)
        self.assertEqual(self.card.quantity_to_command(), 1)
        # We cancel, stock should be back at -1,
        # and quantity to command to 0 (as changed at <2024-09-12>, we don't
        # over-command when we find a negative stock anymore).
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, -1)
        self.assertEqual(self.card.quantity_to_command(), 0)


    def test_cancel_reservation(self):
        # Same from init above, but start with 0.
        # reserve
        # quantity starts at 0
        # We reserve 1, quantity -> -1
        resa, created = self.client.reserve(self.card)
        self.assertEqual(self.card.quantity, -1)

        # 1 to command so far.
        self.assertEqual(self.card.quantity_to_command(), 1)
        # add 2 in stock: quantity -1 -> 1
        self.card.quantity += 2
        self.card.save()
        self.assertEqual(self.card.quantity, 1)
        # still 1 to command.
        self.assertEqual(self.card.quantity_to_command(), 1)
        # cancel reservation.
        self.client.cancel_reservation(self.card)
        # quantity in command is back to 0.
        self.assertEqual(self.card.quantity_to_command(), 0)
        self.assertEqual(self.card.quantity, 2)

    def test_cancel_reservation_multiple(self):
        #
        # We do the same starting with quantity = 2
        #
        self.card.quantity = 2
        self.card.save()
        # We reserve 1 in stock: we decrement the quantity. quantity = 1
        resa, created = self.client.reserve(self.card)
        self.assertEqual(self.card.quantity, 1)
        # we don't need to command it.
        self.assertEqual(self.card.quantity_to_command(), 0)
        # cancel reservation, so we add 1 in stock.
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, 2)
        self.assertEqual(self.card.quantity_to_command(), 0)

        #
        # We do the same, we reserve 2, then cancel the 2.
        #
        resa, created = self.client.reserve(self.card, nb=2)
        self.assertEqual(self.card.quantity, 0)
        # we don't need to command it.
        self.assertEqual(self.card.quantity_to_command(), 0)
        # cancel reservation, so we add 2 in stock.
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, 2)

        #
        # The same with more quantities, we reserve 10, then cancel the 10.
        #
        # Starting quantity is 2.
        self.assertEqual(self.card.quantity, 2)
        resa, created = self.client.reserve(self.card, nb=10)
        self.assertEqual(self.card.quantity, -8)
        # we need to command it.
        self.assertEqual(self.card.quantity_to_command(), 8)
        # cancel reservation, so we add 10 in stock.
        self.client.cancel_reservation(self.card)
        self.assertEqual(self.card.quantity, 2)
        self.assertEqual(self.card.quantity_to_command(), 0)

    def test_cancel_after_sell(self):
        # Reserve, sell and cancel reservation.
        # We should cancel the command done because of this reservation.
        # quantity starts at 0.
        #
        # We reserve a card not in stock: this adds it to the command.
        self.assertEqual(self.card.quantity, 0)
        self.assertEqual(self.card.quantity_to_command(), 0)
        resa, created = self.client.reserve(self.card)
        self.card.refresh_from_db()

        self.assertEqual(self.card.quantity, -1)
        self.assertEqual(self.card.quantity_to_command(), 1)
        # We sell it (but not to the client).
        Sell.sell_card(self.card)

        self.card.refresh_from_db()

        self.assertEqual(self.card.quantity, -2)
        self.assertEqual(self.card.quantity_to_command(), 1)
        # Then we cancel the reservation, that should cancel the command.
        self.client.cancel_reservation(self.card)

        self.card.refresh_from_db()
        self.assertEqual(self.card.quantity, -1)
        # quantity to command is back to 0.
        self.assertEqual(self.card.quantity_to_command(), 0)

    def test_putaside(self):
        # bare bones: no reservation.
        status, msgs = Reservation.putaside(self.card, self.client)
        self.assertFalse(status)
        self.assertTrue(msgs)

        # With an ongoing reservation.
        resa, created = self.client.reserve(self.card)
        status, msgs = Reservation.putaside(self.card, self.client)
        self.assertTrue(status)
        self.assertFalse(msgs)

    def test_reserve_and_sell(self):
        # Reserve: create a reservation object, decrement quantity:
        # from 0 to -1.
        resa, created = self.client.reserve(self.card)
        resas = Reservation.get_card_reservations(self.card)
        self.assertTrue(resas)

        self.card.refresh_from_db()

        # Sell: don't decrement it again, archive reservations.
        self.assertEqual(self.card.quantity, -1)
        Sell.sell_card(self.card, client=self.client)
        self.assertEqual(self.card.quantity, -1)

        # No more open reservations.
        resas = Reservation.get_card_reservations(self.card)
        self.assertFalse(resas)

        # Sell again: all normal.
        Sell.sell_card(self.card)
        self.card.refresh_from_db()

        self.assertEqual(self.card.quantity, -2)

    def test_mobilephone_for_sms(self):
        self.client.mobilephone = "0609090909"
        self.client.save()
        self.assertTrue(self.client.mobilephone_for_sms())
        self.assertEqual('+33609090909', self.client.mobilephone_for_sms())

        self.client.mobilephone = "0033609090909"
        self.client.save()
        self.assertTrue(self.client.mobilephone_for_sms())
        self.assertEqual('+33609090909', self.client.mobilephone_for_sms())

        self.client.country = "Spain"
        self.client.mobilephone = "0033609090909"
        self.client.save()
        self.assertTrue(self.client.mobilephone_for_sms())
        # unchanged:
        self.assertEqual('0033609090909', self.client.mobilephone_for_sms())


class TestInstitutionBasket(TestCase):
    """
    ./manage.py test search.tests.tests_users.TestInstitutionBasket

    JSON sent:

    (jojo:to-json (dict
                  "client" (dict "uuid" "xyz" "id" ID "name" "my client")
                  "list" (dict "id" "1")
                  "data" (list "978xyz,1;978abc,3")))
    "{\"client\":{"id": 106, \"uuid\":\"xyz\",\"name\":\"my client\"},\"list\":{\"id\":\"1\"},\"data\":[\"978xyz,1;978abc,3\"]}"

    """

    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book", vat=5.5)
        self.card = CardFactory()
        # self.shelf = ShelfFactory()
        # A client
        self.client = Client(name="Vincent éèà")
        self.client.save()
        # A default place in Preferences
        self.preferences = PreferencesFactory()
        self.preferences.default_place = PlaceFactory()
        self.preferences.save()

    def tearDown(self):
        pass

    def test_initial(self):
        basket = InstitutionBasket(
            client=self.client,
            raw_data="9782757837009,1;9789461614728,3",
        )

        self.assertTrue(basket.raw_data)
