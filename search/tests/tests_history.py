#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

from django.test import TestCase
# from search.models import Card
from search.models import CardType
from search.models import Entry
from search.models import OutMovement

from tests_models import BasketFactory
from tests_models import CardFactory
from tests_models import DistributorFactory

class TestEntry(TestCase):

    def setUp(self):
        self.card = CardFactory()

    def test_entry(self):
        en, created = Entry.new([self.card])
        self.assertTrue(created)


class TestOutMovement(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book")
        # 3 cards in stock.
        self.card = CardFactory()
        self.card.quantity = 1
        self.card.save()

        self.card2 = CardFactory()
        self.card2.quantity = 1
        self.card2.save()

        self.card3 = CardFactory()
        self.card3.quantity = 1
        self.card3.save()

        # 1 card not registered in stock.
        self.card4 = CardFactory()
        self.basket = BasketFactory()
        self.distributor = DistributorFactory()

        # Add 3 cards to the basket:
        # - the 2 in stock
        # - and also the 4th one not in stock
        # "not in stock": its placecopies object can have been deleted when quantity=0.
        self.basket.add_cards([self.card, self.card2])
        self.basket.add_cards([self.card4])
        self.basket.distributor = self.distributor

    def tearDown(self):
        pass

    def test_return_basket(self):
        # preliminary check of the stock.
        self.assertEqual(1, self.card.quantity)

        # Create the return.
        obj, msgs = self.basket.create_return()
        # (yes, inconsistency to return the Message object)
        self.assertEqual('success', msgs.status)
        self.assertTrue(obj)
        self.assertEqual(3, obj.copies.count())
        self.assertTrue(self.card3 not in obj.copies.all())
        self.assertEqual(1, len(OutMovement.returns()))

        self.card.refresh_from_db()
        self.card2.refresh_from_db()
        self.card3.refresh_from_db()
        self.card4.refresh_from_db()

        self.assertEqual(0, self.card.quantity)
        self.assertEqual(0, self.card2.quantity)
        self.assertEqual(1, self.card3.quantity)

        # the card not in stock was *also* decremented.
        self.assertEqual(-1, self.card4.quantity)
