#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

import datetime
from django.core.urlresolvers import reverse
import mock
from pprint import pprint  # noqa: F401
from django.test import TestCase
from django.test.client import Client

from search.models.common import PAYMENT_CHOICES
from search.models import Author
from search.models import Basket
from search.models import Card
from search.models import CardType
from search.models import Distributor
from search.models import InstitutionBasket
# from search.models import Publisher
from search.models import Sell
from search.models import Shelf
from search import views
from search.views import get_reverse_url
from search.views import _save_bookshop_preferences

from tests_models import CardFactory
from django.contrib import auth

fixture_search_datasource = {
    "test search":
    [{"title": 'fixture',
      "isbn": "111",
      "details_url": "http://fake_url.com",
      "data_source": "chapitre"
  }],
    "éé":
    [{"title": "éé",
      "isbn": "222",
  }]
}

session_mock_data = {
    "search_result": {
        "emma gold":
        [{"title": 'fixture',
       "isbn": "111",
       "details_url": "http://fake_url.com",
       "data_source": "chapitre"
   }],
        "éé":
        [{"title": "éé",
       "isbn": "222",
   }]
    }}

fixture_no_isbn = {"test search":
                  [{"title": "fixture no isbn",
                    "details_url": "http://fake_url",
                    "data_source": "chapitre"  # must fetch module's name
                }]}

fake_postSearch = {"isbn": "111"}

def bulk_mock(isbns):  # noqa
    return "hello"


class TestLogin(TestCase):

    def setUp(self):
        self.c = Client()

    def test_login_no_user(self):
        resp = self.c.get(reverse("card_search"))
        self.assertEqual(resp.status_code, 302)

    def test_login(self):
        self.user = auth.models.User.objects.create_user(username="admin", password="admin")
        self.c.login(username="admin", password="admin")
        resp = self.c.get(reverse("card_search"))
        self.assertEqual(resp.status_code, 200)

    def test_logout(self):
        self.user = auth.models.User.objects.create_user(username="admin", password="admin")
        self.c.login(username="admin", password="admin")
        self.c.get(reverse("logout"))
        resp = self.c.get(reverse("card_search"))
        self.assertEqual(resp.status_code, 302)
        self.assertTrue("login/?next" in resp.url)

    # def test_login_required(self):
    #     """Login is required for all the views of the "search" app.
    #     """
    #     for pattern in search_urls.urlpatterns:
    #         if pattern.name:
    #             # this approach doesn't work cause some views require
    #             # arguments. Still, we might do sthg about it.
    #             resp = self.c.get(reverse(pattern.name))
    #             self.assertEqual(resp.status_code, 302)

class TestViews(TestCase):
    def setUp(self):

        self.type_book = CardType(name="book", vat=5.5)
        self.type_book.save()
        typ = CardType(name="other")
        typ.save()

        self.GOLDMAN = "Emma Goldman"
        self.goldman = Author(name=self.GOLDMAN)
        self.goldman.save()
        self.autobio = Card(title="Living my Life", isbn="123", price=9.99)
        self.autobio.save()
        self.autobio.authors.add(self.goldman)
        self.autobio.add_card()

        #
        # Create a basket
        #
        # Reminder: objects are better be created in the setUp.
        # If we create them in a test function, the change won't be commited to the DB,
        # unless we commit() the transaction, which requires to change the TestCase.
        # from django.db import transaction and then commit(), but not enough.
        #
        #
        self.basket = Basket(name="test basket")
        self.basket.save()
        self.basket.add_copy(self.autobio, nb=1)

        # Have some Sell history.
        Sell.sell_card(self.autobio)

        self.c = Client()
        # see also test fixtures https://docs.djangoproject.com/en/1.7/topics/testing/tools/#django.test.TransactionTestCase.fixtures

        # self.user = auth.models.User.objects.create_user(username="admin", password="admin")
        # Create a SUPERUSER for is_staff=True
        self.user = auth.models.User.objects.create_superuser(
            username="admin", password="admin", email="admin@admin.com")
        self.c.login(username="admin", password="admin")

    def post_to_view(self, isbn=None):
        post_params = {}
        if isbn:
            post_params["isbn"] = isbn
        return self.c.post(reverse("card_sell"), post_params)

    def test_sell_isbn_doesnt_exist(self):
        resp = self.post_to_view(isbn="9876")
        self.assertTrue(resp)
        pass  # test views with RequestFactory

    def test_sell_isbn(self):
        resp = self.post_to_view(isbn="123")
        self.assertTrue(resp)

    def test_sell_no_isbn_in_post(self):
        resp = self.post_to_view()
        self.assertTrue(resp)

    def test_card_create_manually(self):
        self.c.get(reverse("card_create_manually"))
        # Form not valid:
        self.c.post(
            reverse("card_create_manually"),
            {
                "title": "xyz",
                "publisher": "xyz",
                "price": "10",
            }
        )

        # Form valid:
        # (see fields in forms.py)
        data = {
            # 'card_type': None,  #*
            'title': "test",
            'has_isbn': None,
            'isbn': None,
            # 'currency': None, #*
            'price': 10,
            'data_source': "test",
            # 'selling_price': None,
            # 'price_bought': None,
            # 'vat': None,
            # 'shelf': None,
            # 'authors': None,
            # 'publishers': None,
            # 'distributor': None,
            'collection': None,
            'cover': "http://cover.png",
            # 'year_published': None,
            # 'date_publication': None,
            'threshold': 3,
            'summary': "test",
            'comment': "test",
        }

        res = self.c.post(
            reverse("card_create_manually"),
            data,
        )
        self.assertTrue(res)


    def test_simple_get_urls(self):
        """
        Useless GET requests for a few % of code coverage.
        """
        self.c.get(reverse("stats_rotation"))
        self.c.get(reverse("cards_unsold"))
        self.c.get(reverse("card_show", args=(1,)))
        self.c.get(reverse("card_history", args=(1,)))
        self.c.get(reverse("history_today"))
        self.c.get(reverse("history_sells"))

        now = datetime.datetime.now()
        this_month_fmt = "{}-{}".format(now.year, now.month)
        this_day_fmt = "{}-{}-{}".format(now.year, now.month, now.day)
        self.c.get(reverse("history_sells_month", args=(this_month_fmt,)))
        self.c.get(reverse("history_sells_day", args=(this_day_fmt,)))
        self.c.get(reverse("history_restocking_day", args=(this_day_fmt,)))
        self.c.get(reverse("history_entries_month", args=(this_month_fmt,)))
        self.c.get(reverse("history_entries_day", args=(this_day_fmt,)))

        self.c.get(reverse("suppliers_sells"))
        self.c.get(reverse("suppliers_sells_month", args=(this_month_fmt,)))
        self.c.get(reverse("publishers_sells"))
        self.c.get(reverse("publishers_sells_month", args=(this_month_fmt,)))
        self.c.get(reverse("publishers_sells_month_list", args=(1, this_month_fmt,)))
        self.c.get(reverse("distributors_sells_month_list", args=(1, this_month_fmt,)))

        self.c.get(reverse("collection_export"),
                   {
                       'query': "autobio",
                       'select': "selection",
                       'format': "csv",
                    }
                   )
        self.c.get(reverse("collection_exports"))
        self.c.get(reverse("all_exported_bills"))

        self.c.get(reverse("card_collection"))
        self.c.get(reverse("card_sell"))
        self.c.get(reverse("sell_details", args=(1,)))
        self.c.get(reverse("basket_auto_command"))
        self.c.get(reverse("command_supplier", args=("all",)))
        self.c.get(reverse("basket_auto_command_validate"))
        self.c.post(reverse("basket_auto_command_validate"))
        self.c.get(reverse("basket_auto_command_empty"))
        self.c.post(reverse("basket_auto_command_empty"))

        self.c.get(reverse("baskets"))
        # self.c.get(reverse("basket_view", args=(1,)))
        self.c.get(reverse("boxes"))
        self.c.get(reverse("supplier_returns"))

        self.assertTrue(self.c.get(reverse("basket_view", args=(1,))))
        self.assertTrue(self.c.get(reverse("exported_bill_download", args=("bill.pdf",))))
        self.assertTrue(self.c.get(reverse("collection_view_file_export", args=("bill.pdf",))))

        self.assertTrue(views.get_best_shelves(year=2020))
        self.assertTrue(views.get_stock_years_range())
        self.assertTrue(self.c.get(reverse("best_shelves")))
        self.assertTrue(self.c.get(reverse("dashboard")))
        self.assertTrue(self.c.get(reverse("catalogue_selection")))
        self.assertTrue(self.c.get(reverse("catalogue_excluded")))

        self.assertTrue(self.c.get(reverse("reservations")))

        self.assertTrue(self.c.get(reverse("commands_view", args=(1,))))


    def test_basket_export(self):

        # csv listing
        data = {'shippingreceipt': True, 'format': "csv", 'report': "listing"}
        self.assertTrue(self.c.get(
            reverse("basket_export", args=(1,)),
            data,
        ))

        # csv simplelisting
        data = {'shippingreceipt': True, 'format': "csv", 'report': "simplelisting"}
        self.assertTrue(self.c.get(
            reverse("basket_export", args=(1,)),
            data,
        ))

        # txt
        data = {'shippingreceipt': True, 'format': "txt", 'report': "listing"}
        self.assertTrue(self.c.get(
            reverse("basket_export", args=(1,)),
            data,
        ))

        # PDF
        data = {'shippingreceipt': True, 'format': "pdf", 'report': "listing"}
        self.assertTrue(self.c.get(
            reverse("basket_export", args=(1,)),
            data,
        ))

        # PDF bill
        data = {'shippingreceipt': True, 'format': "pdf", 'report': "bill", 'discount': "9"}
        self.assertTrue(self.c.get(
            reverse("basket_export", args=(1,)),
            data,
        ))

        # NEXT: bill from an inventory basket -> deprecated.


    def store_in_session(self, key, val):
        # doc:
        # > To modify the session and then save it, it must be stored in a variable first (because a new SessionStore is created every time this property is accessed).
        session = self.c.session
        session[key] = val
        session.save()
        return val

    def test_cards_set_supplier(self):
        url = "cards_set_supplier"
        # no card ids to set the supplier for: redirect
        res = self.c.get(reverse(url))
        self.assertEqual(res.status_code, 302)

        # some card ids
        self.store_in_session('set_supplier_cards_ids', "1,2")
        res = self.c.get(reverse(url))
        self.assertEqual(res.status_code, 200)

        #
        # No distributor in DB => forms invalid.
        #
        # POST (invalid form)
        self.store_in_session('set_supplier_cards_ids', "1,2")
        data = {
            'supplier': 1,  # not a good for the web form.
        }
        res = self.c.post(reverse(url), data)
        self.assertEqual(res.status_code, 406)

        # POST (other param, still invalid form)
        self.store_in_session('set_supplier_cards_ids', "1,2")
        data = {
            'discount': 9,
        }
        res = self.c.post(reverse(url), data)
        self.assertEqual(res.status_code, 406)

        #
        # Now create a distributor in DB.
        #
        self.dist = Distributor(name="dist test").save()

        # POST (valid form)
        self.store_in_session('set_supplier_cards_ids', "1,2")
        data = {
            'supplier': 1,
        }
        res = self.c.post(reverse(url), data)
        self.assertEqual(res.status_code, 302)
        self.assertTrue("en/collection" in res.url)

        # POST (other param, valid form)
        self.store_in_session('set_supplier_cards_ids', "1,2")
        data = {
            'name': "new test dist",
            'discount': 9,
        }
        res = self.c.post(reverse(url), data)
        self.assertEqual(res.status_code, 302)
        self.assertTrue("en/collection" in res.url)

    def test_cards_set_shelf(self):
        url = "cards_set_shelf"

        # no card ids in session: redirect
        res = self.c.get(reverse(url))
        self.assertEqual(res.status_code, 302)

        # some card ids and GET: nothing to do
        self.store_in_session('set_shelf_cards_ids', "1,2")
        res = self.c.get(reverse(url))
        self.assertEqual(res.status_code, 200)

        #
        # No shelf in DB => forms invalid.
        #
        # POST (invalid form)
        self.store_in_session('set_supplier_cards_ids', "1,2")
        data = {
            'shelf': 1,
        }
        res = self.c.post(reverse(url), data)
        self.assertEqual(res.status_code, 406)

        #
        # Valid shelf name
        #
        # POST
        self.store_in_session('set_supplier_cards_ids', "1,2")
        data = {
            'name': "new shelf",
        }
        res = self.c.post(reverse(url), data)
        self.assertEqual(res.status_code, 302)
        self.assertTrue("/en/collection" in res.url)

        #
        # Create a shelf in DB, valid form.
        #
        self.shelf = Shelf(name="test shelf").save()
        # same
        self.store_in_session('set_supplier_cards_ids', "1,2")
        data = {
            'shelf': 1,
        }
        res = self.c.post(reverse(url), data)
        self.assertEqual(res.status_code, 302)
        self.assertTrue("/en/collection" in res.url)


@mock.patch('search.views_utils.bulk_import_from_dilicom', side_effect=bulk_mock)
class TestGenericImport(TestCase):
    def setUp(self):

        self.type_book = CardType(name="book", vat=5.5)
        self.type_book.save()
        self.card = Card(isbn='9780000000456', title="the missing isbn for generic import").save()
        self.basket = Basket(name="test basket")
        self.basket.save()

        self.institution = InstitutionBasket(name="InstitutionBasket test")
        self.institution.save()

        self.c = Client()
        # see also test fixtures https://docs.djangoproject.com/en/1.7/topics/testing/tools/#django.test.TransactionTestCase.fixtures

        # self.user = auth.models.User.objects.create_user(username="admin", password="admin")
        # Create a SUPERUSER for is_staff=True
        self.user = auth.models.User.objects.create_superuser(
            username="admin", password="admin", email="admin@admin.com")
        self.c.login(username="admin", password="admin")

    def store_in_session(self, key, val):
        session = self.c.session
        session[key] = val
        session.save()
        return val

    def test_generic_import(self, mock):
        # the mock doesn't work :(
        # mock.assert_any_call()
        # => AssertionError: bulk_import_from_dilicom() call not found
        url = reverse("generic_import")
        self.c.get(url)
        self.c.get(url, {'source': "basket", 'source_pk': 1})

        # les isbns: bof y'a 2 lignes de code que je n'arrive pas à couvrir en code coverage.
        data = {
            'inputrows': """9780000000123,99,
            9780000000999,100""",
        }
        self.c.post(url)
        self.c.post(url, data)

        # An ISBN found by the bulk dilicom search,
        # the other exists in DB.
        data = {
            'inputrows': """9780000000123 99,
            9780000000456 100""",
        }
        self.c.post(url, data)


    def test_import_validate(self, mock):  # noqa unused mock here
        url = reverse("import_validate")

        res = self.c.get(url)  # 1 line of coverage, simply redirect.
        self.assertEqual(res.status_code, 302)

        data = {}
        res = self.c.post(url, data)
        self.assertEqual(res.status_code, 406)

        #
        # no more data = session expired?
        #
        data = {'source': 'basket'}
        res = self.c.post(url, data)

        #
        # Missing source_pk
        #
        # NB: once we put data in the session, it stays here.
        # The view doesn't delete it, we don't delete it here.
        # self.store_in_session(session_name, {})
        data = {'source': 'basket',
                }
        print("----- test with no source_pk…")
        res = self.c.post(url, data)
        self.assertEqual(res.status_code, 406)
        # self.assertTrue("import" in res.url)  # a normal render response doesn't have .url

        #
        # Bad source name.
        #
        self.store_in_session("import_basket_1",
            {'dicts':
             [{'isbn': "9780000000123",}],
             'isbns_quantities': [("9780000000123", 10)],  # list of tuples
             }
        )
        data = {'source': 'bad_source_name',   # bad source name
                'source_pk': 1,
                }
        print("----- test with bad source name…")
        res = self.c.post(url, data)
        self.assertEqual(res.status_code, 406)

        #
        # no "dicts" in session (but the session data was not deleted right? coverage seems ok though)
        #
        session_name = "import_basket_1"
        self.store_in_session(session_name, {'not empty': True})
        data = {'source': 'basket',
                'source_pk': 1,
                }
        print("----- test with no 'dicts' in session…")
        res = self.c.post(url, data)
        self.assertEqual(res.status_code, 406)

        #
        # no "isbns_quantities"
        #
        self.store_in_session("import_basket_1",
            {'dicts':
             [{'isbn': "9780000000123",}],
             # 'isbns_quantities': [("9780000000123", 10)],  # missing
             }
        )
        data = {'source': 'basket',
                'source_pk': 1,
                }
        print("----- test with no 'isbns_quantities' in session…")
        res = self.c.post(url, data)
        self.assertEqual(res.status_code, 406)

        #
        # Enough data to run the import.
        #
        print("----- test with enough data…")
        self.store_in_session("import_basket_1",
            {'dicts':
             [{'isbn': "9780000000123",}],
             'isbns_quantities': [("9780000000123", 10)],  # list of tuples
             }
        )
        data = {'source': 'basket',
                'source_pk': 1,
                }
        res = self.c.post(url, data)
        self.assertEqual(res.status_code, 302)
        self.assertTrue("baskets/1" in res.url)

        #
        # Same OK, for an institution.
        #
        self.store_in_session("import_institution_2",
            {'dicts':
             [{'isbn': "9780000000123",}],
             'isbns_quantities': [("9780000000123", 10)],  # list of tuples
             }
        )
        data = {'source': 'institution',  # institution
                'source_pk': 2,
                }
        self.assertEqual(res.status_code, 302)
        res = self.c.post(url, data)
        self.assertTrue(hasattr(res, 'url'), "import for institution failed: our response object should be a redirect with a 'url' key.")
        self.assertTrue("institution" in res.url)


class TestSells(TestCase):
    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book", vat=5.5)
        self.c = Client()
        self.user = auth.models.User.objects.create_user(username="admin", password="admin")
        self.c.login(username="admin", password="admin")

    def populate(self):
        self.card = CardFactory.create()
        p1 = 7.7
        ids_prices_quantities = [{"id": self.card.id,
                    "quantity": 1,
                    "price_sold": p1,
                    }
                  ]
        sell, status, msgs = Sell.sell_cards(ids_prices_quantities,
                                             payment=1, payment_2=2,
                                             total_payment_1=5,
                                             total_payment_2=2.5,
                                             )

    def test_sell_details_no_sell(self):
        resp = self.c.get(reverse("sell_details", args=(1,)))
        self.assertTrue(resp)

    def test_sell_details(self):
        self.populate()
        resp = self.c.get(reverse("sell_details", args=(self.card.id,)))
        self.assertTrue(resp.status_code, "200")

    def test_history_sells_day(self):
        self.populate()

        now = datetime.datetime.now()
        # this_month_fmt = "{}-{}".format(now.year, now.month)
        this_day_fmt = "{}-{}-{}".format(now.year, now.month, now.day)

        res = self.c.get(reverse("history_sells_day", args=(this_day_fmt,)))
        self.assertEqual(200, res.status_code)

        res = views._history_sells_day(now)

        # result:
        #
        #        {u'best_sells': {u'book': [(<Card: 1:card title é and ñ 0, , editor: , distributor: none>,
        #                            1)]},
        # u'data': [(<SoldCards: card sold id 1, 1 sold at price 7.7>,
        #            u'',
        #            '',
        #            True,
        #            u'1')],
        # u'data_grouped_sells': [],
        # u'sells_data': {u'books_sell_mean': 7.7,
        #                 u'data': [<SoldCards: card sold id 1, 1 sold at price 7.7>],
        #                 u'nb_books_sells': 1,
        #                 u'nb_books_sold': 1,
        #                 u'nb_cards_returned': 0,
        #                 u'nb_cards_sold': 1,
        #                 u'nb_sells': 1,
        #                 u'not_books': [],
        #                 u'sell_mean': 7.7,
        #                 u'sell_mean_fmt': u'7,70\xa0\u20ac',
        #                 u'total_price_sold': 7.7,
        #                 u'total_price_sold_books': 7.7,
        #                 u'total_price_sold_books_fmt': u'7,70\xa0\u20ac',
        #                 u'total_price_sold_fmt': u'7,70\xa0\u20ac',
        #                 u'total_price_sold_not_books': 0,
        #                 u'total_price_sold_not_books_fmt': u'0,00\xa0\u20ac',
        #                 u'total_sells': 1,
        #                 u'total_sells_fmt': u'1,00\xa0\u20ac'},
        # u'total_per_payment_items': []}


        self.assertEqual(res['sells_data']['total_sells'], 1)
        self.assertEqual(res['sells_data']['books_sell_mean'], 7.7)
        self.assertTrue(res['total_per_payment_items'])



@mock.patch('search.views_utils.search_on_data_source', return_value=(fixture_search_datasource, []))
class TestSearchView(TestCase):

    def setUp(self):
        self.c = Client()
        self.user = auth.models.User.objects.create_user(username="admin", password="admin")
        self.c.login(username="admin", password="admin")

    def get_for_view(self, cleaned_data, url_name="card_search"):
        """Use our view utility to get the reverse url with encoded query parameters.
        """
        cleaned_data["source"] = "chapitre"
        # get the reverse url with encoded paramaters
        return self.c.get(get_reverse_url(cleaned_data, url_name))

    def test_search_no_query_params(self, search_mock):
        resp = self.get_for_view({})
        self.assertTrue(resp)
        self.assertEqual(resp.status_code, 200)

    def test_search_with_isbn(self, search_mock):
        resp = self.get_for_view({"isbn": "123"})
        self.assertTrue(resp)
        self.assertEqual(resp.status_code, 200)

    def test_search_with_keywords(self, search_mock):
        data = {"q": "emma gold"}
        resp = self.get_for_view(data)
        self.assertTrue(resp)
        self.assertEqual(resp.status_code, 200)

    def test_search_params_not_valid(self, search_mock):
        data = {"foo": "bar"}
        resp = self.get_for_view(data)
        self.assertTrue(resp)
        self.assertEqual(resp.status_code, 200)


@mock.patch('search.views_utils.search_on_data_source', return_value=fixture_search_datasource)
class TestAddView(TestCase):

    def setUp(self):
        # create an author
        self.GOLDMAN = "Emma Goldman"
        self.goldman = Author(name=self.GOLDMAN)
        self.goldman.save()
        # create a Card
        self.fixture_isbn = "987"
        self.fixture_title = "living my life"
        self.autobio = Card(title=self.fixture_title, isbn=self.fixture_isbn, price=9.99)
        self.autobio.save()
        self.autobio.authors.add(self.goldman)
        # Create a distributor
        self.dist = Distributor(name="dist test")
        self.dist.save()
        # mandatory: unknown card type
        typ = CardType(name="unknown")
        typ.save()
        # create other card types
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.autobio.add_card()

        self.c = Client()

        self.user = auth.models.User.objects.create_superuser("admin", "admin@test.test", "admin")
        self.c.login(username="admin", password="admin")

    def test_card_places_add(self, mock):
        """
        Tests the "Add…" button on a Card page.
        (not actually to places)
        """
        url_name = "card_places_add"
        res = self.c.get(reverse(url_name, args=(self.autobio.pk,)))
        # self.assertTrue(res.status)

        res = self.c.post(
            reverse(url_name, args=(self.autobio.pk,)),
            {'quantity': '10'},
        )
        # The form redirects us to the book page.
        maybe_error_message = None
        if hasattr(res, 'error_message'):
            maybe_error_message = res.error_message
        self.assertEqual(302, res.status_code, "The view status_code was not 302 but {}, an error happened in the view: {}".format(res.status_code, maybe_error_message))
        self.assertTrue("/stock/card/1" in res.url)

class TestPreferences(TestCase):

    def setUp(self):
        self.c = Client()
        self.user = auth.models.User.objects.create_superuser("admin", "admin@test.test", "admin")
        self.c.login(username="admin", password="admin")

    def tearDown(self):
        pass

    def test_save_preferences(self):
        # needing fake request… that sucks.
        data = {}  # stuff from clean_data
        self.assertTrue(_save_bookshop_preferences(data))
        # Does nothing, only redirects.
        self.c.get(reverse("preferences_bookshop"))

        # POST
        res = self.c.post(
            reverse("preferences_bookshop"),
            {'name': "me",
             'firstname': "metoo",
             'mobilephone': "09999",
             'company_number': "09999",
             'bank_IBAN': "IBAN",
             'bank_BIC': "BIC",
             'is_vat_exonerated': True,
             'comment': "it's all tests",
             # XXX: more
             },
        )
        self.assertEqual(302, res.status_code)
        self.assertTrue("/en/preferences" in res.url)

    def test_preferences(self):
        # GET: get models, instantiate form.
        res = self.c.get(reverse("preferences"))

        # Post data.
        res = self.c.post(
            reverse("preferences"),
            {'name': "foo",
             'sell_discounts': "0;5;9;30",
             'logo_url': "http://nope.com",
             'default_discounts': "0;5",
             'auto_command_after_sell': True,
             },
        )
        self.assertEqual(302, res.status_code)


# class TestImportView(TestCase):

#     def setUp(self):
#         # create an author
#         self.GOLDMAN = "Emma Goldman"
#         self.goldman = Author(name=self.GOLDMAN)
#         self.goldman.save()
#         # create a Card
#         self.fixture_isbn = "9782918059363"
#         self.fixture_title = "living my life"
#         self.autobio = Card(title=self.fixture_title, isbn=self.fixture_isbn, price=9.99)
#         self.autobio.save()
#         self.autobio.authors.add(self.goldman)
#         # mandatory: unknown card type
#         typ = CardType(name="unknown")
#         typ.save()
#         # create other card types
#         self.type_book = "book"
#         typ = CardType(name=self.type_book)
#         typ.save()
#         # Basket.
#         self.basket = Basket(name="some basket")
#         self.basket.save()

#         self.c = Client()

#         self.user = auth.models.User.objects.create_superuser("admin", "admin@test.test", "admin")
#         self.c.login(username="admin", password="admin")

#     @mock.patch('search.views_utils.bulk_import_from_dilicom', side_effect=bulk_mock)
#     def test_import_view(self, mock_data_source):
#         # mock not working?
#         inputrows = """// comment 1
#                      // comment 1
#                      //
#                      // comment 3
#                      9782918059363 ; 99
#                      9782918059363;77
#         """
#         post_params = {}
#         post_params['source'] = "basket"
#         post_params['source_pk'] = 1
#         post_params['source_name'] = "some basket"
#         post_params['inputrows'] = inputrows

#         resp = self.c.post(reverse("generic_import"), post_params)

#         # We got redirected because all went well:
#         self.assertEqual(resp.status_code, 302)
#         self.assertEqual(resp.url, 'http://testserver/en/baskets/1/##1')

class TestTicketZ(TestCase):

    def setUp(self):
        # create an author
        self.GOLDMAN = "Emma Goldman"
        self.goldman = Author(name=self.GOLDMAN)
        self.goldman.save()

        # Card types: book and unknown type.
        self.type_book = CardType(name="book", vat=5.5)
        self.type_book.save()
        typ = CardType(name="other")
        typ.save()

        # create a Card (book), id 1
        self.fixture_isbn = "987"
        self.fixture_title = "living my life"
        self.autobio = Card(title=self.fixture_title,
                            isbn=self.fixture_isbn,
                            price=9.99,
                            card_type=self.type_book)
        self.autobio.save()
        self.autobio.authors.add(self.goldman)
        # Card with type "other", id 2.
        self.other = Card(title="other card",
                          isbn="4444",
                          price=10,
                          card_type=typ)
        self.other.save()
        # Card with no type (unknown), id 3.
        self.unknown = Card(title="unknown card type",
                            isbn="999",
                            price=3,
                            card_type=None)
        self.unknown.save()

        # Create a distributor
        self.dist = Distributor(name="dist test")
        self.dist.save()

        self.autobio.add_card()

        #### For sells
        # Payment means.
        # Create a first Sell.
        Sell.sell_cards([{"id": "1", "price_sold": 10, "quantity": 1}],
                        payment=PAYMENT_CHOICES[0][0],
                        payment_2=PAYMENT_CHOICES[1][0],
                        total_payment_1=6,
                        total_payment_2=2,
                        total_payment_3=2,
                        )

    def tearDown(self):
        pass

    def test_vat_stats(self):
        now = datetime.datetime.now()
        #################################################
        # first pass.
        res = Sell.search(year=now.year)
        vat_stats = views._stats_by_vat(res['data'])
        # [(20, 10)] = (vat, total)
        self.assertEqual(10, vat_stats[0][1])

        #################################################
        # Sell again with a coupon.
        Sell.sell_cards([{"id": "1", "price_sold": 10, "quantity": 2}],
                        payment=100,  # ignore for revenue.
                        # payment_2=PAYMENT_CHOICES[1][0],
                        total_payment_1=10,
                        # total_payment_2=2,
                        )
        res = Sell.search(year=now.year)
        vat_stats = views._stats_by_vat(res['data'])
        # Same as earlier: we count only one sell for the revenue stats.
        self.assertEqual(10, vat_stats[0][1])

        #################################################
        # Sell again with a coupon AND a real payment mean.
        Sell.sell_cards([{"id": "1",
                          "price_sold": 10,
                          # "quantity": 99,
                          }],
                        # the quantity doesn't impact us here
                        # since we set the total per payment method.
                        payment=1,
                        payment_2=100,
                        payment_3=100,
                        total_payment_1=6,
                        total_payment_2=2,
                        total_payment_3=2,
                        )
        res = Sell.search(year=now.year, with_total_price_sold=True)
        vat_stats = views._stats_by_vat(res['data'])
        # We have to count: 10 + nothing + 6 + nothing
        self.assertEqual(16, res['total_price_sold'])
        self.assertEqual(16, vat_stats[0][1])

        #################################################
        # 4) same as above, once more.
        Sell.sell_cards([{"id": "1", "price_sold": 10, "quantity": 1}],
                        payment=1,
                        payment_2=100,
                        payment_3=100,
                        total_payment_1=6,  # count only 6.
                        total_payment_2=2,
                        total_payment_3=2,
                        )
        res = Sell.search(year=now.year, with_total_price_sold=True)
        vat_stats = views._stats_by_vat(res['data'])
        # We have to count: (10 + nothing + 6) + again 6 for this
        self.assertEqual(22, res['total_price_sold'])
        self.assertEqual(22, vat_stats[0][1])

        #################################################
        # 5) Sell another card type: "other", VAT not book.
        Sell.sell_cards([{"id": "2", "price_sold": 10, "quantity": 1}],
                        payment=1,
                        payment_2=100,
                        payment_3=1,
                        total_payment_1=6,  # count only 6, for VAT not book.
                        total_payment_2=2,
                        total_payment_3=2,
                        )
        res = Sell.search(year=now.year, with_total_price_sold=True)
        vat_stats = views._stats_by_vat(res['data'])
        # a list of two: 5.5 and 20
        # We have to count: (10 + nothing + 6 + 6) + again 6
        self.assertEqual(28, res['total_price_sold'])
        self.assertEqual(22, vat_stats[0][1])  # type book, from previous sells.
        self.assertEqual(6 + 2, vat_stats[1][1])   # type other, from this sell.

        # 6) … was useless?

        #################################################
        # 7) A return and a normal sell: quantity -1 and quantity +1
        Sell.sell_cards([# {"id": "3", "price_sold": 1, "quantity": -1},
                         {"id": "1", "price_sold": 10, "quantity": 1}],  # +10 in total revenue
                        payment=1,
                        # payment_2=100,
                        # total_payment_1=6,  # count only 6.
                        # total_payment_2=4,
                        )
        res = Sell.search(year=now.year, with_total_price_sold=True,
                          sortorder=1,)
        vat_stats = views._stats_by_vat(res['data'])
        # We have to count: +10 and -1
        self.assertEqual(38, res['total_price_sold'])
        for stat in vat_stats:
            if stat[0] == 20:
                self.assertEqual(6 + 2, stat[1])
            elif stat[1] == 5.5:
                self.assertEqual(32, stat[1])
