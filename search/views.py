# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals

import datetime
import io  # write to file in utf8
import json
import multiprocessing
import os
import time
import traceback
import urllib

import dateparser
import pendulum
import toolz
import unicodecsv
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
# from django.core.paginator import EmptyPage
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import StreamingHttpResponse
# from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.template.loader import get_template
from django.utils import timezone
from django.utils.translation import ugettext as _  # not a good name though
from weasyprint import HTML

from abelujo import settings
from search import dilicom_command
from search import forms as viewforms
from search import mailer
from search import models
from search.datasources.bookshops.all.abebooks import abebooksScraper as abebooks  # noqa: F401
from search.datasources.bookshops.all.bookfinder import bookfinderScraper as bookfinder  # noqa: F401
#
# The datasources imports must have the name as their self.SOURCE_NAME
# Also add the search engine in the client side controller (searchResultsController).
#
from search.datasources.bookshops.all.discogs import discogsScraper as discogs  # noqa: F401
from search.datasources.bookshops.all.momox import momox  # noqa: F401
from search.datasources.bookshops.deDE.buchlentner import buchlentnerScraper as buchlentner  # noqa: F401
from search.datasources.bookshops.esES.agapea import agapeaScraper as agapea  # noqa: F401
from search.datasources.bookshops.frFR.chezmonlibraire import chezmonlibraireScraper as chezmonlibraire  # noqa: F401
from search.datasources.bookshops.frFR.filigranes import filigranesScraper as filigranes  # noqa: F401
from search.datasources.bookshops.frFR.lelivre import lelivreScraper as lelivre  # noqa: F401
from search.datasources.bookshops.frFR.librairiedeparis import librairiedeparisScraper as librairiedeparis  # noqa: F401
from search.datasources.bookshops.itIT.hoeplit import hoeplitScraper as hoeplit  # noqa: F401
from search.datasources.bookshops.nlNL.athenaeum import athenaeumScraper as athenaeum  # noqa: F401
from search.datasources.bookshops.nlNL.naibooksellers import naibooksellersScraper as naibooksellers  # noqa: F401
from search.datasources.bookshops.nlNL.bol import bolScraper as bol  # noqa: F401

# from search.models import Entry
# from search.models import Bill
from search.models import Barcode64
from search.models import Basket
from search.models import Card
from search.models import CardType
from search.models import Client
from search.models import Command
from search.models import Distributor
from search.models import EntryCopies
from search.models import InstitutionBasket
from search.models import Inventory
from search.models import InventoryCommand
from search.models import Place
from search.models import Preferences
from search.models import Publisher
from search.models import Sell
from search.models import Shelf
from search.models import SoldCards
from search.models import Stats  # the class
from search.models import bill_utils
from search.models import history
from search.models import stats  # the functions
from search.models import users
from search.models import utils
from search.models.api import _get_command_or_return
from search.models.common import ALERT_ERROR
from search.models.common import ALERT_SUCCESS
from search.models.common import ALERT_WARNING
from search.models.common import get_payment_abbr
from search.models.common import ignore_payment_for_revenue
from search.views_utils import DEFAULT_DATASOURCE
from search.views_utils import Echo
from search.views_utils import bulk_import_from_dilicom
from search.views_utils import cards2csv
from search.views_utils import dilicom_enabled
from search.views_utils import electre_enabled
from search.views_utils import extract_all_isbns_quantities
from search.views_utils import format_price_for_locale
from search.views_utils import is_htmx_request
from search.views_utils import is_official_datasource
from search.views_utils import to_short_date
from search.views_utils import update_from_dilicom_with_cache
from search.views_utils import update_from_electre_with_cache

# Extra, optional and private scripts:
try:
    from abextras import bertrandScraper as bertrand  # noqa: F401
    print("abextras scripts are loaded.")
except ImportError:
    # that's ok.
    pass




log = utils.get_logger()

DEFAULT_NB_COPIES = 1         # default nb of copies to add.

PENDULUM_YMD = '%Y-%m-%d'  # caution, %m is a bit different than datetime's %M.

EXTENSION_TO_CONTENT_TYPE = {
    'csv': 'text/csv',
    'txt': 'text/raw',
    'pdf': 'application/pdf',
}

def filename_content_type(filename):
    extension = filename.split('.')[-1]
    return EXTENSION_TO_CONTENT_TYPE[extension]


def get_reverse_url(cleaned_data, url_name="card_search"):
    """Get the reverse url with the query parameters taken from the
    form's cleaned data.

    query parameters:
    - source
    - q

    type cleaned_data: dict
    return: the complete url with query params

    >>> get_reverse_url({"source": "chapitre", "q": "emma goldman"})
    /search?q=emma+goldman&source=chapitre

    """

    qparam = {}
    qparam['source'] = cleaned_data.get("source")
    if "q" in list(cleaned_data.keys()):
        qparam['q'] = cleaned_data["q"]
    # construct the query parameters of the form
    # q=query+param&source=discogs
    params = urllib.urlencode(qparam)
    rev_url = reverse(url_name) + "?" + params
    return rev_url


@login_required
@staff_member_required
def preferences(request):
    """
    """
    template = "search/preferences.html"
    if request.method == 'GET':
        form = viewforms.PrefsForm()
        bs_model = users.Bookshop.objects.first()
        # note: we handle its POST on preferences_bookshop url.
        if bs_model:
            bookshopform = viewforms.BookshopForm(instance=bs_model)
        else:
            bookshopform = viewforms.BookshopForm()

        return render(request, template, {
            'form': form,
            'bookshopform': bookshopform,
        })

    elif request.method == 'POST':
        bookshopform = viewforms.BookshopForm()  # we handle it on preferences_bookshop
        form = viewforms.PrefsForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            prefs = Preferences.prefs()
            discounts = viewforms.validate_and_get_discounts(data['sell_discounts'])
            if discounts:
                data['sell_discounts'] = discounts
            if not discounts and Preferences.get_sell_discounts():
                data['sell_discounts'] = Preferences.get_sell_discounts()
            if prefs.default_discounts:
                data['default_discounts'] = prefs.default_discounts
            if not data.get('logo_url') and prefs.get_logo_url() and prefs.get_logo_url().strip():
                data['logo_url'] = prefs.get_logo_url()
            prefs.others = json.dumps(data)
            prefs.auto_command_after_sell = data.get('auto_command_after_sell')
            prefs.save()
            messages.add_message(
                request, messages.SUCCESS, _("Preferences saved."))
            form = viewforms.PrefsForm()
            return HttpResponseRedirect(reverse('preferences'))

        return render(request, template, {
            'form': form,
            'bookshopform': bookshopform,
        })

def _save_bookshop_preferences(data):
    """
    Save a User (Bookshop) object with this data.
    """
    # Damn…
    # m2m fields should not be there…
    if 'sms_history' in data:
        data.pop('sms_history')
    bookshop_model = users.Bookshop(**data)
    return bookshop_model

@login_required
@staff_member_required
def preferences_bookshop(request):
    """
    Intermediate view that is used by the Preferences page to save the
    bookstore personal and banking information.

    Only for POST requests.
    """
    # If we POST to preferences/, the currency&discount form is submitted as well,
    # hence its data is null, but valid, and we don't want to erase it.
    # It's simple to use another url. We could use hidden form fields.
    template = "search/preferences.html"
    if request.method == 'GET':
        return HttpResponseRedirect(reverse("preferences"))

    if request.method == 'POST':
        form = viewforms.BookshopForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            try:
                existing = users.Bookshop.objects.first()
                bookshop_model = _save_bookshop_preferences(data)
                if existing:
                    # mmh… update?
                    bookshop_model.pk = existing.pk
                    bookshop_model.created = existing.created
                else:
                    bookshop_model.created = pendulum.now()
                bookshop_model.save()
                messages.add_message(
                    request, messages.SUCCESS, _("Preferences saved."))
                return HttpResponseRedirect(reverse('preferences'))
            except Exception as e:
                log.error('Error saving the bookshop form: {}\n{}'.
                          format(e, traceback.format_exc))
                messages.add_message(
                    request, messages.ERROR, _("Preferences NOT saved."))

    form = viewforms.PrefsForm()
    bookshopform = viewforms.BookshopForm()
    return render(request, template, {
        'form': form,
        'bookshopform': bookshopform,
    })

def _session_result_set(request, key, val):
    """Set request.session['search_result'][key] to val.

    Needed to mock for unit tests. Otherwise, during a test, the
    session remains a void list and we have a key error in the code below.
    """
    request.session['search_result'][key] = val

def _stats_by_payment_mean(data):
    """
    Data: soldcards from sells_data['data']

    We want the total for all payment means, including coupons, even
    if we don't count them in the total. It's useful to know them at the end of the day.

    We add a parentheses as a visual clue to remind to not count this for the revenue:
    "coupon: 10.90" => "(coupon): 10.90"
    It's a minimal change, we don't want to change the data structure now.

    Return: a list, sorted by total, of tuples: payment mean abbreviation, total.
    """
    # Stats by payment mean.
    total_per_payment = {}  # payment human abbreviation -> total
    seen_sells_ids = []  # don't count the same soldcard.sell twice...
    for soldcard in data:
        if soldcard.sell.pk in seen_sells_ids:
            continue
        payment = soldcard.sell.payment  # int
        abbr = get_payment_abbr(payment) if payment else None
        if ignore_payment_for_revenue(payment):
            abbr = "({})".format(abbr)  # visual clue to not count this for total revenue.

        payment_2 = soldcard.sell.payment_2
        abbr_2 = get_payment_abbr(payment_2) if payment_2 else None
        if ignore_payment_for_revenue(payment_2):
            abbr_2 = "({})".format(abbr_2)

        payment_3 = soldcard.sell.payment_3
        abbr_3 = get_payment_abbr(payment_3) if payment_3 else None
        if ignore_payment_for_revenue(payment_3):
            abbr_3 = "({})".format(abbr_3)

        if abbr and abbr not in total_per_payment:
            # Count the payment in its category even if it's a coupon,
            # don't use ignore_payment_for_revenue here.
            total_per_payment[abbr] = 0
        if abbr:
            _total = soldcard.sell.total_payment_1
            if not _total:
                # XXX: is that an omission when we pay from a list?
                # we don't see total_payment_1
                _total = soldcard.sell.total_price_sold
            # total_per_payment[abbr] += soldcard.sell.total_payment_1
            total_per_payment[abbr] += _total

        if abbr_2:
            if abbr_2 not in total_per_payment:
                total_per_payment[abbr_2] = 0
            total_per_payment[abbr_2] += soldcard.sell.total_payment_2

        if abbr_3:
            if abbr_3 not in total_per_payment:
                total_per_payment[abbr_3] = 0
            if soldcard.sell.total_payment_3:  # can be None after migration. <2024-12-04>
                total_per_payment[abbr_3] += soldcard.sell.total_payment_3

        seen_sells_ids.append(soldcard.sell.pk)

    # Sort by total.
    total_per_payment_items = sorted(total_per_payment.items(),
                                     key=lambda x: x[1],
                                     reverse=True)

    # Round.
    res = []
    for tup in total_per_payment_items:
        elt = ["", 0]
        elt[0] = tup[0]
        elt[1] = utils.roundfloat(tup[1], rounding="ROUND_HALF_EVEN")
        res.append(elt)
    total_per_payment_items = res

    return total_per_payment_items

def _stats_by_vat(soldcards):
    """
    Numbers by VAT %.

    Return: list of tuples VAT %, total.
    """
    vat_to_total = {}
    seen_sells_ids = []
    vat_book = Preferences.get_vat_book()
    type_book = CardType.get_book_type()
    for soldcard in soldcards:
        if soldcard.sell.pk in seen_sells_ids:
            continue

        vat = None
        if soldcard.card and soldcard.card.vat:
            vat = soldcard.card.vat
        elif soldcard.card and soldcard.card.card_type:
            vat = soldcard.card.card_type.vat

        # This card has no card_type… should we guess it?
        # Take the VAT by default.
        if vat is None:
            vat = Preferences.get_vat_default()
            # What shall we do? This is the risk of having a number too big.
            # continue

        if vat is None and soldcard.card and soldcard.card.card_type and soldcard.card.card_type == type_book:
            vat = vat_book
        if vat is not None and vat not in vat_to_total:
            vat_to_total[vat] = 0

        # Sum the prices,
        # but ignore coupons and similar payment methods.
        # see also utils.total_price_sold
        # Do NOT count returns in the total revenue!
        if (vat is not None) or True:  # and soldcard.quantity >= 1:  # beware returns and exchanges
            if not ignore_payment_for_revenue(soldcard.sell.payment) \
               and not ignore_payment_for_revenue(soldcard.sell.payment_2):
                # TODO: avoid sells for deposits.
                vat_to_total[vat] += soldcard.quantity * soldcard.price_sold
                continue
            if not ignore_payment_for_revenue(soldcard.sell.payment):
                vat_to_total[vat] += soldcard.sell.total_payment_1
                seen_sells_ids.append(soldcard.sell.pk)  # yes? <2023-09-28 Thu>
                # continue

            if not ignore_payment_for_revenue(soldcard.sell.payment_2):
                vat_to_total[vat] += soldcard.sell.total_payment_2
                seen_sells_ids.append(soldcard.sell.pk)  # yes? <2023-09-28 Thu>

            if not ignore_payment_for_revenue(soldcard.sell.payment_3) and soldcard.sell.total_payment_3:
                # can be None after migration. <2024-12-04>
                vat_to_total[vat] += soldcard.sell.total_payment_3
                seen_sells_ids.append(soldcard.sell.pk)

        else:
            # print("---- IGNORING {}".format(soldcard))
            pass

    vat_items = vat_to_total.items()
    for i, item in enumerate(vat_items):
        vat_items[i] = (item[0], utils.roundfloat(item[1], rounding='ROUND_HALF_EVEN'))

    # Sort by total.
    total_per_vat_items = sorted(vat_items,
                                 key=lambda x: x[1],
                                 reverse=True)
    return total_per_vat_items

@login_required
def stats_rotation(request):
    template = "search/stats_rotation.html"
    if request.method == 'GET':
        data = stats.categories_ventilation(with_shelves=True)

        return render(request, template, {
            'stats': data,
        })

@login_required
def search(request):
    template = "search/searchresults.html"

    # List of clients (for modale)
    clients = [it.to_dict() for it in Client.objects.order_by("name").all()]

    return render(request, template, {
        "clients": clients,
        "default_datasource": DEFAULT_DATASOURCE,
    })

def cards_unsold(request):
    template = "search/cards_unsold.html"
    page_size = 200
    page = request.GET.get('page', 1)
    shelf_id = request.GET.get('shelf')
    if shelf_id:
        try:
            shelf_id = int(shelf_id)
        except Exception as e:
            log.debug("cards unsold error getting shelf: {}".format(e))

    shelves = Shelf.objects.order_by("name").all()

    distributor_pk = request.GET.get('distributor')
    if distributor_pk:
        distributor_pk = int(distributor_pk)
    distributors = Card.cards_unsold_distributors()

    if page:
        page = int(page)
    print("--- page", page)
    if page and page < 1:
        page = 1

    cards = Card.cards_unsold(page=page, page_size=page_size,
                              shelf_id=shelf_id,
                              distributor_pk=distributor_pk)
    cards_count = cards.count()
    total = Card.cards_unsold_total()

    paginator = Paginator(cards, page_size)
    if page and page > paginator.num_pages:
        page = paginator.num_pages
        print("--- page", page)

    if page is not None:
        try:
            cards = paginator.page(page)
        except paginator.EmptyPage:
            cards = paginator.page(paginator.num_pages)
        finally:
            cards = cards.object_list
    else:
        cards = paginator.object_list

    return render(request, template, {
        "cards": cards,
        "cards_count": cards_count,
        "shelf_id": int(shelf_id) if shelf_id else 0,
        "shelves": shelves,
        "distributors": distributors,
        "distributor_pk": distributor_pk,
        "page": page,
        "total": total,
        "paginator": paginator,
        "before_last_page": paginator.num_pages - 1,
    })

def cards_without_eans(request):
    """
    Show all cards without INBS, or where it is "".
    """
    template = "search/cards_without_eans.html"
    cards = Card.cards_without_eans()
    return render(request, template, {
        "cards": cards,
    })

@login_required
@staff_member_required
def card_create_manually(request):
    """
    Used to create a card manually.

    When we "add" a card from search results, see card_add.html and /api/card/<id>/add
    """
    template = 'search/card_create.html'
    if request.method == 'GET':
        card_form = viewforms.CardCreateForm()
        add_places_form = viewforms.CardPlacesAddForm()
        return render(request, template, {'form': card_form,
                                          'add_places_form': add_places_form})
    elif request.method == 'POST':
        card_form = viewforms.CardCreateForm(request.POST)
        add_places_form = viewforms.CardPlacesAddForm(request.POST)
        card = None

        if card_form.is_valid():
            card_dict = card_form.cleaned_data
            card, msgs = viewforms.CardCreateForm.create_card(card_dict)

            if not card:
                log.warning("create card manually: card not created? {}, {}"
                            .format(card_dict, msgs))
                messages.add_message(request, messages.SUCCESS, _('Warn: the card was not created.'))

        else:
            return render(request, template, {'form': card_form,
                                              'add_places_form': viewforms.CardPlacesAddForm()})

        if card and add_places_form.is_valid():
            places_qties = add_places_form.cleaned_data
            for name_id, qty in places_qties.items():
                if not qty:
                    continue
                if name_id:  # only 'quantity'
                    card.add_card(nb=qty)
                    log.info("Card added to stock x{}".format(qty))

            messages.add_message(request, messages.SUCCESS, _('The card was created successfully.'))

        else:
            return render(request, template, {'form': card_form,
                                              'add_places_form': add_places_form})

        # Return to… new void form?
        card_form = viewforms.CardCreateForm()
        add_places_form = viewforms.CardPlacesAddForm()
        return render(request, template, {'form': card_form,
                                          'add_places_form': add_places_form})

@login_required
def card_show(request, pk):
    template = "search/card_show.html"
    card = None
    sells = []
    places = Place.objects.order_by("id").all()
    places_quantities = []
    sold_since_last_entry = "0"
    messages = []  # unused
    if request.method == 'GET':
        card = get_object_or_404(Card, id=pk)

        # Avoid warn messages during tests.
        # Should we save those fields in DB anyways?
        card.availability = None
        card.availability_fmt = ""

        # Don't try to get an update from Dilicom if we got this book from
        # somewhere else.
        official_data_source = is_official_datasource(card.data_source) if card.data_source else None
        if not official_data_source:
            # XXX: unfinished… let's commit anyways bc I won't loose it
            # and I'm still alone.
            if os.getenv('ABELUJO_UNIT_TEST') != "1":
                log.info("--- {} is not an official_data_source, no need to try to update this card: {}".format(card.data_source if card.data_source else None, card.id))

        # Update critical data from Electre or Dilicom, if possible.
        if electre_enabled():
            try:
                card, msgs = update_from_electre_with_cache(card)
                messages.append(msgs)
            except Exception as e:
                log.warn(e)
        elif dilicom_enabled():
            try:
                # There is a 2s timeout on a Dilicom query,
                # but it would be much better to do this async.
                card, msgs = update_from_dilicom_with_cache(card)
                messages.append(msgs)
            except Exception as e:  # for ConnectionError
                log.warn("card show: Dilicom connection error: {}".format(e))

        # Ongoing commands.
        pending_commands = card.commands_pending()

        # Last time we entered this item
        last_entry = EntryCopies.last_entry(card)

        # Quantity per box.
        quantity_boxes = card.quantity_boxes()

        # Sells since the last entry
        if last_entry:
            res = Sell.search(card.id, date_min=last_entry.created)
            sold_since_last_entry = res['nb_cards_sold']

        # Total copies sold:
        total_copies_sold = card.total_copies_sold()

        # Last sell:
        # last_soldcard = card.soldcards_set.filter(sell__canceled=False).last()
        # There's a difference between the two: filtering or not a canceled sell.
        # But it's interesting to know that a book was sold
        # (despite being returned or *exchanged*), it shows an interest.
        # We use this second date for the age shelf, so let's be consistent.
        last_soldcard_date = card.last_soldcard_date
        last_soldcard_date_short = to_short_date(last_soldcard_date)

        # Nb sold last 12 months:
        total_sold_last_12_months = card.sold_last_months()
        total_sold_last_3_months = card.sold_last_months(months=3)

        # List of clients (for modale)
        clients = [it.to_dict() for it in Client.objects.order_by("name").all()]

        # Client reservations
        reservations = users.Reservation.get_card_reservations(pk)

        # Distributors
        if not hasattr(card, 'distributor') or not card.distributor:
            distributors = Distributor.objects.all()
        else:
            distributors = []

    return render(request, template, {
        "object": card,
        "sells": sells,
        "clients": clients,
        "distributors": distributors,
        # "shelves": Shelf.objects.order_by("name").all(),
        "places_quantities": places_quantities,
        "has_many_places": len(places) > 1,
        "quantity_boxes": quantity_boxes,
        "last_entry": last_entry,
        "sold_since_last_entry": sold_since_last_entry,
        "total_copies_sold": total_copies_sold,
        "total_sold_last_12_months": total_sold_last_12_months,
        "total_sold_last_3_months": total_sold_last_3_months,
        # "last_soldcard": last_soldcard,
        "last_soldcard_date": last_soldcard_date,
        "last_soldcard_date_short": last_soldcard_date_short,
        "age_dict": models.find_age_from_date(last_soldcard_date),
        "pending_commands": pending_commands,
        "page_title": "Abelujo - " + card.title[:50],
        "reservations": reservations,
        "has_one_reservation": reservations.count() == 1,

        # Feature flags.
        "feature_show_reservation_button": settings.FEATURE_SHOW_RESERVATION_BUTTON,
        "feature_exclude_for_website": settings.FEATURE_EXCLUDE_FOR_WEBSITE,
        "tags": models.Tag.objects.all(),
    })

def card_history(request, pk):
    """
    Show the card's sells, entries and commands history.
    """
    MAX = 100
    DATE_YMD = "%Y-%m-%d"
    card = None
    template = "search/card_history.html"

    if request.method == 'GET':
        card = get_object_or_404(Card, pk=pk)

        # Sells
        sells_data = Sell.search(card_id=card.id)
        canceled_sells = card.soldcards_set.filter(sell__canceled=True)[0:20]

        # OutMovements
        # TODO: get all out movements
        # outmovement_set: direct link
        # outmovementcopies_set: linked from baskets
        outs = card.outmovementcopies_set.order_by("-created").all()[:MAX]

        # Commands
        pending_commands = card.commands_pending()
        commands = card.commands_received()

        # Other entries
        entries = card.entrycopies_set.order_by('-created').all()[:MAX]
        first_entry = entries.last()  # last: because order by -created
        first_entry_date_fmt = ""
        if first_entry:
            first_entry_date_fmt = datetime.datetime.strftime(first_entry.created, DATE_YMD)

        # Time series, for JS chart.
        #
        # We want a format easy to give to c3.js:
        # x: [ date, date, date…]
        # sells: [ quantity, quantity, quantity…]
        #
        # But, get the sells in a dict first.
        #
        time_series_data = {}
        time_series = {}
        time_series_x = ["date"]  # do not translate, the JS side looks for a "date" label.
        time_series_sells = [_("Sells")]
        try:
            for soldcard in sells_data['data']:
                date = soldcard.created
                date_fmt = datetime.datetime.strftime(soldcard.created, DATE_YMD)
                quantity = soldcard.quantity
                if date_fmt not in time_series:
                    time_series[date_fmt] = quantity
                else:
                    time_series[date_fmt] += quantity

            # Add a marker for the first entry
            # (would be start of the time series).
            if first_entry_date_fmt and first_entry_date_fmt not in time_series:
                time_series[first_entry_date_fmt] = 0  # 0 sold. We don't show the entries yet.

            # Transform the dict to two lists (format for the JS chart).
            dates = time_series.keys()
            for date in dates:
                time_series_x.append(date)
                time_series_sells.append(time_series[date])

            # Add a marker for today at the end of the lists, if required,
            # so we always have today at the right of the chart.
            today = pendulum.today()
            today_fmt = datetime.datetime.strftime(today, DATE_YMD)
            if today_fmt not in time_series_x:
                time_series_x.append(today_fmt)
                time_series_sells.append(0)

            time_series_data = {'x': time_series_x, 'sells': time_series_sells}
            time_series_data = json.dumps(time_series_data)
        except Exception as e:
            log.warn("Card history: error while computing the sells time series for the chart: {}".format(e))

    return render(request, template, {
        "card": card,
        "sells_data": sells_data,
        "canceled_sells": canceled_sells,
        "entries": entries,
        "outs": outs,
        "pending_commands": pending_commands,
        "commands": commands,
        "time_series_data": time_series_data,
        "page_title": "Abelujo - {} - {}".format(_("History"), card.title),
    })


@login_required
@staff_member_required
def card_places_add(request, pk=None):
    """
    Add the given card to the stock.
    """
    template = "search/card_places_add.html"
    params = request.GET

    if request.method == 'GET':
        form = viewforms.CardPlacesAddForm()
        return render(request, template, {
            "form": form,
            "pk": pk,
            "type": params.get('type'),
        })

    else:
        back_to = reverse("card_show", args=(pk,))
        form = viewforms.CardPlacesAddForm(request.POST)
        if form.is_valid():
            # When the field name was an int (the place id),
            # the form was valid but cleaned_data had None values.
            for (label_id, nb) in form.cleaned_data.items():
                # label_id is now simply 'quantity', not the Place name.
                if label_id != "quantity":
                    log.warn("card_places_add: label should be 'quantity' and nothing else. It's changed.")
                if nb:
                    try:
                        card = Card.objects.filter(id=pk).first()
                        card.add_card(nb=nb)
                    except Exception as e:
                        log.error("Error adding card {} to stock: {}".format(pk, e))
                        # beware of "redirect" django function.
                        _redirect = HttpResponseRedirect(back_to)
                        _redirect.status_code = 304
                        _redirect.error_message = "{}".format(e)
                        return _redirect

        return HttpResponseRedirect(back_to)


@login_required
@staff_member_required
def cards_set_supplier(request, **kwargs):
    """
    When POST:
      if 'supplier' (id) is given, get the existing distributor.
      if 'discount' (int) is given, create a new distributor with also the 'name' param,

    and set the distributor for the cards given in the session (string, comma-separated ids).

    Redirect to My Stock (/collection/).
    """
    template = 'search/set_supplier.html'
    form = viewforms.SetSupplierForm()
    newsupplier_form = viewforms.NewSupplierForm()
    cards_ids = request.session.get('set_supplier_cards_ids')
    if not cards_ids:
        log.debug("no cards ids to set supplier for. Redirect.")
        return HttpResponseRedirect(reverse('card_collection'))

    cards_ids = cards_ids.split(',')
    response_dict = {
        'form': form,
        'newsupplier_form': newsupplier_form,
        'nb_cards': len(cards_ids),
    }

    if request.method == 'GET':
        return render(request, template, response_dict)

    elif request.method == 'POST':
        dist_id = None
        dist_obj = None
        req = request.POST.copy()

        # The user chose an existing distributor.
        if 'supplier' in list(req.keys()):
            form = viewforms.SetSupplierForm(req)
            if form.is_valid():
                dist_id = form.cleaned_data['supplier']
                dist_obj = Distributor.objects.get(id=dist_id)
            else:
                messages.add_message(request, messages.ERROR, _("The form is invalid."))
                return render(request, template, response_dict, status=406)

        # Create distributor.
        elif 'discount' in list(req.keys()):
            # needs 'name' param too.
            form = viewforms.NewSupplierForm(req)
            if form.is_valid():
                data = form.cleaned_data

                # Check existing name.
                existing = Distributor.objects.filter(name=data['name'])
                if existing:
                    messages.add_message(request, messages.ERROR, _("A supplier with the same name already exists."))
                    response_dict['messages'] = messages.get_messages(request)
                    return render(request, template, response_dict)

                try:
                    dist_obj = Distributor(name=data['name'], discount=data['discount'])
                    dist_obj.save()
                except Exception as e:
                    log.error("Could not create new distributor: {}".format(e))
                    messages.add_message(request, messages.ERROR, _("An internal error occured, we have been notified."))
                    response_dict['messages'] = messages.get_messages(request)
                    return render(request, template, response_dict, status=404)

            else:
                messages.add_message(request, messages.ERROR, _("The form is invalid."))
                return render(request, template, response_dict, status=406)

        else:
            log.error("Error in the form setting the supplier for many cards. We didn't recognize any of the two forms.")
            return render(request, template, response_dict)

        # Set supplier for all cards.
        try:
            cards = Card.objects.filter(pk__in=cards_ids)
            if cards:
                cards.update(distributor=dist_obj)
        except Exception as e:
            log.error("Error batch-setting the distributor: {}".format(e))
            messages.add_message(request, messages.ERROR, _("Internal error :("))
            response_dict['messages'] = messages.get_messages(request)
            return render(request, template, response_dict)

        messages.add_message(request, messages.SUCCESS, _("The supplier was correctly set for those {} cards.".format(len(cards_ids))))

        return HttpResponseRedirect(reverse('card_collection'))

@login_required
@staff_member_required
def cards_set_shelf(request, **kwargs):
    template = 'search/set_shelf.html'
    form = viewforms.SetShelfForm()
    newshelf_form = viewforms.NewShelfForm()
    cards_ids = request.session.get('set_shelf_cards_ids')

    if not cards_ids:
        return HttpResponseRedirect(reverse('card_collection'))

    cards_ids = cards_ids.split(',')
    response_dict = {
        'form': form,
        'newshelf_form': newshelf_form,
        'nb_cards': len(cards_ids),
    }

    if request.method == 'GET':
        return render(request, template, response_dict)

    elif request.method == 'POST':
        new_id = None
        new_obj = None
        req = request.POST.copy()

        # The user chose an existing shelf.
        if 'shelf' in list(req.keys()):
            form = viewforms.SetShelfForm(req)
            if form.is_valid():
                new_id = form.cleaned_data['shelf']
                new_obj = Shelf.objects.get(id=new_id)
            else:
                return render(request, template, response_dict, status=406)

        # Create new shelf.
        elif 'name' in list(req.keys()):
            form = viewforms.NewShelfForm(req)
            if form.is_valid():
                data = form.cleaned_data

                # Check existing name.
                existing = Shelf.objects.filter(name=data['name'])
                if existing:
                    messages.add_message(request, messages.ERROR, _("A shelf with the same name already exists."))
                    response_dict['messages'] = messages.get_messages(request)
                    return render(request, template, response_dict)

                try:
                    new_obj = Shelf(name=data['name'])
                    new_obj.save()
                except Exception as e:
                    log.error("Could not create new shelf: {}".format(e))
                    messages.add_message(request, messages.ERROR, _("An internal error occured, we have been notified."))
                    response_dict['messages'] = messages.get_messages(request)
                    return render(request, template, response_dict)

            else:
                messages.add_message(request, messages.ERROR, _("The form is invalid."))
                return render(request, template, response_dict)

        else:
            log.error("Error in the form setting the shelf for many cards. We didn't recognize any of the two forms.")
            return render(request, template, response_dict, status=406)

        # Set shelf for all cards.
        cards = Card.objects.filter(id__in=cards_ids)
        cards.update(shelf=new_obj)
        messages.add_message(request, messages.SUCCESS, _("The shelf was correctly set for those {} cards.".format(len(cards_ids))))

        return HttpResponseRedirect(reverse('card_collection'))

def is_big_stock():
    """
    Return True if we have an amount of cards that would make a
    complete .csv export too slow for the web.
    """
    MAX = 1000
    return Card.objects.count() > MAX

@login_required
def collection_export(request):
    if request.method == 'GET':

        by_email = request.GET.get('email')
        by_email = utils._is_truthy(by_email)
        if by_email:
            proc = multiprocessing.Process(target=do_collection_export, args=(request,))
            proc.start()
            messages.add_message(request, messages.SUCCESS,
                                 _("OK. The email should arrive in a few minutes."))
            return HttpResponseRedirect(reverse("card_collection"))

        return do_collection_export(request)


# def do_collection_export(request):
def do_collection_export(request):

    """
    Export a search of our stock.

    We disabled the possibility to export all of the stock, because
    the answer time is too long (and the browser kills the
    connection).
    => send by email <2024-11-29>.

    Instead, we must fire a search, with a free text search or with filters.

    - format: text, csv.
    - select: all, selection.
    """

    now = datetime.date.strftime(timezone.now(), '%Y-%m-%d %H:%M')

    if request.method == 'GET':
        by_email = request.GET.get('email')
        by_email = utils._is_truthy(by_email)
        formatt = request.GET.get('format')
        select = request.GET.get('select')
        query = request.GET.get('query')
        distributor = request.GET.get("distributor")
        distributor_id = request.GET.get("distributor_id")
        card_type_id = request.GET.get("card_type_id")
        publisher_id = request.GET.get("publisher_id")
        place_id = request.GET.get("place_id")
        shelf_id = request.GET.get("shelf_id")
        order_by = request.GET.get("order_by")
        quantity_choice = request.GET.get("quantity_choice")
        # bought = request.GET.get("in_stock")
        date_created_sort = request.GET.get("date_created_sort")  # string like ">="
        date_created = request.GET.get("date_created")  # string like "august 2022"
        if date_created:
            date_created = dateparser.parse(date_created)

        # If with_sells is truthy, add a column to count the number of sells
        # between two dates.
        with_sells = request.GET.get('with_sells')
        DATE_FORMAT_YMD = "%Y-%m-%d"
        date_sell_start = None
        date_sell_end = None
        date_sell_start_string = request.GET.get("date_sell_start")  # like 2023-04-04
        date_sell_end_string = request.GET.get("date_sell_end")  # like 2023-04-04
        if date_sell_start_string:
            date_sell_start = pendulum.strptime(date_sell_start_string, DATE_FORMAT_YMD)
        if date_sell_end_string:
            date_sell_end = pendulum.strptime(date_sell_end_string, DATE_FORMAT_YMD)

        # Export all the stock or a custom search ?
        # would rather validate request.GET and **
        # or call api's cards search and get the json.
        res = []
        too_broad = False
        if select == "selection":
            if query or any([distributor, distributor_id, card_type_id, publisher_id, place_id, shelf_id, quantity_choice, date_created]):
                query_list = query.split(" ")
                res, _discard = Card.search(query_list, to_list=True,
                                    distributor=distributor,
                                    distributor_id=distributor_id,
                                    publisher_id=publisher_id,
                                    card_type_id=card_type_id,
                                    place_id=place_id,
                                    shelf_id=shelf_id,
                                    quantity_choice=quantity_choice,
                                    date_created=date_created,
                                    date_created_sort=date_created_sort,
                                    order_by=order_by,
                                    with_authors=True,

                                    # If we want to count the number of sells
                                    # and we ask for a CSV file: compute the sells and
                                    # add a column.
                                    with_sells=date_sell_start is not None,
                                    date_sell_start=date_sell_start,
                                    date_sell_end=date_sell_end,

                                    in_deposits=True)
            else:
                # too_broad = True
                too_broad = is_big_stock()

        elif select == "all":
            # Used for email.
            # res = Card.objects.filter(in_stock=True).all()
            res = Card.cards_in_stock()

        if not os.path.exists(settings.EXPORTS_ROOT):
            os.makedirs(settings.EXPORTS_ROOT)

        # Which format ?
        content_type = ""
        if formatt == 'txt':
            content_type = "text/raw"
            content = utils.ppcard(res) if res else ""

        elif formatt == "csv":
            content_type = "text/csv"
            # Be careful. With to_list=True above, this should not be needed.
            if res and not isinstance(res[0], dict):
                res = [it.to_list() for it in res]
            content = cards2csv(res,
                                with_sells=with_sells,
                                date_sell_start_string=date_sell_start_string,
                                date_sell_end_string=date_sell_end_string,
                                )

        else:
            content = "This format isn't supported."

        if too_broad:
            content = _("Please filter your search.")

        # Save the file.
        filename_date = ""
        if with_sells:
            filename_date = "{}--{}".format(date_sell_start_string, date_sell_end_string)
        else:
            filename_date = "{}".format(now)
        filename = "abelujo-stock-{}.{}".format(filename_date, formatt)
        filepath = os.path.join(settings.EXPORTS_ROOT, filename)
        if formatt in ["csv", 'csv']:
            with open(filepath, 'w') as f:
                f.write(content)
        else:
            with io.open(filepath, 'w', encoding='utf8') as f:
                f.write(content)

        if by_email:
            log.debug("collection: sending email attaching file {}".format(filepath))
            mailer.send_email(
                subject=_("Abelujo: your file is ready"),
                attachment_file_path=filepath,
                attachment_file_name=filename,
                attachment_type=formatt,
            )
            log.debug("collection: mail normally sent.")
            return HttpResponseRedirect(reverse("card_collection"))

        # Build the response.
        response = HttpResponse(content, content_type=content_type)
        filename = "Abelujo stock search"
        if query:
            filename += " - {}".format(query)

        response['Content-Disposition'] = 'attachment; filename="{}.{}"'.format(filename, formatt)
        return response

    else:
        content = "no search query"

    return HttpResponse(content, content_type=content_type)

@login_required
def all_exported_bills(request):
    """
    View the exported bills in collectedstatic/.
    View all PDF files.
    """
    template = "search/collection_exports.html"
    all_files = os.listdir(settings.STATIC_ROOT)
    # We get all sort of files here lol:
    # vendor.js, directories…
    files = []
    for it in all_files:
        if ".pdf" in it:
            files.append(it)

    files = list(reversed(sorted(files)))
    return render(request, template, {"files": files})

@login_required
def exported_bill_download(request, filename):
    if filename:
        # filepath = os.path.join(settings.EXPORTS_ROOT, filename)
        filepath = os.path.join(settings.STATIC_ROOT, filename)
        content = ""
        content_type = ""
        if os.path.exists(filepath):
            with open(filepath, "r") as f:
                content = f.read()
                content_type = filename_content_type(filename)

        response = HttpResponse(content, content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response


@login_required
def collection_exports(request):
    """
    View the stock export files.
    """
    template = "search/collection_exports.html"
    files = os.listdir(settings.EXPORTS_ROOT)
    files = list(reversed(sorted(files)))
    return render(request, template, {"files": files})

@login_required
def collection_view_file_export(request, filename):
    if filename:
        filepath = os.path.join(settings.EXPORTS_ROOT, filename)
        content = ""
        content_type = ""
        if os.path.exists(filepath):
            with open(filepath, "r") as f:
                content = f.read()
                content_type = filename_content_type(filename)

        response = HttpResponse(content, content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response

@login_required
def collection(request):
    """Search our own collection and take actions.

    - return: a list (of card dicts)
    """
    currency = Preferences.get_default_currency()
    bookshop = users.Bookshop.objects.last()
    publishers = Publisher.objects.all()
    # distributors = settings.dilicom_distributors_as_list()
    # Actually… is it really good to list all of them here?
    # Better fit in the Search with Electre.
    distributors = []
    if not distributors:
        distributors = Distributor.get_all(to_dict=True)

    # List of clients (for modale)
    clients = [it.to_dict() for it in Client.objects.order_by("name").all()]

    return render(request, "search/collection.html", {
        'currency': currency,
        'bookshop': bookshop,  # to check email
        'bookshop_email': bookshop.email if hasattr(bookshop, 'email') else "",
        # 'show_places': Place.objects.count() > 1,
        'publishers': publishers,
        'clients': clients,
        'feature_dilicom_enabled': settings.dilicom_enabled(),
        'distributors': distributors,
        'tags': models.Tag.objects.all(),
    })

@login_required
@staff_member_required
def sell(request):
    show_selling_places = False
    if Place.objects.count() > 1:
        show_selling_places = True
    try:
        bookshop = users.Bookshop.objects.first()
    except Exception as e:
        log.error("Error getting Bookshop: {}".format(e))
        bookshop = None
    return render(request, "search/sell_create.html",
                  {
                      "show_selling_places": show_selling_places,
                      "bookshop": bookshop,
                  })

@login_required
@staff_member_required
def sell_details(request, pk):
    template = "search/sell_details.html"
    sell = None
    soldcards = []
    total_sell = None
    total_price_init = None

    if request.method == 'GET':
        try:
            sell = Sell.objects.get(id=pk)
        except Exception as e:
            log.error(e)
            #XXX return a 404

        if sell:
            soldcards = sell.soldcards_set.all()
            total_sell = sum([it.price_sold for it in soldcards])
            total_price_init = sum([it.price_init for it in soldcards])

    return render(request, template, {
        "sell": sell,
        "soldcards": soldcards,
        "total_sell": total_sell,
        "total_price_init": total_price_init,
    })


@login_required
@staff_member_required
def basket_auto_command(request):
    template = "search/to_command_index.html"

    basket = Basket.auto_command_basket()
    # We get all cards, and group them by distributor.
    copies = basket.copies.all()
    total_copies = len(copies)
    copies_by_dist = toolz.groupby(lambda it: it.distributor_id, copies)
    dists = []
    no_dist = []
    for (dist_id, copies) in list(copies_by_dist.items()):
        if dist_id is not None:
            dists.append((Distributor.objects.get(id=dist_id), len(copies)))
        else:
            no_dist = ((_("NO SUPPLIER"), len(copies)))

    # bof
    dists = sorted(dists, cmp=lambda it, that: it[0].name.lower() < that[0].name.lower())
    if request.method == "GET":
        return render(request, template, {
            'dists': dists,
            'no_dist': no_dist,
            'total_copies': total_copies,
        })

@login_required
@staff_member_required
def basket_auto_command_empty(request):
    if request.method == "POST":
        basket = Basket.auto_command_basket()
        basket.basketcopies_set.all().delete()
        # messages.add_message(request, messages.SUCCESS, _("List of commands emptied successfully."))
        return HttpResponseRedirect(reverse('basket_auto_command'))
    return HttpResponseRedirect(reverse('basket_auto_command'))

@login_required
@staff_member_required
def basket_auto_command_validate(request):
    if request.method == 'POST':
        try:
            res, message = dilicom_command.send_command(Basket.auto_command_copies())
            if res:
                messages.add_message(request, messages.SUCCESS, _("List of commands sent successfully."))
                # TODO: empty list.
            else:
                messages.add_message(request, messages.INFO, message)

        except Exception as e:
            log.error("basket_auto_command_validate: Error while sending the command to Dilicom: {}".format(e))
            messages.add_message(request, messages.ERROR, _("The list of commands was NOT sent to Dilicom."))

        return HttpResponseRedirect(reverse('basket_auto_command'))

    return HttpResponseRedirect(reverse('basket_auto_command'))

@login_required
@staff_member_required
def command_supplier(request, pk):  # pylint: disable
    # pk used on the JS side.
    template = "search/to_command.html"
    return render(request, template)

@login_required
@staff_member_required
def baskets(request):
    template = "search/baskets.html"
    if request.method == "GET":
        return render(request, template)

@login_required
@staff_member_required
def boxes(request):
    template = "search/baskets.html"
    if request.method == "GET":
        return render(request, template, {
        })

@login_required
@staff_member_required
def supplier_returns(request):
    template = "search/baskets.html"
    if request.method == "GET":
        # Create a "box" for every distributor of our DB if needed.
        Basket.ensure_supplier_returns()
        return render(request, template, {
        })

@login_required
def basket_export(request, pk):
    """
    Export the given basket to txt, csv or pdf, with or without
    barcodes.

    Possible GET parameters:
    - report:
      - bill
      - listing
      - simplelisting
      - shippingreceipt: with a supplier_id
    - format: txt, pdf, csv
    - distributor_id: a distributor id (see To Command basket)

    Return: an HttpResponse with the right content type.
    """
    response = HttpResponse()
    params = request.GET.copy()
    params['basket_id'] = pk

    try:
        basket = Basket.objects.get(id=pk)
    except Exception as e:
        log.error("Error trying to export basket{}: {}".format(pk, e))
        return response

    # copies_set = basket.basketcopies_set.all()

    report = request.GET.get('report')
    doc_format = request.GET.get('format')
    distributor_id = request.GET.get('distributor_id')
    # Export of all cards: distributor_id is None.
    # Export of cards with no distributor_id:
    if distributor_id == 'undefined' or distributor_id in [""]:
        distributor_id = -1
    elif distributor_id in ["all", u"all"]:
        pass
    elif distributor_id:
        distributor_id = int(distributor_id)
        # TODO: filter the copies by distributor, or absence thereof.

    shippingreceipt = request.GET.get('shippingreceipt')  # bool
    with_title = request.GET.get('with_title')

    # see also auto_command_copies (that returns a list instead of a QS)
    copies_set = Basket.search(pk, distributor_id=distributor_id)

    barcodes = utils._is_truthy(request.GET.get('barcodes'))
    covers = utils._is_truthy(request.GET.get('covers'))

    distributor = basket.distributor
    list_name = ""
    if shippingreceipt:
        list_name = _("Bon de livraison")
    if distributor:
        list_name = list_name + " {}".format(distributor.name)
    elif basket:
        list_name = _("Liste")
        if basket.name == "auto_command":
            list_name = "command"
        else:
            list_name = list_name + " {}".format(basket.name)
    now = PENDULUM_YMD
    now = pendulum.now()
    list_name += "-" + now.strftime(PENDULUM_YMD)

    if copies_set and report and doc_format:
        response = _export_response(copies_set, report=report, doc_format=doc_format,
                                    # distributor_id=distributor_id,
                                    with_title=with_title,
                                    # shippingreceipt=shippingreceipt,
                                    bon_de_commande=basket.bon_de_commande,
                                    barcodes=barcodes,
                                    covers=covers,
                                    name=list_name,
                                    params=params)
        # exporting response for dist 9 took 0:00:04.128225
        # filtering dist by query
        # basket search distributor_id 9 took 0:00:00.000333
        # exporting response for dist 9 took 0:00:04.598553 = same csv generation time.
        # with rows generated by values_list: see below
        # --
        # exporting response for dist 2 (sodis, 300 cards) took 0:00:00.444018

    return response


def _export_response(copies_set,
                     report="",
                     doc_format="",
                     inv=None,
                     name="",
                     # distributor=None,
                     # distributor_id=None,
                     with_title=None,
                     # shippingreceipt=False,  # bool: is it a shippingreceipt?
                     bon_de_commande="",     # the shippingreceipt value.
                     discount=0,
                     params=[],
                     covers=False,
                     barcodes=False,
                     total=None,
                     total_with_discount=None):
    """
    Build the response with the right data (a bill ? just a list ?).

    - copies_set: list of objects, like basketcopies_set: has attributes card and quantity.
    - inv: inventory object (necessary to get the diff and the sold cards).
    - params: list of GET parameters. Yet another way to get a bill. Used in Institutions, a simple link with URL parameters.

    Return: formatted HttpResponse
    """
    response = HttpResponse()
    quantity_header = _("Quantity")
    rows = None

    currency_fmt = Preferences.get_default_currency_symbol()

    # <2023-09-21 Thu> Create a bill from an institution basket page,
    # with a simple GET URL.
    # Initially this view was to create row exports of a basket, not bills (only from an inventory).
    # Bills were created with an API call. The API call returns a JSON response and needs
    # a bit of client-side handling. Here we can simplify and directly hit a URL, like:
    # /baskets/<pk>/export?bill=true&is_institution=true etc
    # We just had to factorize the bill creation from the API to a common function.
    if report == 'bill' and params and utils._is_truthy(params.get('is_institution')):
        # just add a param for the bill_utils function.
        params['show_discounted_price'] = True
        if not params.get('bon_de_commande_id') and bon_de_commande:
            params['bon_de_commande_id'] = bon_de_commande
        to_ret = bill_utils.create_bill(params)
        response = HttpResponse(to_ret.get('outhtml'), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(to_ret['filename'])
        return response

    # The rest of the view uses previous methods, returns listings.
    #
    # create a bill from an inventory.
    if report == 'bill':
        # The cards of the inventory alongside their quantities.
        # DEPRECATED since this is for an inventory.
        if inv:
            header = (_("Title"), _("Quantity sold"))
            diff = inv.diff()[0]  # that should be cached XXX. A json row in the db ?
            rows = []
            for k in diff.values():
                if k.get('diff', 0) < 0:
                    qtysold = - k.get('diff')
                else:
                    qtysold = 0
                rows.append((k['card'], qtysold))

            # Sort quantities first, then by title.
            rows = sorted(rows)
            rows = sorted(rows, key = lambda it: it[1] > 0, reverse=True)  # with quantities first

        else:
            log.error("Implementation error: if you want a bill, that's certainely from an inventory, but we don't have the 'inv' object.")

    #
    # listing
    #
    # many columns
    #
    elif report == 'listing':
        # inv_cards = inv.inventorycopies_set.all()
        header = ("ISBN", _("Title"), _("Authors"), _("Publishers"), _("Supplier"),
                  _("Shelf"),
                  _("Price"),
                  _("with discount"),
                  _("Quantity"))
        rows = []
        for ic in copies_set:
            if ic.card:
                rows.append([
                    ic.card.isbn if ic.card.isbn else "",
                    ic.card.title,
                    ic.card.authors_repr,
                    ic.card.pubs_repr,
                    ic.card.distributor_repr,
                    ic.card.shelf.name if ic.card.shelf else "",
                    format_price_for_locale(ic.card.price),
                    ic.card.price_discounted,
                    ic.quantity,
                ]
                )
        rows = sorted(rows)

    #
    # simplelisting
    #
    # ISBN, quantity, and optionally title.
    #
    elif report == 'simplelisting':
        header = None
        rows = copies_set
        # List of ISBN / quantity to command (BasketCopy.nb)
        if with_title:
            rows = copies_set.values_list('card__isbn', 'nb', 'card__title')
        else:
            rows = copies_set.values_list('card__isbn', 'nb')
        # Cleanup: remove void ISBNs. A Dilicom command would fail.
        if rows:
            rows = [it for it in rows if it[0]]
            rows = sorted(rows)

    # From here we have rows: list of tuples with the card obj and the quantity.
    if rows is None:
        log.error("No rows when exporting to file. Shouldn't happen !")

    if doc_format in ['csv']:
        pseudo_buffer = Echo()
        writer = unicodecsv.writer(pseudo_buffer, delimiter=b';')
        content = writer.writerow(b"")

        if report in ['bill']:
            rows = [(it[0].title, it[1]) for it in rows]
        if header:
            rows.insert(0, header)
        start = timezone.now()
        content = b"".join([writer.writerow(row) for row in rows])
        end = timezone.now()
        print("writing rows to csv took {}".format(end - start))

        response = StreamingHttpResponse(content, content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(name)

    elif doc_format in ['txt']:
        # 63 = MAX_CELL + 3 because of trailing "..."
        rows = ["{:63} {:20} {}".format(utils.truncate(it.card.title),
                                         it.card.pubs_repr,
                                         it.quantity)
                for it in copies_set
                if it and it.card is not None
                ]
        rows = sorted(rows)
        content = "\n".join(rows)
        response = HttpResponse(content, content_type="text/raw")

    elif doc_format in ['pdf']:
        date = datetime.date.today()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(name)

        template = get_template('pdftemplates/pdf-barcode.html')
        if report == "listing":
            cards_qties = [(it.card, it.quantity)
                           for it in copies_set
                           if it and it.card]
        elif report == "bill":
            # DEPRECATED since this is for an inventory.
            quantity_header = _("Quantity sold")
            for it in rows or []:
                if not utils.is_isbn(it[0].isbn):
                    it[0].isbn = "0000000000000"

            cards_qties = rows

        cards_qties = [it for it in cards_qties if it is not None] if cards_qties else []

        if total is None:
            total = sum([it[1] * it[0].price if it[0].price else 0
                         for it in cards_qties
                         if it and it[0] is not None])
        if discount:
            total = total - total / 100 * discount
        if total_with_discount is None:
            total_with_discount = -1  # unemplemented. Inventories should compute it before.
        total_qty = sum([it[1] for it in cards_qties
                         if it and it[1]
                         ])

        # barcode
        start = time.time()
        if barcodes:
            for card, _noop in cards_qties:
                # Find or create the base64 barcode.
                search = Barcode64.objects.filter(ean=card.ean)
                if not search:
                    eanbase64 = Barcode64.ean2barcode(card.ean)
                    try:
                        if eanbase64:
                            log.info("---- saving a base64 for ean {}".format(card.ean))
                            Barcode64(ean=card.ean, barcodebase64=eanbase64).save()
                    except Exception as e:
                        log.error('Error saving barcode of ean {}: {}'.format(card.ean, e))
                else:
                    eanbase64 = search[0].barcodebase64

                card.eanbase64 = eanbase64

        sourceHtml = template.render({'cards_qties': cards_qties,
                                      'name': name,
                                      'bon_de_commande': bon_de_commande,
                                      'total': total,
                                      'total_with_discount': total_with_discount,
                                      'total_qty': total_qty,
                                      'barcode': barcodes,
                                      'covers': covers,
                                      'quantity_header': quantity_header,
                                      'currency_fmt': currency_fmt,
                                      'date': date})

        genstart = time.time()
        outhtml = HTML(string=sourceHtml).write_pdf()
        genend = time.time()
        log.info("------ html generation is taking {}".format(genend - genstart))

        response = HttpResponse(outhtml, content_type='application/pdf')
        end = time.time()
        log.info("-------- generating barcodes for {} cards took {}".format(len(cards_qties), end - start))
        response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(name)

    return response


@login_required
def generic_import(request):
    """
    Generic import view, and we send the results to a basket or something else.
    """
    template = "search/upload.html"
    alert = {'level': ALERT_SUCCESS,
             'message': ""}
    status = 200
    isbns = None
    dicts = None
    msgs = []
    source = None
    source_pk = None
    source_name = None
    boxes_page = None
    returns_page = None

    if request.method == 'POST':
        params = request.POST.copy()

        boxes_page = utils._is_truthy(params.get('boxes_page'))
        returns_page = utils._is_truthy(params.get('returns_page'))

        inputrows = params.get('inputrows')
        if not inputrows:
            return render(request, template, {'messages': ['could not find rows.']})

        inputrows = inputrows.strip()
        inputrows = inputrows.split('\n')  # \r probably remaining.
        inputrows = [it.strip() for it in inputrows]

        # Find and extract the isbns and an optional quantity.
        isbns_quantities, msgs = extract_all_isbns_quantities(inputrows)
        isbns = [it[0] for it in isbns_quantities]

        # Bulk search.
        if not isbns:
            messages.add_message(request, messages.INFO, "Aucun ISBN ?")
            return render(request, template, {
                'source': source,
                'source_pk': source_pk,
                'source_name': source_name,
                'rows': dicts,
                'status': status,
                'alerts': msgs,
                'currency': Preferences.get_default_currency(),
                'feature_dilicom_enabled': dilicom_enabled(),
            })

        dicts, msgs = bulk_import_from_dilicom(isbns)

        # Did we find everything?
        found_isbns = [it.get('isbn') for it in dicts]
        # it's done on Dilicom's side too, except we get a list of messages,
        # not the list of isbns not found. Could be fixed.
        missing_isbns = set(isbns) - set(found_isbns)

        # Look for missing ones in DB: we might have registered some manually,
        # specially the ones not in the official market.
        # We do this after a Dilicom search, for we prefer to get up-to-date data on import.
        found_missing = Card.objects.filter(isbn__in=list(missing_isbns))
        #values_list('isbn', flat=True)
        found_missing_isbns = [it.isbn for it in found_missing]
        found_missing_isbns_quantities = []  # list of list ISBN, quantity.
        found_missing_quantities = []
        if found_missing_isbns:
            missing_isbns = missing_isbns - set(found_missing_isbns)
            found_isbns += found_missing_isbns
            # get quantities and preserve order here.
            found_missing_isbns_quantities = [it for it in isbns_quantities
                                              if it[0] in found_missing_isbns]
            found_missing_quantities = [_find_quantity_for_isbn(found_missing_isbns_quantities, it.isbn)
                                        for it in found_missing]

        if found_isbns:
            if missing_isbns:
                msgs.insert(0, "We could not find all the ISBNs!")
                status = 502
            else:
                msgs.insert(0, "Searched {} ISBNs. All found.".format(len(isbns)))
        else:
            pass

        # re-get the source_pk hidden fields.
        if params.get('source') and not utils._is_falsy(params.get('source')):
            source = params.get('source')
            source_pk = params.get('source_pk')
            source_name = params.get('source_name')

        # If complete, add the cards to their place and then redirect to it.
        # We don't need to validate in the validate view, everythings's ok,
        # so save a step and a click.
        if not missing_isbns:
            # Get card objects.
            # (loop copied from validate view)
            cards = []
            quantities_list = []
            for i, card_dict in enumerate(dicts):
                card, created = Card.objects.get_or_create(isbn=card_dict.get('isbn'))
                card = Card.update_from_dict(card, card_dict=card_dict,
                                                distributor_gln=card_dict.get('distributor_gln'))
                if card and card.isbn:
                    cards.append(card)
                    quantities_list.append(_find_quantity_for_isbn(isbns_quantities, card.isbn))
                print(card)

            alert = _import_cards_to_destination(source, source_pk, cards, quantities_list)

            alert_2 = _import_cards_to_destination(source, source_pk, found_missing, found_missing_quantities)

            alerts = [alert, alert_2]

            # If import was ok:
            for alert in alerts:
                if alert.get('level') == ALERT_SUCCESS:
                    if source == 'institution':
                        url = reverse('client_institution_baskets')
                    else:
                        if boxes_page:
                            url = reverse('box_view', args=(source_pk,))
                        elif returns_page:
                            url = reverse('supplier_returns', args=(source_pk,))
                        else:
                            url = reverse('basket_view', args=(source_pk,))

                        url += "##{}".format(source_pk)

                    messages.add_message(request, messages.SUCCESS,
                                         _('All imported.'))
                    return HttpResponseRedirect(url)
                # otherwise, show the alert in the validate view:
                else:
                    messages.add_message(request, messages.INFO, alert.get('message'))

            # If not complete, save data in session and ask for validation in another view.
        session_key = "import_{}_{}".format(source, source_pk)
        try:
            request.session[session_key] = {'isbns': isbns,
                                            'dicts': dicts,
                                            'isbns_quantities': isbns_quantities,
                                            }
        except Exception as e:
            log.error("Could not save isbns search results in session, so we won't be able to validate the search, if needed. {}".format(e))

    # GET
    else:
        params = request.GET
        source = params.get('source')
        source_pk = params.get('source_pk') or params.get('id')   # plz use source_pk
        source_name = ""
        boxes_page = utils._is_truthy(params.get('boxes_page'))
        returns_page = utils._is_truthy(params.get('returns_page'))
        if source == 'basket' and source_pk:
            basket_obj = Basket.objects.filter(pk=source_pk).first()
            source_name = basket_obj.name if basket_obj else None

    return render(request, template, {
        'source': source,
        'source_pk': source_pk,
        'source_name': source_name,
        'boxes_page': boxes_page,
        'returns_page': returns_page,
        'rows': dicts,
        'status': status,
        'alerts': msgs,
        'currency': Preferences.get_default_currency(),
        'feature_dilicom_enabled': dilicom_enabled(),
    })


def _find_quantity_for_isbn(isbns_quantities, isbn):
    """
    - isbns_quantities: list of tuples isbn, quantity.
    - isbn: string.

    Return: the corresponding quantity.

    (why not use a dict? Because earlier we manipulate rows of data)
    """
    for isbn_qty in isbns_quantities:
        if isbn_qty and isbn_qty[0] == isbn:
            return isbn_qty[1]


def _import_cards_to_destination(source, source_pk, cards, quantities_list):
    """
    - source: "basket"
    - source_pk: int
    - cards: card objects.
    - quantities_list: list of tuples isbn, quantity.

    Return: alert object.
    """
    alert = {'level': ALERT_SUCCESS,
             'message': ""}
    if source == 'basket':
        if not source_pk:
            log.warn("validate import: we have a source name but no source pk??")
            alert['message'] = "Could not import the cards to {}, missing its ID.".format(source)
            alert['level'] = ALERT_ERROR
        basket_obj = Basket.objects.filter(pk=source_pk).first()

    elif source == "institution":
        if not source_pk:
            log.warn("validate import: we have a source name but no source pk??")
            alert['message'] = "Could not import the cards to {}, missing its ID.".format(source)
            alert['level'] = ALERT_ERROR
        basket_obj = InstitutionBasket.objects.filter(pk=source_pk).first()
        if not basket_obj:
            log.warn("import: InstitutionBasket not found: {}".format(source_pk))

    else:
        log.warn("import cards: unhandled destination: {}".format(source))
        alert['message'] = "Could not finish import. Invalid destination."
        alert['level'] = ALERT_ERROR
        return alert

    if not basket_obj:
        log.warn("validate import: could not find basket id {} for {}. Can't finish import.".format(source_pk, source))
        alert['message'] = "Could not finish import. Invalid destination."
        alert['level'] = ALERT_ERROR
    else:
        if cards:
            alert = basket_obj.add_cards(cards, quantities_list)

    return alert


@login_required
def import_validate(request, *args, **kwargs):
    """
    In the case we didn't find all asked ISBNs, we ask for the user confirmation.

    POST params:

    - source: "basket" or "institution"
    - source_pk: the source pk

    We must find in the session:

    - dicts: list of dicts (cards as dicts, with an ISBN etc)

    The data is passed in session by the key "import_<source>_<source pk>"
    such as "import_basket_1".
    The data is a dict, with a "dicts" key.

    optional:

    - boxes_page: truthy
    - returns_page: truthy
    """
    template = "search/upload.html"
    # alerts = []
    alert = {'level': ALERT_SUCCESS,
             'message': ""}
    if request.method == 'POST':
        params = request.POST.copy()
        source = params.get('source', "")
        source_pk = params.get('source_pk')
        boxes_page = utils._is_truthy(params.get('boxes_page'))
        returns_page = utils._is_truthy(params.get('returns_page'))

        if not source or not source_pk:
            alert['message'] = "No data. Session expired?"
            alert['level'] = ALERT_WARNING
            return render(request, template, {
                          'alerts': [alert],
                          },
                          status=406)

        if source not in ['basket', 'institution']:
            log.warn("import_validate: bad source name: {}. It must be 'basket' or 'institution'.".format(source))
            alert['message'] = "Destination unknown. Nothing to do."
            alert['level'] = ALERT_WARNING
            return render(request, template, {
                          'alerts': [alert],
                          },
                          status=406)

        session_key = "import_{}_{}".format(source, source_pk)
        session_val = request.session.get(session_key, {})
        if not session_val:
            # bad session name.
            log.debug("import_validate: no session data found. Bad name for the session key?")
            alert['message'] = "No data in this session. Nothing to do"
            alert['level'] = ALERT_WARNING
            return render(request, template, {
                          'alerts': [alert],
                          },
                          status=406)

        dicts = session_val.get('dicts')
        isbns_quantities = session_val.get('isbns_quantities')

        # Now clear this session data.
        # try:
        #     del request.session[session_key]  # better keep for refreshes?
        # except Exception as e:
        #     log.warn("Could not delete the session key {}: {}".format(session_key, e))

        cards = []  # list of card objects
        quantities_list = []  # list of ints, respecting cards order.

        if not dicts:
            log.debug("import_validate: no dicts in session.")
            alert['message'] = "No cards data. Session expired?"  # or bad session key.
            alert['level'] = ALERT_WARNING
            return render(request, template, {
                          'alerts': [alert],
                          },
                          status=406)

        if not isbns_quantities:
            alert['message'] = "No quantities data. Session expired?"
            alert['level'] = ALERT_WARNING
            return render(request, template, {
                          'alerts': [alert],
                          },
                          status=406)

        # Add all the books.
        # TODO: with their quantities
        # TODO: in the basket.
        # Create cards.
        for i, card_dict in enumerate(dicts):
            card, created = Card.objects.get_or_create(isbn=card_dict.get('isbn'))
            card = Card.update_from_dict(card, card_dict=card_dict,
                                            distributor_gln=card_dict.get('distributor_gln'))
            if card and card.isbn:
                _quantity_for_isbn = _find_quantity_for_isbn(isbns_quantities, card.isbn)
                if _quantity_for_isbn is not None:
                    cards.append(card)
                    quantities_list.append(_quantity_for_isbn)
                else:
                    log.debug("import_validate: the quantity for card {} is None. Bad data format? Do nothing.".format(card.pk))
            # log.debug("Importing and updated card {}".format(card.pk))

        #
        # Get all cards:
        #
        alert = _import_cards_to_destination(source, source_pk, cards, quantities_list)

        if alert.get('level') == ALERT_SUCCESS:
            if source == 'institution':
                url = reverse('client_institution_baskets')
            else:
                if boxes_page:
                    url = reverse('box_view', args=(source_pk,))
                elif returns_page:
                    url = reverse('supplier_returns', args=(source_pk,))
                else:
                    url = reverse('basket_view', args=(source_pk,))
                url += "##{}".format(source_pk)

            messages.add_message(request, messages.SUCCESS, _('All imported.'))
            return HttpResponseRedirect(url)

        return render(request, template, {
            'alerts': alert,
        })

    else:
        return HttpResponseRedirect(reverse('generic_import'))


@login_required
def history_today(request, *args, **kwargs):
    """
    Redirect to today's sells history page.
    """
    now = pendulum.now()
    url = reverse('history_sells_day',
                  args=(now.strftime('%Y-%m-%d'),))
    return HttpResponseRedirect(url)

@login_required
def history_sells(request, **kwargs):
    now = pendulum.now()
    url = reverse('history_sells_month',
                  args=(now.strftime('%Y-%m'),))
    return HttpResponseRedirect(url)

@login_required
def history_sells_month(request, date, **kwargs):
    template = 'search/history_sells.html'
    try:
        day = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_sells'))  # xxx: loop?

    now = pendulum.datetime.now()
    year = day.year
    month = day.month
    previous_month = day.subtract(months=1).replace(day=1)
    next_month = day.add(months=1).replace(day=1)

    # start = time.time()
    if now.year == day.year and now.month == day.month:
        # No cache for the current month.
        sells_data = Sell.stat_days_of_month(month=month, year=year)
    else:
        # Use cache.
        sells_data = models.stat_days_of_month_with_cache(month=month, year=year)
    # end = time.time()
    # print("--- stat_days_of_month took {}".format(end - start))

    # min year and min month.
    # to build the year select.
    # We also get it from context_processors.
    min_obj = SoldCards.objects.order_by("-created").last()
    min_date = min_obj.created if min_obj else pendulum.now()
    years_range = reversed(range(min_date.year, now.year + 1))

    # Best shelves.
    # start = time.time()
    if now.year == day.year and now.month == day.month:
        # No cache.
        _best_shelves = Stats.best_shelves(year=day.year, month=day.month)
    else:
        # Cache previous months' calculations.
        # Ideally, we'd cache in DB directly.
        _best_shelves = models.best_shelves_with_cache(year=day.year, month=day.month)
    # end = time.time()
    # print("--- best_shelves_with_cache for this month took {}".format(end - start))

    return render(request, template, {'sells_data': sells_data,
                                      'best_shelves': _best_shelves,
                                      'day': day,
                                      'now': now,
                                      # for computer:
                                      'previous_month_obj': previous_month,
                                      # for the URL slug:
                                      'previous_month': previous_month.strftime('%Y-%m'),
                                      'next_month_obj': next_month,
                                      'next_month': next_month.strftime('%Y-%m'),
                                      'year': year,
                                      'years_range': years_range,
                                      })

def _csv_response_from_rows(rows, headers=None, filename=''):
    pseudo_buffer = Echo()
    writer = unicodecsv.writer(pseudo_buffer, delimiter=b';')
    content = writer.writerow(b"")

    if headers:
        rows.insert(0, headers)
    start = timezone.now()
    content = b"".join([writer.writerow(row) for row in rows])
    end = timezone.now()
    print("writing rows to csv took {}".format(end - start))

    response = StreamingHttpResponse(content, content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(filename)
    return response

def _txt_response_from_rows(rows, filename=""):
    """
    For sells export.
    """
    # 63 = MAX_CELL + 3 because of trailing "..."
    # Not ideal, but for compliance with the csv method we get a list of data, not a dict:
    # (sell.card.title,         # 0
    #      sell.card.isbn,      # 1
    #      sell.card.authors_repr,
    #      sell.card.pubs_repr,
    #      sell.card.distributor_repr, # 4
    #      sell.card.shelf.name if sell.card.shelf else "",
    #      sell.card.price,      # 6
    #      sell.card.price_discounted, # 7
    #      sell.quantity)        # 8

    format_str = "{:63} {} {:23} {:20} {:5} {:5} {:3}"
    rows = [format_str.
            format(utils.truncate(it[0]),
                   it[1],
                   utils.truncate(it[3], max_length=20),
                   it[4],
                   it[6],
                   it[7],
                   it[8].decode('utf8'),
            ) for it in rows]
    rows = sorted(rows)
    content = "\n".join(rows)
    response = HttpResponse(content, content_type="text/raw")
    response['Content-Disposition'] = 'attachment; filename={}.txt'.format(filename)
    return response

@login_required
def history_sells_month_export(request, date, **response_kwargs):
    # Creation date date.
    # (see also Bill in api_users)
    INFILE_DATE_FMT = '%m/%Y'  # ticketz: show only month and year, not day.

    try:
        day = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_sells'))  # xxx: loop?

    params = request.GET.copy()
    filename = _("Sells_{}-{}".format(day.year, day.month))

    fileformat = params.get('fileformat')
    # REPORT_CHOICES = [  # noqa:F841
    #     'listing',      # basic: one soldcard per line
    #     'totalperday',  # total revenue of day ; day (CA par jour)
    #     'ticketz',      # a recap
    # ]
    report = params.get('report', 'listing')

    #
    # Report is: one line per day with total revenue.
    #
    if report == 'totalperday':
        revenue_per_day = {}

        # Get a list of soldcards with pre-computed totals that exclude coupons.
        sells_data = Sell.stat_days_of_month(month=day.month, year=day.year)

        for i in xrange(day.days_in_month):
            revenue_per_day[i + 1] = 0
        for soldcard in sells_data['data']:
            try:
                revenue_per_day[soldcard['date_obj'].day] += soldcard['total_price_sold']
            except Exception as e:
                log.warning("error summing the total revenue per day for day {}: {}".format(day, e))

        headers = ("total", _("day"))
        # sort and format the first column to year/month/day.
        raw_rows = sorted(revenue_per_day.items())
        formatted_rows = []
        for row in raw_rows:
            label = "{}/{:02d}/{:02d}".format(day.year, day.month, row[0])
            revenue = utils.roundfloat(row[1], rounding='ROUND_HALF_EVEN')
            formatted_rows.append([label, revenue])
        response = _csv_response_from_rows(formatted_rows, headers=headers, filename=filename)
        return response

    elif report == 'ticketz':
        name = "ticketz"
        if date:
            try:
                creation_date = pendulum.parse(date)
            except Exception as e:
                log.warning("ticketz: could not parse date from string: {}".format(date))

        creation_date_fmt = creation_date.strftime(INFILE_DATE_FMT)

        try:
            bookshop = users.Bookshop.objects.first()
        except Exception as e:
            log.error("Error getting Bookshop: {}".format(e))
            bookshop = {}

        sells_data = Sell.search(month=day.month, year=day.year,
                                 with_total_price_sold=True,
                                 sortorder=1)  # ascending

        # (like in history of day page)
        # Stats by payment mean:
        # list of tuples payment human abbreviation, total.
        total_per_payment_items = _stats_by_payment_mean(sells_data['data'])

        # Stats by VAT of products sold.
        total_per_vat_items = _stats_by_vat(sells_data['data'])

        template = get_template('pdftemplates/ticketz.html')
        sourceHtml = template.render({
            'creation_date_label': "",
            'creation_date_fmt': creation_date_fmt,
            'bookshop': bookshop,
            'total_per_payment_items': total_per_payment_items,
            'total_per_vat_items': total_per_vat_items,
            'sells_data': sells_data,
            'currency': Preferences.get_default_currency(),
        })

        genstart = time.time()
        outhtml = HTML(string=sourceHtml).write_pdf()
        genend = time.time()
        log.info("------ ticketz: html generation is taking {}".format(genend - genstart))

        response = HttpResponse(outhtml, content_type='application/pdf')
        end = time.time()
        log.info("-------- generating ticketz took {}".format(end - genstart))
        response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(name)
        return response

    ###########################################
    # else: basic case, one line per sold item.
    ###########################################

    headers = (_("Date"),
               _("transaction ID"),  # sell id
               _("Title"), "ISBN", _("Authors"), _("Publishers"), _("Supplier"),
               _("Shelf"),
               _("Product type"),
               _("Price bought"),
               _("Price"),
               _("with discount"),
               _("VAT"),
               _("Payment"),
               _("Quantity"),
               _("Total"),    # quantity x sold price… should be total_payment_1 + 2 ?
               _("Revenue"),  # this doesn't count coupons, so can be 0.
               )

    def get_soldcard_vat(soldcard):
        if soldcard.card.vat is not None:
            return soldcard.card.vat
        if soldcard.card.card_type and soldcard.card.card_type.vat is not None:
            return soldcard.card.card_type.vat
        return settings.VAT_TAXES.get('default', 0)

    data = Sell.search(month=day.month, year=day.year,
                       with_total_price_sold=True,
                       sortorder=1)  # ascending

    rows = []
    seen_sells_ids = []
    for soldcard in data['data']:
        if soldcard.card is None:
            # Why does this happen? Deleted card, before we prevent it?
            try:
                total_revenue = soldcard.sell.total_price_sold_for_revenue
            except Exception as e:
                total_revenue = "?"
                log.warning("History month CSV all sells export: could not get total revenue excluding coupons, the soldcard.card was deleted? {}".format(e))
            rows.append(
                (soldcard.created.strftime(PENDULUM_YMD),
                 "?",
                 soldcard.sell.pk,
                 "?",
                 "?",
                 "?",
                 "?",
                 "?"
                 "?",
                 format_price_for_locale(soldcard.price_sold),
                 "",
                 "?",
                 soldcard.sell.payments_repr(),
                 soldcard.quantity,
                 utils.roundfloat(soldcard.quantity * soldcard.price_sold),
                 total_revenue,
                 ))

        else:
            if soldcard.sell.pk not in seen_sells_ids:
                total_revenue = soldcard.sell.total_price_sold_for_revenue
                seen_sells_ids.append(soldcard.sell.pk)
            else:
                total_revenue = 0

            price_bought = 0
            try:
                if soldcard.card and soldcard.card.price_bought:
                    price_bought = format_price_for_locale(soldcard.card.price_bought)
            except Exception as e:
                log.debug("python I tell you this should not happen {}".format(e))

            rows.append((
                soldcard.created.strftime(PENDULUM_YMD),
                soldcard.sell.pk,
                soldcard.card.title,
                soldcard.card.isbn,
                soldcard.card.authors_repr,
                soldcard.card.pubs_repr,
                soldcard.card.distributor_repr,
                soldcard.card.shelf.name if soldcard.card.shelf else "",
                soldcard.card.card_type.pk if soldcard.card.card_type else "",
                price_bought,
                format_price_for_locale(soldcard.price_sold),
                format_price_for_locale(soldcard.card.price_discounted),
                # VAT
                # Necessary lookup until all the DB is updated with the VAT from Dilicom.
                # 2022-06-14
                get_soldcard_vat(soldcard),
                soldcard.sell.payments_repr(),
                soldcard.quantity,
                utils.roundfloat(soldcard.quantity * soldcard.price_sold),
                total_revenue,
            ))

        rows = sorted(rows)

    if fileformat in ['csv']:
        response = _csv_response_from_rows(rows, headers=headers, filename=filename)
        return response

    if fileformat in ['txt']:
        response = _txt_response_from_rows(rows, filename=filename)
        return response


@login_required
def history_entries_month(request, date, **kwargs):
    template = 'search/history_entries.html'
    try:
        day = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_sells'))

    now = pendulum.datetime.now()
    year = day.year
    month = day.month
    previous_month = day.subtract(months=1).replace(day=1)
    next_month = day.add(months=1).replace(day=1)

    data = history.Entry.history(year=year, month=month)
    return render(request, template, {'data': data,
                                      'now': now,
                                      'day': day,
                                      'previous_month': previous_month,
                                      'next_month': next_month,
                                      'year': year})

def _csv_response_from_rows_entries(rows, headers=None, filename=''):
    pseudo_buffer = Echo()
    writer = unicodecsv.writer(pseudo_buffer, delimiter=b';')
    content = writer.writerow(b"")

    if headers:
        rows.insert(0, headers)
    start = timezone.now()
    content = b"".join([writer.writerow(row) for row in rows])
    end = timezone.now()
    print("writing rows to csv took {}".format(end - start))

    response = StreamingHttpResponse(content, content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(filename)
    return response

def _txt_response_from_rows_entries(rows, filename=""):
    """
    For entries export.
    """
    format_str = "{:63} {} {:23} {:20} {:5} {:5} {:3}"
    rows = [format_str.
            format(utils.truncate(it[0]),
                   it[1],
                   utils.truncate(it[3], max_length=20),
                   it[4],
                   it[6],
                   it[7],
                   it[8],
            ) for it in rows]
    rows = sorted(rows)
    content = "\n".join(rows)
    response = HttpResponse(content, content_type="text/raw")
    response['Content-Disposition'] = 'attachment; filename={}.txt'.format(filename)
    return response

@login_required
def history_entries_month_export(request, date, **kwargs):
    """
    Export the entries of this month.
    """
    try:
        date = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_entries'))  # xxx: loop?

    # data = Sell.search(year=day.year, month=day.month,
    # with_total_price_sold=False)
    data = EntryCopies.objects.filter(created__year=date.year,
                                      created__month=date.month)

    fileformat = request.GET.get('fileformat')
    filename = _("Entries_{}-{}".format(date.year, date.month))

    headers = (_("Date"),
               _("Title"), "ISBN", _("Authors"), _("Publishers"), _("Supplier"),
               _("Shelf"),
               _("Price"),
               )
    rows = [
        (entrycopy.entry.created.strftime(PENDULUM_YMD),
         entrycopy.card.title,
         entrycopy.card.isbn,
         entrycopy.card.authors_repr,
         entrycopy.card.pubs_repr,
         entrycopy.card.distributor_repr,
         entrycopy.card.shelf.name if entrycopy.card.shelf else "",
         format_price_for_locale(entrycopy.card.price),
         )
        for entrycopy in data]
    rows = sorted(rows)

    if fileformat in ['csv']:
        response = _csv_response_from_rows_entries(rows, headers=headers, filename=filename)
        return response

    if fileformat in ['txt']:
        response = _txt_response_from_rows_entries(rows, filename=filename)
        return response


def _history_sells_day(day, place_id=None):
    """
    Return the list of sells of this day.

    - day: pendulum object.
    - place_id: filter by this place.
    """
    sells_data = Sell.search(day=day.day, month=day.month, year=day.year,
                             place_id=place_id,
                             with_total_price_sold=True,
                             sortorder=1)  # ascending

    # Best sells per card type.
    best_sells = models.get_best_sells(sells_data['data'])

    # Stats by card_type
    # grouped_sells = toolz.groupby(lambda (it): it.card.card_type, sells_data['data'])
    grouped_sells = toolz.groupby(lambda (it): it[2], sells_data['not_books'])
    # data for the template.
    data_grouped_sells = []
    for card_type, soldcards in grouped_sells.iteritems():
        # solcards: tuple quantity, price_sold, card_type.pk, isbn, type name
        if card_type is None:
            type_name = _("undefined")
        else:
            if soldcards:
                type_name = soldcards[0][4]
            else:
                type_name = ""
        datadict = [type_name, {}]
        datadict[1]['data'] = soldcards
        datadict[1]['total_sells'] = sum([it[0] for it in soldcards])
        datadict[1]['nb_sells'] = len(soldcards)
        totalsold = sum([it[0] * it[1] for it in soldcards])
        datadict[1]['total_sold'] = totalsold
        datadict[1]['sell_mean'] = totalsold / len(soldcards) if soldcards else 0
        data_grouped_sells.append(datadict)

    # Stats by payment mean:
    # list of tuples payment human abbreviation, total.
    total_per_payment_items = _stats_by_payment_mean(sells_data['data'])

    # Visually show the same sells with the same background color.
    bg_colors = []
    white = ""
    # grey = "#d3d3d3"
    grey = "#eee9e9"  # snow2
    grey2 = "#cdc9c9"
    previous_sell_id = -1
    cur_color = grey2

    # In the template, we want to know the first soldcard of a sell
    # transaction.
    # Yes, we should have iterated over sells and then over soldcards.
    sell_transaction_markers = []
    sells_ids = []

    def flip_color(color):
        if color == white:
            return grey
        elif color == grey:
            return grey2
        return white

    # Show payments (abbreviated).
    payments = []
    payments_2 = []
    for it in sells_data['data']:
        sells_ids.append("{}".format(it.sell_id))
        if it.sell_id == previous_sell_id:
            bg_colors.append(cur_color)
            previous_sell_id = it.sell_id
            payments.append("")
            payments_2.append("")
            sell_transaction_markers.append(False)
        else:
            cur_color = flip_color(cur_color)
            bg_colors.append(cur_color)
            previous_sell_id = it.sell_id
            payments.append(it.sell.payments_repr())
            sell_transaction_markers.append(True)

    data = list(zip(sells_data['data'], bg_colors, payments, sell_transaction_markers, sells_ids))
    return {'best_sells': best_sells,
            'sells_data': sells_data,
            'data': data,
            'total_per_payment_items': total_per_payment_items,
            'data_grouped_sells': data_grouped_sells,
            }


@login_required
def history_sells_day(request, date, **kwargs):
    """
    Return the list of sells of this day.

    - date: string (format %Y-%M-%d)
    """
    template = 'search/history_sells_day.html'
    title = _("History")
    currency = Preferences.get_default_currency()

    place_id = request.GET.get("place_id")
    place = None
    if place_id:
        place = Place.objects.filter(pk=place_id).first()

    # XXX: view called twice?? as well as history_sells
    try:
        day = pendulum.datetime.strptime(date, PENDULUM_YMD)
    except Exception as e:
        log.error('History per days: could not parse {}: {}'.format(date, e))

        return HttpResponseRedirect(reverse('history_sells'))

    now = pendulum.datetime.today()
    previous_day = day.subtract(days=1)  # yes, not subStract.
    previous_day_fmt = previous_day.strftime(PENDULUM_YMD)  # URL slug
    next_day = None
    next_day_fmt = None
    if day < now:
        next_day = day.add(days=1)
        next_day_fmt = next_day.strftime(PENDULUM_YMD)

    history_min_date = Preferences.get_history_min_date()

    # Get the history:
    res = _history_sells_day(day, place_id=place_id)

    html = render(request, template, {'sells_data': res['sells_data'],
                                      'data': res['data'],
                                     'title': title,
                                     'place': place,
                                     'places': Place.objects.filter(is_stand=False).all(),
                                      'best_sells': res['best_sells'],
                                      'data_grouped_sells': res['data_grouped_sells'],
                                      'total_per_payment_items': res['total_per_payment_items'],
                                      'previous_day': previous_day,
                                      'previous_day_fmt': previous_day_fmt,
                                      'next_day': next_day,
                                      'next_day_fmt': next_day_fmt,
                                      'month_fmt': '{}-{}'.format(day.year,
                                                                  format(day.month, '0>2')),
                                      'history_min_date': history_min_date,
                                      'default_currency': currency,
                                      'now': now,
                                      'day': day})

    return html


@login_required
def history_restocking_day(request, date, **kwargs):
    template = 'search/history_restocking_day.html'
    try:
        day = pendulum.datetime.strptime(date, PENDULUM_YMD)
    except Exception as e:
        log.error('Entries history per day: could not parse {}: {}'.format(date, e))
        return HttpResponseRedirect(reverse('history_entries_month'))

    now = pendulum.datetime.today()
    previous_day = day.subtract(days=1)  # yes, not subStract.
    previous_day_fmt = previous_day.strftime(PENDULUM_YMD)
    next_day = None
    next_day_fmt = None
    if day < now:
        next_day = day.add(days=1)
        next_day_fmt = next_day.strftime(PENDULUM_YMD)

    soldcards = SoldCards.search(day=day.day, month=day.month, year=day.year,
                                  sortorder=1)  # ascending
    seen = {}  # dict with id: bool
    cards = []
    for sc in soldcards:
        if not seen.get(sc.card.pk):
            cards.append(sc.card)
            seen[sc.card.pk] = True

    title = _("Restocking")
    return render(request, template, {'cards': cards,
                                      'title': title,
                                      'previous_day': previous_day,
                                      'previous_day_fmt': previous_day_fmt,
                                      'next_day': next_day,
                                      'next_day_fmt': next_day_fmt,
                                      'month_fmt': '{}-{}'.format(day.year,
                                                                  format(day.month, '0>2')),
                                      'now': now,
                                      'day': day})


@login_required
def history_entries_day(request, date, **kwargs):
    """
    Return the list of entries of this day.

    - date: string (format %Y-%M-%d)
    """
    template = 'search/history_entries_day.html'
    try:
        day = pendulum.datetime.strptime(date, PENDULUM_YMD)
    except Exception as e:
        log.error('Entries history per day: could not parse {}: {}'.format(date, e))
        return HttpResponseRedirect(reverse('history_entries_month'))

    data = history.Entry.history_day(year=day.year, month=day.month, day=day.day)

    now = pendulum.datetime.today()
    previous_day = day.subtract(days=1)  # yes, not subStract.
    previous_day_fmt = previous_day.strftime(PENDULUM_YMD)
    next_day = None
    next_day_fmt = None
    if day < now:
        next_day = day.add(days=1)
        next_day_fmt = next_day.strftime(PENDULUM_YMD)

    shelves = data['entries'].values_list('card__shelf__name', flat=True)
    # remove duplicates
    shelves = [it for it in shelves if it is not None]
    shelves = list(set(shelves))

    publishers = data['entries'].values_list('card__publishers__name', flat=True)
    publishers = list(set(publishers))
    publishers = [it for it in publishers if it is not None]

    return render(request, template, {'data': data,
                                      'shelves': shelves,
                                      'publishers': publishers,
                                      'previous_day': previous_day,
                                      'previous_day_fmt': previous_day_fmt,
                                      'next_day': next_day,
                                      'next_day_fmt': next_day_fmt,
                                      'month_fmt': '{}-{}'.format(day.year,
                                                                  format(day.month, '0>2')),
                                      'now': now,
                                      'day': day})

@login_required
def history_sells_exports(request, **kwargs):
    """
    Export a list of Sells in csv or txt.  If no date nor distributor
    is given, export the first 50 results.

    - month: int (starting at 1 for python, when it starts at 0 in js)
    - year: int
    - distributor_id: int

    Return: a StreamingHttpResponse with the right content_type.
    """
    params = request.GET.copy()
    outformat = params.get('format')
    params.pop('format')
    month = params.get('month', timezone.now().month)
    year = params.get('year', timezone.now().year)
    distributor_id = params.get('distributor_id')
    filename = _("Sells history")
    if year:
        filename += " - {}".format(year)
    if month:
        filename += "-{}".format(month)

    response = None
    res = Sell.search(to_list=True,
                      distributor_id=distributor_id,
                      month=month,
                      year=year)
    # total = res.get('total')
    res = res['data']

    if outformat in ['csv']:
        pseudo_buffer = Echo()
        writer = unicodecsv.writer(pseudo_buffer, delimiter=b';')
        content = writer.writerow(b"")

        rows = [(it['created'],
                 it['price_sold'],
                 it['card']['title'],
                 it['card']['distributor']['name'] if it['card']['distributor'] else "",
                )
                for it in res]
        header = (_("date sold"),
                  _("price sold"),
                  _("title"),
                  _("supplier"),
        )
        rows.insert(0, header)
        content = b"".join([writer.writerow(row) for row in rows])

        response = StreamingHttpResponse(content, content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(filename)

    elif outformat in ['txt']:
        rows = ["{}-+-{}-+-{}-+-{}".format(
            _("date sold"),
            _("price sold"),
            _("title"),
            _("supplier"),
        )]
        # format: {:min width.truncate}
        # https://pyformat.info/
        rows += sorted(["{:10.10} {} {:5} {:30}".format(
            it['created'],
            it.get('price_sold', 0),
            utils.truncate(it['card']['title']),  # truncate long titles
            it['card']['distributor']['name'] if it['card']['distributor'] else "",
        )
                        for it in res])
        content = "\n".join(rows)
        response = HttpResponse(content, content_type="text/raw")
        response['Content-Disposition'] = 'attachment; filename="{}.txt"'.format(filename)

    return response

@login_required
def suppliers_sells(request, **kwargs):
    now = pendulum.now()
    url = reverse('suppliers_sells_month',
                  args=(now.strftime('%Y-%m'),))
    return HttpResponseRedirect(url)


@login_required
def suppliers_sells_month(request, date, **kwargs):
    """
    Total sells of the month for distributors and
    publishers (for books that were not counted for distributors first).
    """
    template = 'search/suppliers_sells_month.html'
    try:
        day = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_sells'))  # xxx: loop?

    now = pendulum.now()
    year = day.year
    month = day.month
    previous_month = day.subtract(months=1).replace(day=1)
    next_month = day.add(months=1).replace(day=1)

    # Cache the query for +/- a day.
    # res = models.history_suppliers(year=year, month=month)
    res = models.history_suppliers_with_cache(year=year, month=month)

    return render(request, template, {'distributors_data': res['distributors_data'],
                                      'chart_distributors_data': res['chart_distributors_data'],
                                      # must be the table name, so the link to the admin works:
                                      'object_db_name': 'distributor',
                                      'object_name': _('distributor'),
                                      'day': day,
                                      'now': now,
                                      'previous_month_obj': previous_month,
                                      'previous_month': previous_month.strftime('%Y-%m'),
                                      'previous_month_url': reverse('suppliers_sells_month',
                                                                    args=(previous_month.strftime('%Y-%m'),)),
                                      'next_month_obj': next_month,
                                      'next_month': next_month.strftime('%Y-%m'),
                                      'next_month_url': reverse('suppliers_sells_month',
                                                                args=(next_month.strftime('%Y-%m'),)),
                                      'year': year,
                                      'default_currency': Preferences.get_default_currency(),
                                      'title': _("Distributors"),
    })

@login_required
def publishers_sells(request, **kwargs):
    now = pendulum.now()
    url = reverse('publishers_sells_month', args=(now.strftime('%Y-%m'),))
    return HttpResponseRedirect(url)

@login_required
def publishers_sells_month(request, date, **kwargs):
    """
    Total sells of the month for each publisher.
    Similar to the suppliers page.
    """
    template = 'search/suppliers_sells_month.html'
    try:
        day = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_sells'))  # xxx: loop?

    now = pendulum.now()
    year = day.year
    month = day.month
    previous_month = day.subtract(months=1).replace(day=1)
    next_month = day.add(months=1).replace(day=1)

    # data, can take seconds, so we cache it for 1 day:
    # res = models.history_publishers(year=year, month=month)
    res = models.history_publishers_with_cache(year=year, month=month)
    # or no data, for quick devel:
    # res = {'distributors_data': [], 'chart_distributors_data': []}

    return render(request, template, {'distributors_data': res['distributors_data'],
                                      'chart_distributors_data': res['chart_distributors_data'],
                                      # must be the table name, so the link to the admin works:
                                      'object_db_name': 'publisher',
                                      'object_name': _('publisher'),
                                      'day': day,
                                      'now': now,
                                      'previous_month_obj': previous_month,
                                      'previous_month': previous_month.strftime('%Y-%m'),
                                      'previous_month_url': reverse('publishers_sells_month',
                                                                    args=(previous_month.strftime('%Y-%m'),)),
                                      'next_month_obj': next_month,
                                      'next_month': next_month.strftime('%Y-%m'),
                                      'next_month_url': reverse('publishers_sells_month',
                                                                args=(next_month.strftime('%Y-%m'),)),
                                      'year': year,
                                      'default_currency': Preferences.get_default_currency(),
                                      'title': _("Publishers"),
    })

@login_required
def publishers_sells_month_list(request, pk, date, **kwargs):
    template = 'search/supplier_sells_month_list.html'
    default_currency = Preferences.get_default_currency()
    try:
        day = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_sells'))  # xxx: loop?

    try:
        publisher_obj = Publisher.objects.get(id=pk)
    except ObjectDoesNotExist:
        # XXX: add message.
        return HttpResponseRedirect(reverse('publishers_sells_month', args=(date,)))

    now = pendulum.now()
    year = day.year
    month = day.month
    previous_month = day.subtract(months=1).replace(day=1)
    next_month = day.add(months=1).replace(day=1)

    sells = Sell.sells_of_month(year=year, month=month, publisher_id=pk)

    cards_sold = sells.values_list('quantity', flat=True)
    nb_cards_sold = sum(cards_sold)
    prices_sold = sells.values_list('price_sold', flat=True)
    public_prices = sells.values_list('price_init', flat=True)
    assert len(prices_sold) == len(cards_sold)
    total = sum([cards_sold[i] * prices_sold[i] for i in range(len(prices_sold))])
    total_public_price = sum([public_prices[i] * cards_sold[i] for i in range(len(cards_sold))])

    url_name = 'publishers_sells_month_list'
    previous_month_url = reverse(url_name, args=(pk, previous_month.strftime('%Y-%m')))
    next_month_url = reverse(url_name, args=(pk, next_month.strftime('%Y-%m')))

    return render(request, template, {'sells': sells,
                                      'cards_sold': cards_sold,
                                      'nb_cards_sold': nb_cards_sold,
                                      'total': total,
                                      'total_fmt': utils.price_fmt(total, default_currency),
                                      'total_public_price': total_public_price,
                                      'total_public_price_fmt': utils.price_fmt(total_public_price, default_currency),
                                      'obj': publisher_obj,
                                      'day': day,
                                      'now': now,
                                      'next_month_url': next_month_url,
                                      'previous_month_url': previous_month_url,
    })

@login_required
def distributors_sells_month_list(request, pk, date, **kwargs):
    template = 'search/supplier_sells_month_list.html'
    default_currency = Preferences.get_default_currency()
    try:
        day = pendulum.datetime.strptime(date, '%Y-%m')
    except Exception:
        return HttpResponseRedirect(reverse('history_sells'))  # xxx: loop?

    try:
        obj = Distributor.objects.get(id=pk)
    except ObjectDoesNotExist:
        # XXX: add message.
        return HttpResponseRedirect(reverse('suppliers_sells_month', args=(date,)))

    now = pendulum.now()
    year = day.year
    month = day.month
    previous_month = day.subtract(months=1).replace(day=1)
    next_month = day.add(months=1).replace(day=1)

    sells = Sell.sells_of_month(year=year, month=month, distributor_id=pk)
    # copy pasted :S
    cards_sold = sells.values_list('quantity', flat=True)
    nb_cards_sold = sum(cards_sold)
    prices_sold = sells.values_list('price_sold', flat=True)
    public_prices = sells.values_list('price_init', flat=True)
    assert len(prices_sold) == len(cards_sold)
    total = sum([cards_sold[i] * prices_sold[i] for i in range(len(prices_sold))])
    total_public_price = sum([public_prices[i] * cards_sold[i] for i in range(len(cards_sold))])

    url_name = 'distributors_sells_month_list'
    previous_month_url = reverse(url_name, args=(pk, previous_month.strftime('%Y-%m')))
    next_month_url = reverse(url_name, args=(pk, next_month.strftime('%Y-%m')))

    return render(request, template, {'sells': sells,
                                      'cards_sold': cards_sold,
                                      'nb_cards_sold': nb_cards_sold,
                                      'total': total,
                                      'total_fmt': utils.price_fmt(total, default_currency),
                                      'total_public_price': total_public_price,
                                      'total_public_price_fmt': utils.price_fmt(total_public_price, default_currency),
                                      'obj': obj,
                                      'day': day,
                                      'now': now,
                                      'next_month_url': next_month_url,
                                      'previous_month_url': previous_month_url,
    })

# DEPRECATED. Deletion date: beginning of 2024.
@login_required
@staff_member_required
def inventory_export(request, pk):
    """
    Export the list of books in CSV.

    PDF export was disabled (button removed from the UI): too long for
    the clients' browsers, and fails. Also not very useful.
    """
    total = total_with_discount = 0
    try:
        inv = Inventory.objects.get(id=pk)
    except Exception as e:
        log.error("Error trying to export inventory of pk {}: {}".format(pk, e))

    copies_set = inv.inventorycopies_set.all()

    report = request.GET.get('report')
    format = request.GET.get('format')
    barcodes = utils._is_truthy(request.GET.get('barcodes'))
    covers = utils._is_truthy(request.GET.get('covers'))
    total = inv.value()
    total_with_discount = inv.value(discount=True)

    response = _export_response(copies_set, report=report, doc_format=format,
                                inv=inv,
                                barcodes=barcodes,
                                covers=covers,
                                name=inv.name,
                                total=total,
                                total_with_discount=total_with_discount,)

    return response

@login_required
@staff_member_required
def inventories(request):
    """
    GET all inventories.

    POST actions:
    - archive_all
    - resetallquantitiestozero
    """
    template = "search/inventories.html"
    if request.method == 'GET':
        return render(request, template)

    elif request.method == 'POST':
        action = request.GET.get('action')
        if not action:
            log.info("POSTing to inventories: no action given, nothing to do.")
            return HttpResponseRedirect(reverse("inventories"))
        if action == "archive_all":
            # This action is deprecated.
            # DEPRECATED. Deletion date: beginning of 2024.

            # Check if some inventories are open but not applied?
            # We currently warn in the confirmation alert.
            # not_applied = Inventory.objects.filter(archived=False, applied=False)

            try:
                Inventory.objects.all().update(archived=True, closed=datetime.datetime.now())
                messages.add_message(
                    request, messages.SUCCESS, _("The inventories were successfully archived."))
            except Exception as e:
                log.error("Trying to archive all inventories: {}".format(e))
                messages.add_message(
                    request, messages.ERROR, _("The inventories were not archived."))

            return HttpResponseRedirect(reverse("inventories"))

        elif action == "resetallquantitiestozero":
            status, msgs = Card.reset_all_quantities_to_zero(confirmed=True)
            if status:
                messages.add_message(
                    request,
                    messages.SUCCESS, _("OK"))
            else:
                messages.add_message(
                    request,
                    messages.SUCCESS, _("mmh"))
            return HttpResponseRedirect(reverse("inventories_v2"))

        else:
            # Unknown action.
            log.info("POSTing to inventories: unknown action {}".format(action))
            return HttpResponseRedirect(reverse("inventories"))


# DEPRECATED. Deletion date: beginning of 2024.
@login_required
@staff_member_required
def inventories_archived(request):
    """
    View archived inventories.

    GET params:
    - year
    """
    template = "search/inventories_archived.html"
    params = request.GET.copy()
    years_set = set()
    allcreated_dates = Inventory.objects.filter(archived=True).values_list('created', flat=True)
    for date in allcreated_dates:
        years_set.add(date.year)
    years = list(years_set)
    MAX_YEARS = 5
    years = list(reversed(sorted(years)))[:MAX_YEARS]

    inventories = []
    if years:
        if params and params.get('year'):
            archive_year = params.get('year')
        else:
            archive_year = years[0]
        inventories = Inventory.objects.filter(archived=True).filter(created__year=archive_year).order_by("-created")
    if request.method == 'GET':
        return render(request, template, {
            'inventories': inventories,
            'years': years,
        })

# DEPRECATED. Deletion date: beginning of 2024.
@login_required
@staff_member_required
def inventory(request, pk):
    template = "search/inventory_view.html"
    if request.method == "GET":
        if pk:
            try:
                inv = Inventory.objects.get(id=pk)  # noqa: F841
            except Exception as e:
                log.error(e)
            # state = inv.state()
            return render(request, template)

# DEPRECATED. Deletion date: beginning of 2024.
@login_required
@staff_member_required
def inventory_archive(request, pk):
    # if request.method == "POST":
    if request.method == "GET":
        if pk:
            try:
                inv = Inventory.objects.get(id=pk)
                inv.archive()
                messages.add_message(
                    request, messages.SUCCESS, _("The inventory {} was closed and archived.").format(inv.id))
            except Exception as e:
                log.error(e)
                messages.add_message(
                    request, messages.ERROR,
                    _("There was an error when trying to archive the inventory {}: {}.").format(pk, e))

    return HttpResponseRedirect(reverse("inventories"))

# DEPRECATED. Deletion date: beginning of 2024.
@login_required
@staff_member_required
def inventory_delete(request, pk):
    #XXX should be a post
    if request.method == "GET":
        if pk:
            try:
                inv = Inventory.objects.get(id=pk)
                id = inv.id
                inv.delete()
                messages.add_message(
                    request, messages.SUCCESS, _("The inventory {} was deleted.").format(id))
            except Exception as e:
                log.error(e)
                messages.add_message(
                    request, messages.ERROR,
                    _("There was an error when trying to delete the inventory {}: {}.").format(pk, e))

    return HttpResponseRedirect(reverse("inventories"))

# DEPRECATED. Deletion date: beginning of 2024.
@login_required
@staff_member_required
def inventory_terminate(request, pk):
    """
    """
    template = "search/inventory_terminate.html"
    return render(request, template)

def get_best_shelves(year=None):
    """
    Return data about best shelves.
    By default, for the current year.

    Return: a dict, with best_shelves (a list), shelves_data (a list with name and cards_qty).
    """
    start = time.time()
    best_shelves = []
    try:
        year = year or pendulum.now().year
        best_shelves = models.best_shelves_with_cache(year=year)
        if not best_shelves:
            # In case the cache is messing up.
            # Maybe not useful outside of dev.
            best_shelves = Stats.best_shelves(year=year)
    except Exception as e:
        log.error("Error while asking for the best shelves: {}".format(e))
    end = time.time()
    print("---- best_shelves took: {}".format(end - start))

    # Size of shelves.
    shelves_data = []
    for shelf in Shelf.objects.all():
        shelves_data.append(
            [
                shelf.name,
                shelf.cards_qty,
            ]
        )

    shelves_data = sorted(shelves_data, key=lambda it: it[1], reverse=True)
    data = {
        "best_shelves": best_shelves,
        "shelves_data": shelves_data,
    }
    return data

def get_stock_years_range():
    """
    A list of years: from the first card created in stock, up to today.

    Return: a list of ints.
    """
    now = pendulum.now()
    qs = Card.objects.order_by("created")
    ymin = now.year
    ymax = now.year
    if qs.count():
        ymin = qs.first().created.year

    years_range = range(ymin, ymax + 1)
    return years_range

@login_required
def best_shelves(request):
    """
    Return the best_shelves template for shelves of the given year.

    Params:
    - year

    Return: template. Used with HTMX.
    """
    template = "search/includes/best_shelves.html"
    now = pendulum.now()
    year = request.GET.get('year')
    if year:
        try:
            year = int(year)
        except Exception:
            year = now.year
    else:
        year = now.year

    default_currency = Preferences.get_default_currency()

    years_range = get_stock_years_range()

    shelves_data = get_best_shelves(year=year)
    chart_shelves_data = ""
    if shelves_data['shelves_data']:
        chart_shelves_data = json.dumps(shelves_data['shelves_data'])

    return render(request, template, {
        "best_shelves": shelves_data['best_shelves'],
        "shelves_data": shelves_data['shelves_data'][:10],
        "chart_shelves_data": chart_shelves_data,
        "now": now,
        "year": year,
        "years_range": reversed(years_range),
        "default_currency": default_currency,
    })

@login_required
def dashboard(request):
    template = "search/dashboard.html"
    now = pendulum.now()
    default_currency = Preferences.get_default_currency()

    year = request.GET.get('year', now.year)
    years_range = get_stock_years_range()
    if years_range:
        years_range = list(reversed(years_range))
        years_range = years_range[:5]

    # Some numbers about the stock.
    stock = Stats.stock()

    # Best shelves.
    shelves_data = get_best_shelves(year=year)
    chart_shelves_data = ""
    if shelves_data['shelves_data']:
        chart_shelves_data = json.dumps(shelves_data['shelves_data'])

    return render(request, template, {
        "stats_stock": stock,
        "best_shelves": shelves_data['best_shelves'],
        "shelves_data": shelves_data['shelves_data'][:10],
        "chart_shelves_data": chart_shelves_data,
        "now": now,
        "year": year,
        "years_range": years_range,
        "default_currency": default_currency,
        "bookshop": users.Bookshop.get_bookshop(),
        "logo_url": Preferences.get_logo_url(),
    })


@login_required
def commands_view(request, pk):
    template = "search/commands_view.html"
    currency_fmt = Preferences.get_default_currency_symbol()
    return render(request, template, {
        'object': Command.objects.filter(pk=pk).first(),
        'currency_fmt': currency_fmt,
    })


@login_required
@staff_member_required
def command_receive(request, pk):
    """
    GET: get the inventory state for this command.
    POST: create a new one.
    """
    template = "search/command_receive.html"
    cmd = None

    try:
        cmd = Command.objects.get(id=pk)
    except ObjectDoesNotExist as e:
        log.warning(e)
        messages.add_message(request, messages.ERROR,
                             "Internal erorr: the command you requested does not exist.")
        return HttpResponseRedirect(reverse("commands_view", args=(pk,)))

    if not cmd.inventory:
        inv = InventoryCommand()
        inv.save()
        cmd.inv = inv
        cmd.save()

    return render(request, template)


@login_required
@staff_member_required
def command_receive_terminate(request, pk):
    """
    """
    template = "search/inventory_terminate.html"
    return render(request, template)


@login_required
@staff_member_required
def command_receive_export(request, pk):
    cmd = _get_command_or_return(pk)
    inv = cmd.get_inventory()
    copies_set = inv.copies_set.all()
    inv_name = inv.name or inv.command.title

    report = request.GET.get('report')
    formatt = request.GET.get('format')
    barcodes = utils._is_truthy(request.GET.get('barcodes'))
    covers = utils._is_truthy(request.GET.get('covers'))

    response = _export_response(copies_set, report=report, doc_format=formatt,
                                inv=inv,
                                barcodes=barcodes,
                                covers=covers,
                                name=inv_name)

    return response

@login_required
@staff_member_required
def command_card(request, pk):
    """
    Command the card of the given id, choose a client (optional).
    """
    template = "search/card_command.html"

    if request.method == 'GET':
        card = None
        try:
            card = Card.objects.get(id=int(pk))
        except ObjectDoesNotExist:
            pass
        if card:
            return render(request, template)

@login_required
def catalogue_selection(request):
    """
    Show the selected cards for the online catalogue (ABStock in our case, for those who installed it side by side with Abelujo).
    """
    template = "search/catalogue_selection.html"
    if request.method == 'GET':
        cards = Card.objects.filter(is_catalogue_selection=True)
        return render(request, template, {
            'cards': cards,
        })

@login_required
def catalogue_excluded(request):
    """
    """
    template = "search/excluded_from_catalogue.html"
    if request.method == 'GET':
        cards = Card.objects.filter(is_excluded_for_website=True)
        return render(request, template, {
            'cards': cards,
        })

@login_required
def reservations(request):
    template = "search/reservations.html"
    if request.method == 'GET':
        order_by = request.GET.get('order_by')
        res = users.Reservation.get_reservations(order_by=order_by)
        nb_reservations = len(res)
        res = users.Reservation.group_by_client(res)
        if res:
            res = users.Reservation.generate_email_bodies(res)
        if res:
            res = users.Reservation.generate_sms_bodies(res,
                                                        bookshop_name=users.Bookshop.name())

        return render(request, template, {
            'reservations': res,
            'nb_reservations': nb_reservations,
            'title': _("Reservations"),
            'FEATURE_SMS': settings.FEATURE_SMS,
        })


def _get_isbn_quantities(raw):
    isbn_quantities = []
    if raw:
        isbn_qties = raw.split(';')
        for isbn_qty in isbn_qties:
            if isbn_qty:  # possible leading blank string.
                isbn, qty = isbn_qty.split(',')
                int_qty = int(qty)
                isbn_quantities.append((isbn, int_qty))
    return isbn_quantities

@login_required
def institutions_client_summary(request, pk, *args, **kw):
    template = "search/client_institution_summary.html"
    nb_titles = 0
    nb_copies = 0
    client = ""
    baskets = []
    copies = []
    msgs = []
    total = 0
    total_discounted = 0

    DEFAULT_INSTITUTION_DISCOUNT = 9  # XXX: weak, french only.

    try:
        client = Client.objects.filter(pk=pk).first()
        baskets = client.institutionbasket_set.filter(archived=False)
        for basket in baskets:
            copies += basket.basketcopies_set.all()
            nb_titles += basket.basketcopies_set.count()
            nb_copies += basket.quantity()

        # Totals
        for copy in copies:
            if copy.card and copy.card.price and copy.nb:
                total += copy.card.price * copy.nb

        if total:
            total_discounted = utils.price_minus_percent(total, DEFAULT_INSTITUTION_DISCOUNT)

    except Exception as e:
        log.error("Error getting institution client {} summary: {}".format(pk, e))
        msgs = ["Internal error"]

    return render(request, template, {
        'messages': msgs,
        'client': client,
        'baskets': baskets,
        'nb_titles': nb_titles,
        'nb_copies': nb_copies,
        'total': total,
        'total_discounted': total_discounted,
        'DEFAULT_INSTITUTION_DISCOUNT': DEFAULT_INSTITUTION_DISCOUNT,
    })

@login_required
def institutions_client_export(request, pk, *args, **kw):
    """
    Export bill and estimates for all the ongoing baskets of this institution client.

    - pk: client id. We get all her ongoing institution baskets.

    Return: HTTP response with attached PDF file, that opens in the browser.

    No JS snippet required.
    """
    DEFAULT_INSTITUTION_DISCOUNT = 9
    # to do: currency
    # default_currency = Preferences.get_default_currency()
    # report = request.GET.get('report')
    # doc_format = request.GET.get('format')

    client = Client.objects.filter(pk=pk).first()
    baskets = client.institutionbasket_set.filter(archived=False)
    copies_set = []

    # We take all cards in all this client's baskes,
    # cards received or not.
    for basket in baskets:
        copies_set += basket.basketcopies_set.all()

    cards_data = []  # list of triples: card object, quantity, price sold
    for copy in copies_set:
        cards_data.append((copy.card,
                           copy.nb,
                           copy.card.price,
                           utils.roundfloat(
                               utils.price_minus_percent(copy.card.price, DEFAULT_INSTITUTION_DISCOUNT),
                               rounding="ROUND_HALF_EVEN")
                           ))

    params = {}
    params['cards_data'] = cards_data
    params['client_id'] = pk
    params['show_discounted_price'] = True
    to_ret = bill_utils.create_bill(params)
    response = HttpResponse(to_ret.get('outhtml'), content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(to_ret['filename'])
    return response

@login_required
def client_institution_baskets(request, *args, **kw):
    """
    Return all baskets of all institutional clients.
    """
    template = "search/client_institution_basket.html"
    if request.method == 'GET':
        # client = Client.objects.filter(pk=pk).first()
        # if not client:
            # return
        baskets = InstitutionBasket.objects.exclude(archived=True).all()
        # TODO: group by institutions.
        grouped_baskets = toolz.groupby(lambda it: it.client, baskets)
        isbn_quantities = []  # list of tuples ISBN, quantity.
        raw = None
        if baskets:
            raw = baskets[0].raw_data
        if raw:
            isbn_quantities = _get_isbn_quantities(raw)

        return render(request, template, {
            'client': baskets[0].client if baskets else "",
            'basket': baskets[0] if baskets else None,
            'baskets': baskets,
            'grouped_baskets': grouped_baskets.items(),
            'isbn_quantities': isbn_quantities,
            'books_nb': sum([it[1] for it in isbn_quantities]),
        })

@login_required
def client_institution_basket_view(request, pk, *args, **kw):
    template = "search/client_institution_basket.html"
    discount = 9
    # status = 200
    default_currency = Preferences.get_default_currency()

    if is_htmx_request(request.META):
        # dev reminder: here we split the template in two: one complete, inheriting base.html,
        # a second tailored for HTMX, to "swap" in the DOM.
        # But we could use htmx's hx-select to have one template and choose the part
        # to swap.
        # https://htmx.org/attributes/hx-select/
        # It's good to split the template when generating the base one is compute-intensive.
        template = "search/client_institution_basket_content.html"

    basket = InstitutionBasket.objects.filter(id=pk).first()
    if not basket:
        return  # TODO: not found

    isbns_quantities = []
    raw = basket.raw_data
    if raw:
        isbns_quantities = _get_isbn_quantities(raw)

    # Always import the institution raw data?
    # If we already have some books: add content.
    #
    # Scenario: the client validates a list. They want to add more:
    # the owner un-validates their list, so they can carry on
    # working. They validate again, books are accumulated.
    #
    # If they want to start over from zero: the owner and the client
    # are talking, the owner deletes/cancels the list on his side.
    #
    # if not basket.copies.count():

        # bulk import.
        isbns = [it[0] for it in isbns_quantities]
        log.info("Institution list {}: bulk import".format(pk))
        dicts, msgs = bulk_import_from_dilicom(isbns)

        # Did we find everything?
        # XXX: copied from generic import.
        found_isbns = [it.get('isbn') for it in dicts]
        # it's done on Dilicom's side too, except we get a list of messages,
        # not the list of isbns not found. Could be fixed.
        missing_isbns = set(isbns) - set(found_isbns)
        if found_isbns:
            if missing_isbns:
                msgs.insert(0, "We could not find all the ISBNs!")
                # status = 502
            else:
                msgs.insert(0, "Searched {} ISBNs. All found.".format(len(isbns)))
        else:
            pass

        # re-get the source_pk hidden fields.
        # update: easy, we have the institution basket pk already,
        # since we are not using the generic import view UI.
        # if params.get('source') and not utils._is_falsy(params.get('source')):
        #     source = params.get('source')
        #     source_pk = params.get('source_pk')
        #     source_name = params.get('source_name')

        # If complete, add the cards to their place and then redirect to it.
        # We don't need to validate in the validate view, everythings's ok,
        # so save a step and a click.
        if not missing_isbns:
            # Get card objects.
            # (loop copied from validate view)
            cards = []
            quantities_list = []
            for i, card_dict in enumerate(dicts):
                card, created = Card.objects.get_or_create(isbn=card_dict.get('isbn'))
                card = Card.update_from_dict(card, card_dict=card_dict,
                                             distributor_gln=card_dict.get('distributor_gln'))
                if card and card.isbn:
                    cards.append(card)
                    quantities_list.append(_find_quantity_for_isbn(isbns_quantities, card.isbn))
                log.info("Imported card: {}".format(card))

            # Add these cards to the institution basket:
            alert = _import_cards_to_destination("institution", pk, cards, quantities_list)

            # If import was ok:
            if alert.get('level') == ALERT_SUCCESS:
                basket.raw_data = None
                basket.save()
                # url = reverse('basket_view', args=(source_pk,))
                # url += "##{}".format(source_pk)
                messages.add_message(request, messages.SUCCESS,
                                     _('All imported.'))
                # return HttpResponseRedirect(url)
            # otherwise, show the alert in the validate view:
            else:
                messages.add_message(request, messages.INFO, alert.get('message'))

    else:
        # no raw data anymore: we successfully imported all ISBNs to card objects.
        pass

    # Is this Command ready, did we receive all cards?
    # items = basket.basketcopies_set.values_list('nb', 'nb_received')
    is_command_complete = basket.is_all_received()

    # Total, and with 9% discount (default for institutions).
    items = basket.basketcopies_set.values_list('card__price', 'nb')
    total = 0
    for item in items:
        if item[0] and item[1] > 0:
            total += (item[0] * 100) * item[1]

    total = total
    total_discounted = total - (total * discount / 100)
    total_discounted = total_discounted / 100.0
    total = total / 100.0

    return render(request, template, {
        'client': basket.client if basket else "",
        'basket': basket,
        'copies': basket.basketcopies_set.all(),
        'content': isbns_quantities if not basket.copies.count() else [],
        'books_nb': sum([it[1] for it in isbns_quantities]),
        'is_command_complete': is_command_complete,
        'total': total,
        'total_fmt': utils.price_fmt(total, default_currency),
        'total_discounted': total_discounted,
        'total_discounted_fmt': utils.price_fmt(total_discounted, default_currency),
        'discount': discount,
        'discount_label': "{}: {}%".format(_("discount"), discount),
    })


##########################
##
## Endpoints for tests.
##
##########################
@login_required
def test_owner_confirmation(request):
    template = 'mailer/new_command_template.html'
    bookshop_name = "hello name"
    # XXX: copy-paste from noci test!
    real_test_payload = {
        'buyer': {
            'billing_address': {
                'last_name': 'Vincent',
                'first_name': 'vindarel',
                'email': 'vindarel@mailz.org',
                'address': 'here my city',
                'address_comp': 'comp',
                'city': 'Touwin',
                'postcode': '34100',
                'country': 'France',
                'phone': '07 33 88 88 77',
            },
            'delivery_address': {
                'last_name': 'Vincent',
                'first_name': 'vindarel',
                'email': 'vindarel@mailz.org',
                'address': 'here France',
                'address_comp': 'comp',
                'city': 'Touwin',
                'postcode': '34100',
                'country': 'France',
                'phone': '07 98 88 88 88'
            },
        },
        'order': {
            'online_payment': True,
            'shipping_method': 'relay',  # colissimo
            'mondial_relay_AP': '{"Adresse1":"28 RUE EMILE CARTAILHAC","Adresse2":null,"Available":true,"CP":"31000","HoursHtmlTable":"<table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Monday<\\/th><td>15h00-19h00<\\/td><td>-<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr class=\'d\'><th>Tuesday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Wednesday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr class=\'d\'><th>Thursday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Friday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr class=\'d\'><th>Saturday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Sunday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table>","ID":"071058","Lat":"43,6076448","Long":"1,440779","Nature":"D","Nom":"CBD SHOP FRANCE","Pays":"FR","Photo":null,"Ville":"TOULOUSE","Warning":"","Letter":"A"}',

            'amount': 5050,
            # 'abelujo_items': [{'id': 100, 'qty': 1}, {'id': 101, 'qty': 1}],
            # 'abelujo_items': [{'id': 1, 'qty': 1}, {'id': 2, 'qty': 1}],
            'abelujo_items': [{'id': 1, 'qty': 1}],
            'stripe_payload': {
                'payment_method_types': ["card"],
                'line_items': [
                    {
                        'price_data': {
                            'currency': 'eur',
                            'product_data': {
                                'name': 'ârt Du Chantier, Construire Et dééémolir Du Xvi Au Xxie Siecle',
                                'description': 'ISBN: 9789461614728',
                            },
                            'unit_amount': 4200,
                        },
                        'quantity': 1,
                    },
                    {
                        'price_data': {
                            'currency': 'eur',
                            'product_data': {
                                'name': 'Frais de port',
                                'description': 'Colissimo, 1450g',
                            },
                            'unit_amount': 850,
                        },
                        'quantity': 1,
                    }
                ],
                'mode': "payment",
                'success_url': 'https://techne-bookshop.fr/commande-effectuee',
                'cancel_url': 'https://techne-bookshop.fr/commande-annulee',
            }
        }
    }

    params = request.GET.copy()
    is_online_payment = True
    if params.get('paid') and not utils._is_truthy(params.get('paid')):
        real_test_payload['order']['online_payment'] = False
        is_online_payment = False

    # Parse mondial relay data (it's JSON in another string).
    try:
        mondial_relay_json = real_test_payload['order']['mondial_relay_AP']
        mondial_relay_dict = json.loads(mondial_relay_json)
        real_test_payload['order']['mondial_relay_AP'] = mondial_relay_dict
    except Exception as e:
        log.warning('Could not parse order.mondial_relay_AP JSON data from this payload: {}: {}'.format(real_test_payload, e))

    # Test total weight.
    cards = Card.objects.all()[:4]
    total_weight, weight_message = utils.get_total_weight(cards)
    return render(request, template,
                  {'payload': real_test_payload,
                   'payment_meta': real_test_payload,
                   'is_online_payment': is_online_payment,
                   'bookshop_name': bookshop_name,
                   'cards': cards,
                   'total_weight': total_weight,
                   'weight_message': weight_message,
                   })

@login_required
def test_client_confirmation_email(request):
    # template = 'mailer/client_confirmation_template.html'
    import mailer
    template = mailer.find_theme_template('client_confirmation_template.html')
    only_testing = mailer.get_template_with_default(template, 'mailer/client_confirmation_template.html')  # noqa
    bookshop_name = "Librairie"
    # XXX: copy-paste from noci test!
    real_test_payload = {
        'buyer': {
            'billing_address': {
                'last_name': 'Vincent',
                'first_name': 'vindarel',
                'email': 'vindarel_XYZ@mailz.org',
                'address': 'here my city',
                'address_comp': 'comp',
                'city': 'Touwin',
                'postcode': '34100',
                'country': 'France',
                'phone': '07 33 88 88 77',
            },
            'delivery_address': {
                'last_name': 'Vincent',
                'first_name': 'vindarel',
                'email': 'vindarel_XYZ@mailz.org',
                'address': 'here France',
                'address_comp': 'comp',
                'city': 'Touwin',
                'postcode': '34100',
                'country': 'France',
                'phone': '07 98 88 88 88'
            },
        },
        'order': {
            'online_payment': True,
            'shipping_method': 'relay',  # colissimo
            'mondial_relay_AP': '{"Adresse1":"28 RUE EMILE CARTAILHAC","Adresse2":null,"Available":true,"CP":"31000","HoursHtmlTable":"<table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Monday<\\/th><td>15h00-19h00<\\/td><td>-<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr class=\'d\'><th>Tuesday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Wednesday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr class=\'d\'><th>Thursday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Friday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr class=\'d\'><th>Saturday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table><table class=\'PR-Hours\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th>Sunday<\\/th><td>11h00-13h00<\\/td><td>14h00-19h00<\\/td><\\/tr><\\/table>","ID":"071058","Lat":"43,6076448","Long":"1,440779","Nature":"D","Nom":"CBD SHOP FRANCE","Pays":"FR","Photo":null,"Ville":"TOULOUSE","Warning":"","Letter":"A"}',

            'amount': 5050,
            # 'abelujo_items': [{'id': 100, 'qty': 1}, {'id': 101, 'qty': 1}],
            # 'abelujo_items': [{'id': 1, 'qty': 1}, {'id': 2, 'qty': 1}],
            'abelujo_items': [{'id': 1, 'qty': 1}],
            'stripe_payload': {
                'payment_method_types': ["card"],
                'line_items': [
                    {
                        'price_data': {
                            'currency': 'eur',
                            'product_data': {
                                'name': 'ârt Du Chantier, Construire Et dééémolir Du Xvi Au Xxie Siecle',
                                'description': 'ISBN: 9789461614728',
                            },
                            'unit_amount': 4200,
                        },
                        'quantity': 1,
                    },
                    {
                        'price_data': {
                            'currency': 'eur',
                            'product_data': {
                                'name': 'Frais de port',
                                'description': 'Colissimo, 1450g',
                            },
                            'unit_amount': 850,
                        },
                        'quantity': 1,
                    }
                ],
                'mode': "payment",
                'success_url': 'https://techne-bookshop.fr/commande-effectuee',
                'cancel_url': 'https://techne-bookshop.fr/commande-annulee',
            }
        }
    }

    real_test_payload = """
{"buyer": {"delivery_address": {"city": "V ille", "first_name": "vindarel", "last_name": "vindarel", "country": "France", "phone": "", "postcode": "31000", "address": "34", "address_comp": "", "email": "ehvince@mailz.org"}, "billing_address": {"city": "Ville", "first_name": "vindarel", "last_name": "vindarel", "country": "France", "phone": "", "postcode": "31000", "address": "34", "address_comp": "", " email": "ehvince@mailz.org"}}, "order": {"online_payment": false, "shipping_method": "local", "stripe_payload": {"line_items": [{"price_data": {"currency": "eur", "product_data": {"name" : "Comme un million de papillons noirs"}, "unit_amount": 1400}, "quantity": 1}], "customer_emai l": "ehvince@mailz.org", "cancel_url": "https://techne-bookshop.fr/commande-annulee", "success_ url": "https://techne-bookshop.fr/commande-effectuee", "mode": "payment", "payment_method_types ": ["card"]}, "used_promo_code": "", "amount": 1400, "abelujo_items": [{"id": 1045, "qty": 1}], "mondial_relay_AP": ""}}
    """
    real_test_payload = real_test_payload.strip()
    real_test_payload = json.loads(real_test_payload)

    params = request.GET.copy()
    is_online_payment = True
    if params.get('paid') and not utils._is_truthy(params.get('paid')):
        real_test_payload['order']['online_payment'] = False
        is_online_payment = False

    return render(request, template,
                  {'payload': real_test_payload,
                   'payment_meta': real_test_payload,
                   'is_online_payment': is_online_payment,
                   'bookshop_name': bookshop_name,
                   'date': "2021-03-98",
                   })
