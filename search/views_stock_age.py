# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
# from django.utils.translation import ugettext as _

from search.models import Stats

from search.models.utils import get_logger

from views_utils import is_htmx_request

log = get_logger()

DEFAULT_NB_COPIES = 1         # default nb of copies to add.
PENDULUM_YMD = '%Y-%m-%d'  # caution, %m is a bit different than datetime's %M.

@login_required
def shelf_age(request, *args, **kw):
    """
    Get the cards of the given age: 0-3 months, 3-6… >24.
    """
    template = "search/shelf_age.html"
    if is_htmx_request(request.META):
        template = "search/shelf_age_content.html"

    if request.method == 'GET':
        params = request.GET.copy()
        pk = params.get('shelf_id', 1)
        age = int(params.get('age', 1))  # not gonna accept weird user input.
        page = int(params.get('page', 1))

        data = Stats.shelf_age_cards(pk, age, page=page)

        return render(request, template, {
            'cards': data['data'],
            'meta': data['meta'],
        })
