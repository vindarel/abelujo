# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0053_auto_20220118_1657'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookshop',
            name='firstname_ascii',
            field=models.CharField(max_length=200, null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='bookshop',
            name='name_ascii',
            field=models.CharField(max_length=200, null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='client',
            name='firstname_ascii',
            field=models.CharField(max_length=200, null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='client',
            name='name_ascii',
            field=models.CharField(max_length=200, null=True, editable=False, blank=True),
        ),
    ]
