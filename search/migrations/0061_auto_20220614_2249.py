# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0060_auto_20220525_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='cardtype',
            name='vat',
            field=models.FloatField(null=True, verbose_name='VAT tax', blank=True),
        ),
    ]
