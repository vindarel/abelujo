# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0066_auto_20230519_1707'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='last_soldcard_date',
            field=models.DateTimeField(null=True, editable=False, blank=True),
        ),
    ]
