# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0080_auto_20240912_0959'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='place',
            name='can_sell',
        ),
    ]
