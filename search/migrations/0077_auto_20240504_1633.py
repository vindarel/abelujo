# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0076_auto_20240409_1515'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='restockingcopies',
            name='card',
        ),
        migrations.RemoveField(
            model_name='restockingcopies',
            name='restocking',
        ),
        migrations.DeleteModel(
            name='Restocking',
        ),
        migrations.DeleteModel(
            name='RestockingCopies',
        ),
    ]
