# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0079_auto_20240905_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='nb_from_stock',
            field=models.IntegerField(default=1, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reservation',
            name='nb_to_command',
            field=models.IntegerField(default=0, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='statscache',
            name='only_metadata',
            field=models.CharField(max_length=10000, blank=True),
        ),
    ]
