# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0068_auto_20230519_1721'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='payment',
            field=models.CharField(blank=True, max_length=200, null=True, choices=[(7, 'ESPECES'), (9, 'CB'), (8, 'CHEQUE'), (12, 'cheque lire'), (10, 'virement'), (100, "bon d'achat"), (11, 'autre')]),
        ),
        migrations.AlterField(
            model_name='outmovement',
            name='typ',
            field=models.IntegerField(choices=[(1, 'sell'), (2, 'return'), (3, 'loss'), (4, 'gift'), (5, 'box')]),
        ),
        migrations.AlterField(
            model_name='sell',
            name='payment',
            field=models.CharField(default=(7, 'ESPECES'), choices=[(7, 'ESPECES'), (9, 'CB'), (8, 'CHEQUE'), (12, 'cheque lire'), (10, 'virement'), (100, "bon d'achat"), (11, 'autre')], max_length=200, blank=True, null=True, verbose_name='payment'),
        ),
        migrations.AlterField(
            model_name='sell',
            name='payment_2',
            field=models.CharField(default='0', choices=[(7, 'ESPECES'), (9, 'CB'), (8, 'CHEQUE'), (12, 'cheque lire'), (10, 'virement'), (100, "bon d'achat"), (11, 'autre')], max_length=200, blank=True, null=True, verbose_name='payment'),
        ),
    ]
