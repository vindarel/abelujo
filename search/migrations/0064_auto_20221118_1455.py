# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0063_auto_20221019_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='img2_crop',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='card',
            name='img3_crop',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='card',
            name='img4_crop',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
