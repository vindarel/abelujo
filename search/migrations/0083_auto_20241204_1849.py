# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0082_auto_20241203_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sell',
            name='total_payment_3',
            field=models.FloatField(default='0', null=True, blank=True),
        ),
    ]
