# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0051_auto_20220117_1456'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributor',
            name='comment',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distributor',
            name='mobilephone',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distributor',
            name='telephone',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distributor',
            name='website',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
