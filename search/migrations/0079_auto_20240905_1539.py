# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0078_auto_20240515_1146'),
    ]

    operations = [
        migrations.CreateModel(
            name='StatsCache',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=200)),
                ('date', models.DateTimeField()),
                ('raw_data', models.CharField(max_length=90000)),
                ('only_metadata', models.CharField(max_length=10000)),
                ('metadata_1', models.CharField(max_length=10000, blank=True)),
                ('metadata_2', models.CharField(max_length=10000, blank=True)),
                ('metadata_3', models.CharField(max_length=10000, blank=True)),
                ('metadata_4', models.CharField(max_length=10000, blank=True)),
                ('metadata_5', models.CharField(max_length=10000, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),

        # Let's not remove those fields yet... if someone relies on them this year

        # migrations.RemoveField(
        #     model_name='deposit',
        #     name='dest_place',
        # ),
        # migrations.RemoveField(
        #     model_name='deposit',
        #     name='distributor',
        # ),
        # migrations.RemoveField(
        #     model_name='depositstate',
        #     name='copies',
        # ),
        # migrations.RemoveField(
        #     model_name='depositstate',
        #     name='deposit',
        # ),
        # migrations.RemoveField(
        #     model_name='depositstatecopies',
        #     name='card',
        # ),
        # migrations.RemoveField(
        #     model_name='depositstatecopies',
        #     name='deposit_state',
        # ),
        # migrations.RemoveField(
        #     model_name='depositstatecopies',
        #     name='sells',
        # ),
        # migrations.RemoveField(
        #     model_name='alert',
        #     name='deposits',
        # ),
        # migrations.RemoveField(
        #     model_name='sell',
        #     name='deposit',
        # ),
        # migrations.RemoveField(
        #     model_name='soldcards',
        #     name='deposit',
        # ),
        # migrations.DeleteModel(
        #     name='Deposit',
        # ),
        # migrations.DeleteModel(
        #     name='DepositState',
        # ),
        # migrations.DeleteModel(
        #     name='DepositStateCopies',
        # ),

    ]
