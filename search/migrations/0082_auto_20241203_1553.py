# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0081_auto_20240918_1718'),
    ]

    operations = [
        migrations.AddField(
            model_name='sell',
            name='payment_3',
            field=models.CharField(default='0', choices=[(7, 'ESPECES'), (9, 'CB'), (8, 'CHEQUE'), (12, 'cheque lire'), (10, 'virement'), (100, "bon d'achat"), (11, 'autre')], max_length=200, blank=True, null=True, verbose_name='payment'),
        ),
        migrations.AddField(
            model_name='sell',
            name='total_payment_3',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
